# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib


class FoxtrotLoader:

    def _get_keyfile(self, file_info, metadata_path):
        music_mtime = file_info.get_attribute_uint64("time::modified")
        keyfile = GLib.KeyFile.new()
        keyfile.load_from_file(metadata_path, GLib.KeyFileFlags.KEEP_COMMENTS)
        metadata_mtime = keyfile.get_integer("metadata", "modified")
        if music_mtime == metadata_mtime:
            return keyfile
        gfile = Gio.File.new_for_path(metadata_path)
        gfile.delete()
        keyfile.unref()
        return None

    def load_metadata(self, file_info, metadata_path):
        if not GLib.file_test(metadata_path, GLib.FileTest.EXISTS):
            return None
        keyfile = self._get_keyfile(file_info, metadata_path)
        if keyfile is None:
            return None
        title = keyfile.get_string("metadata", "title")
        artist = keyfile.get_string("metadata", "artist")
        album = keyfile.get_string("metadata", "album")
        seconds = keyfile.get_integer("metadata", "duration")
        keyfile.unref()
        return title, artist, album, seconds
