# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

names = [GLib.get_user_cache_dir(), "kusozako1", "audio-metadata"]
DIRECTORY = GLib.build_filenamev(names)


class FoxtrotDirectory:

    def get_path(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        uri = gfile.get_uri()
        self._checksum.update(bytes(uri, "utf-8"))
        md5_checksum = self._checksum.get_string()
        self._checksum.reset()
        names = [DIRECTORY, md5_checksum+".metadata"]
        return md5_checksum, GLib.build_filenamev(names)

    def __init__(self):
        self._checksum = GLib.Checksum.new(GLib.ChecksumType.MD5)
        if GLib.file_test(DIRECTORY, GLib.FileTest.EXISTS):
            return
        gfile = Gio.File.new_for_path(DIRECTORY)
        gfile.make_directory_with_parents(None)
