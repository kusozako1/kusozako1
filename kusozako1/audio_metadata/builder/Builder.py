# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# from gi.repository import Gio
from gi.repository import GLib
from .Duration import FoxtrotDuration
from .Mutagen import FoxtrotMutagen


class FoxtrotBuilder:

    def _get_metadata(self, file_info):
        metadata = self._mutagen.get_metadata(file_info)
        if metadata is None:
            metadata = file_info.get_name(), "", ""
        title, artist, album = metadata
        seconds = self._duration.get_duration(file_info)
        return title, artist, album, seconds

    def _save(self, file_info, metadata_path, metadata):
        key_file = GLib.KeyFile.new()
        title, artist, album, seconds = metadata
        gfile = file_info.get_attribute_object("standard::file")
        uri = gfile.get_uri()
        key_file.set_string("metadata", "uri", uri)
        modified = file_info.get_attribute_uint64("time::modified")
        key_file.set_integer("metadata", "modified", modified)
        key_file.set_string("metadata", "title", title)
        key_file.set_string("metadata", "artist", artist)
        key_file.set_string("metadata", "album", album)
        key_file.set_integer("metadata", "duration", seconds)
        key_file.save_to_file(metadata_path)
        key_file.unref()

    def build_(self, file_info, metadata_path):
        metadata = self._get_metadata(file_info)
        GLib.idle_add(self._save, file_info, metadata_path, metadata)
        return metadata

    def __init__(self):
        self._mutagen = FoxtrotMutagen()
        self._duration = FoxtrotDuration()
