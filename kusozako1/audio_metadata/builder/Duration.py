# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from pymediainfo import MediaInfo


class FoxtrotDuration:

    def get_duration(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        path = gfile.get_path()
        media_info = MediaInfo.parse(path)
        for track in media_info.tracks:
            if track.track_type == "Audio":
                seconds = int(track.duration/1000)
                return seconds
        return 0
