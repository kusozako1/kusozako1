# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import mutagen
from . import Keys


class FoxtrotMutagen:

    def _get_mutagen_file(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        path = gfile.get_path()
        try:
            return mutagen.File(path)
        except mutagen.mp3.HeaderNotFoundError:
            return None

    def _has_key(self, mutagen_file, key):
        try:
            has_key = key in mutagen_file
            return has_key
        except ValueError:
            return False

    def _get_tag(self, mutagen_file, keys, default):
        for key in keys:
            if self._has_key(mutagen_file, key):
                return mutagen_file[key][0]
        return default

    def get_metadata(self, file_info):
        mutagen_file = self._get_mutagen_file(file_info)
        if mutagen_file is None:
            return None
        basename = file_info.get_name()
        title = self._get_tag(mutagen_file, Keys.TITLE, basename)
        artist = self._get_tag(mutagen_file, Keys.ARTIST, "")
        album = self._get_tag(mutagen_file, Keys.ALBUM, "")
        return title, artist, album

    def __init__(self):
        pass
