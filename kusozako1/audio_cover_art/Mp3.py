
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import mutagen
from mutagen.id3 import ID3
from gi.repository import Gio
from gi.repository import GLib
from .ThumbnailLoader import AlfaThumbnailLoader


class FoxtrotMp3(AlfaThumbnailLoader):

    def _get_stream(self, id3):
        for key in id3:
            if not key.startswith("APIC"):
                continue
            apic = id3.get(key)
            glib_bytes = GLib.Bytes.new(apic.data)
            return Gio.MemoryInputStream.new_from_bytes(glib_bytes)

    def _get_stream_for_path(self, path):
        try:
            id3 = ID3(path)
            return self._get_stream(id3)
        except mutagen.MutagenError:
            return None
