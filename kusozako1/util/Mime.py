# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import magic
from gi.repository import Gio


def _get_from_magic(path, file_info):
    try:
        mime = magic.Magic(mime=True)
        return mime.from_file(path)
    except PermissionError:
        return file_info.get_attribute_string("standard::content-type")


def _get_from_gfile(path, gfile):
    file_info = gfile.query_info("*", 0)
    if file_info.get_file_type() == Gio.FileType.REGULAR:
        return _get_from_magic(path, file_info)
    return file_info.get_attribute_string("standard::content-type")


def from_path(path):
    gfile = Gio.File.new_for_path(path)
    if not gfile.query_exists():
        return None
    return _get_from_gfile(path, gfile)
