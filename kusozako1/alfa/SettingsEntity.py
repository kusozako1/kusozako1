# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class AlfaSettingsEntity(DeltaEntity):

    GROUP = "define group here"
    KEY = "define key here"
    DEFAULT = "defaine default value here."

    def _reset(self, value):
        raise NotImplementedError

    def _bind(self):
        pass

    def _get_query(self):
        return self.GROUP, self.KEY, self.DEFAULT

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group == self.GROUP and key == self.KEY:
            self._reset(value)

    def __init__(self, parent):
        self._parent = parent
        default = self._enquiry("delta > settings", self._get_query())
        self._reset(default)
        self._bind()
        self._raise("delta > register settings object", self)
