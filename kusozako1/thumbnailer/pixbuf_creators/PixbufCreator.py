# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio


class BravoPixbufCreator:

    def _get_gfile(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        uri_scheme = gfile.get_uri_scheme()
        if uri_scheme == "file":
            return gfile
        target_uri = file_info.get_attribute_string("standard::target-uri")
        return Gio.File.new_for_uri(target_uri)
