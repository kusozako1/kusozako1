# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from pymediainfo import MediaInfo
from gi.repository import GdkPixbuf
from kusozako1.const import PixbufOptionKeys
from .PixbufCreator import BravoPixbufCreator


class FoxtrotVideo(BravoPixbufCreator):

    def _get_options(self, gfile, file_info, video_track):
        options = {}
        mime = file_info.get_content_type()
        options[PixbufOptionKeys.URI] = gfile.get_uri()
        options[PixbufOptionKeys.SIZE] = str(file_info.get_size())
        mtime = file_info.get_attribute_uint64("time::modified")
        options[PixbufOptionKeys.MTIME] = str(mtime)
        options[PixbufOptionKeys.MIMETYPE] = mime
        options[PixbufOptionKeys.SOFTWARE] = PixbufOptionKeys.SOFTWARE_NAME
        options[PixbufOptionKeys.WIDTH] = str(video_track.width)
        options[PixbufOptionKeys.HEIGHT] = str(video_track.height)
        options[PixbufOptionKeys.LENGTH] = str(video_track.duration)
        return options

    def _get_safe_path(self):
        while True:
            names = [GLib.get_tmp_dir(), str(GLib.random_int())+".png"]
            path = GLib.build_filenamev(names)
            if not GLib.file_test(path, GLib.FileTest.EXISTS):
                return path

    def create_from_file_info(self, file_info):
        gfile = self._get_gfile(file_info)
        path = gfile.get_path()
        media_info = MediaInfo.parse(path)
        video_track = media_info.video_tracks[0]
        edge_length = max(video_track.width, video_track.height)
        scale = 128/min(video_track.width, video_track.height)
        destination_path = self._get_safe_path()
        command = [
            "ffmpegthumbnailer",
            "-i", path,
            "-o", destination_path,
            "-t", "10%",
            "-s", str(edge_length*scale)
            ]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        success, _, _ = subprocess.communicate_utf8()
        if not success:
            return None
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(destination_path)
        options = self._get_options(gfile, file_info, video_track)
        return pixbuf, options
