# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from pymediainfo import MediaInfo
from gi.repository import GdkPixbuf
from kusozako1.const import PixbufOptionKeys
from .PixbufCreator import BravoPixbufCreator


class FoxtrotImage(BravoPixbufCreator):

    def _get_options(self, gfile, file_info, image_track):
        options = {}
        options[PixbufOptionKeys.URI] = gfile.get_uri()
        options[PixbufOptionKeys.SIZE] = str(file_info.get_size())
        mtime = file_info.get_attribute_uint64("time::modified")
        options[PixbufOptionKeys.MTIME] = str(mtime)
        options[PixbufOptionKeys.MIMETYPE] = file_info.get_content_type()
        options[PixbufOptionKeys.SOFTWARE] = PixbufOptionKeys.SOFTWARE_NAME
        options[PixbufOptionKeys.WIDTH] = str(image_track.width)
        options[PixbufOptionKeys.HEIGHT] = str(image_track.height)
        return options

    def create_from_file_info(self, file_info):
        gfile = self._get_gfile(file_info)
        path = gfile.get_path()
        media_info = MediaInfo.parse(path)
        tracks = media_info.image_tracks
        if not tracks:
            print("path", path, "has-no-image")
            return None
        image_track = tracks[0]
        scale = min(1, max(128/image_track.width, 128/image_track.height))
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
            path,
            int(image_track.width*scale),
            int(image_track.height*scale),
            True
            )
        options = self._get_options(gfile, file_info, image_track)
        return pixbuf, options
