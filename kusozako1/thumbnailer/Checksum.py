# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


class FoxtrotChecksum:

    @classmethod
    def get_default(cls):
        if "_default" not in dir(cls):
            cls._default = cls()
        return cls._default

    def _get_basename_for_uri(self, uri):
        uri_as_bytes = bytes(uri, encoding="utf-8")
        self._checksum.update(uri_as_bytes)
        basename = self._checksum.get_string()
        self._checksum.reset()
        return basename

    def get_thumbnail_path_for_uri(self, uri):
        basename = self._get_basename_for_uri(uri)
        names = [self._base_directory, basename+".png"]
        return GLib.build_filenamev(names)

    def __init__(self):
        self._checksum = GLib.Checksum.new(GLib.ChecksumType.MD5)
        names = [GLib.get_user_cache_dir(), "kusozako1", "thumbnail", "128"]
        self._base_directory = GLib.build_filenamev(names)
