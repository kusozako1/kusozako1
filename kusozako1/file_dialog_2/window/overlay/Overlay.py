# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from .EventControllerKey import DeltaEventControllerKey
from .pages.Pages import EchoPages


class DeltaOverlay(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_hide_overlay(self):
        self.set_visible_child_name("blank")
        self.set_can_target(False)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.SHOW_OVERLAY:
            return
        page_name, _ = param
        self.set_visible_child_name(page_name)
        self.set_can_target(True)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.CROSSFADE,
            transition_duration=1000,
            vexpand=True,
            hhomogeneous=False,
            vhomogeneous=False,
            can_target=False,
            )
        EchoPages(self)
        DeltaEventControllerKey(self)
        self._raise("delta > add overlay", self)
        self._raise("delta > register file dialog object", self)
