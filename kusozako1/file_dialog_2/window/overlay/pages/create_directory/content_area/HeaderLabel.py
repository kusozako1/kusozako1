# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaHeaderLabel(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, label="Make Directory", margin_top=16)
        self.add_css_class("kusozako-large-title")
        self.set_size_request(-1, 32)
        self._raise("delta > add to container", self)
