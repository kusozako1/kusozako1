# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import OverlayPages
from .VerticalSpacer import DeltaVerticalSpacer
from .Model import DeltaModel
from .content_area.ContentArea import DeltaContentArea


class DeltaCreateDirectory(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_call_change_name(self, name):
        self._model.change_name(name)

    def _delta_call_try_make_directory(self):
        self._model.try_make_directory()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            )
        DeltaVerticalSpacer(self)
        self._model = DeltaModel(self)
        DeltaContentArea(self)
        DeltaVerticalSpacer(self)
        user_data = self, OverlayPages.CREATE_DIRECTORY
        self._raise("delta > add to stack", user_data)
