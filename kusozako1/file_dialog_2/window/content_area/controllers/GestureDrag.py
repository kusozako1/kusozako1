# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity

EDGES = [
    [Gdk.SurfaceEdge.NORTH_WEST, "default", Gdk.SurfaceEdge.NORTH_EAST],
    ["default", None, "default"],
    [Gdk.SurfaceEdge.SOUTH_WEST, "default", Gdk.SurfaceEdge.SOUTH_WEST]
    ]

BORDER_WIDTH = 16


class DeltaGestureDrag(Gtk.GestureDrag, DeltaEntity):

    def _get_index(self, size, position):
        if BORDER_WIDTH > position:
            return 0
        elif size-BORDER_WIDTH > position > BORDER_WIDTH:
            return 1
        return 2

    def _on_drag_begin(self, gesture_drag, x, y):
        widget = gesture_drag.get_widget()
        x_index = self._get_index(widget.get_allocated_width(), x)
        y_index = self._get_index(widget.get_allocated_height(), y)
        edge = EDGES[y_index][x_index]
        surface = widget.get_native().get_surface()
        new_y = y + surface.get_height() - widget.get_allocated_height()
        device = gesture_drag.get_device()
        if edge == "default":
            surface.begin_move(device, 1, x, new_y, Gdk.CURRENT_TIME)
        elif edge is not None:
            surface.begin_resize(edge, device, 1, x, new_y, Gdk.CURRENT_TIME)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureDrag.__init__(self)
        self.connect("drag-begin", self._on_drag_begin)
        self._raise("delta > add controller", self)
