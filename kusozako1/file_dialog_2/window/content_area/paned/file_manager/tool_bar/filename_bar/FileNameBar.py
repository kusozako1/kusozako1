# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Label import DeltaLabel
from .entry.Entry import DeltaEntry


class DeltaFileNameBar(Gtk.CenterBox, DeltaEntity):

    def _delta_call_add_to_container_start(self, widget):
        self.set_start_widget(widget)

    def _delta_call_add_to_container_center(self, widget):
        self.set_center_widget(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            )
        self.add_css_class("kusozako-primary-surface")
        self.set_size_request(-1, 48)
        DeltaLabel(self)
        DeltaEntry(self)
        self._raise("delta > add to container", self)
