# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import ConfigKeys
from .watchers.Watchers import EchoWatchers


class DeltaEntry(Gtk.Entry, DeltaEntity):

    def _delta_info_entry(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        filename = self._enquiry("delta > config", ConfigKeys.DEFAULT_FILENAME)
        if filename is None:
            filename = "foobar"
        Gtk.Entry.__init__(
            self,
            hexpand=True,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            text=filename,
            placeholder_text=_("filenme"),
            secondary_icon_activatable=True,
            secondary_icon_name="edit-clear-symbolic",
            )
        EchoWatchers(self)
        self._raise("delta > add to container center", self)
