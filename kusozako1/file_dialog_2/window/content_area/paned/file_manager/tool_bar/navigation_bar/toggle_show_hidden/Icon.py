# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.icon import Icon
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaIcon(Gtk.Image, DeltaEntity):

    def _reset(self):
        if self._show:
            icon_name = "view-reveal-symbolic"
            self.add_css_class("kusozako-button-indicator-warning")
        else:
            icon_name = "view-conceal-symbolic"
            self.remove_css_class("kusozako-button-indicator-warning")
        paintable = Icon.get_paintable_for_name(icon_name)
        self.set_from_paintable(paintable)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.SETTINGS_CHANGED:
            return
        key, value = param
        if key != "show-hidden":
            return
        self._show = value
        self._reset()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, vexpand=True)
        self._show = self._enquiry("delta > settings", "show-hidden")
        self._reset()
        self._raise("delta > add to container", self)
        self._raise("delta > register file dialog object", self)
