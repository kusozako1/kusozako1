# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.widget.IndicatorButton import AlfaIndicatorButton
from kusozako1.widget.ButtonIndicator import DeltaButtonIndicator
from kusozako1.file_dialog_2.const import OverlayPages
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaCreateDirectoryButton(AlfaIndicatorButton):

    __tooltip_text__ = _("Create Directory")

    def _on_clicked(self, button):
        current_gfile = self._enquiry("delta > current gfile")
        signal_param = OverlayPages.CREATE_DIRECTORY, current_gfile
        user_data = FileDialogSignals.SHOW_OVERLAY, signal_param
        self._raise("delta > file dialog signal", user_data)

    def _on_initialize(self):
        image = Gtk.Image.new_from_icon_name("folder-new-symbolic")
        image.set_vexpand(True)
        self._box.append(image)
        self._indicator = DeltaButtonIndicator.new_for_rgba(self)
