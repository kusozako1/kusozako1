# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaClicked(DeltaEntity):

    def _on_clicked(self, button):
        gfile = self._enquiry("delta > button gfile")
        user_data = FileDialogSignals.MOVE_DIRECTORY, gfile
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        button = self._enquiry("delta > button")
        button.connect("clicked", self._on_clicked)
