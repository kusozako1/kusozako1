# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaCurrentGFile(DeltaEntity):

    def _reset(self, current_gfile):
        gfile = self._enquiry("delta > button gfile")
        self._equal = gfile.equal(current_gfile)
        self._raise("delta > directory moved", self._equal)
        self._raise("delta > extended changed", False)

    def receive_transmission(self, user_data):
        signal, current_gfile = user_data
        if signal != FileDialogSignals.DIRECTORY_MOVED:
            return
        self._reset(current_gfile)

    def start(self):
        current_gfile = self._enquiry("delta > current gfile")
        self._reset(current_gfile)
        self._raise("delta > register file dialog object", self)

    def __init__(self, parent):
        self._parent = parent
        self._equal = False

    @property
    def equal(self):
        return self._equal
