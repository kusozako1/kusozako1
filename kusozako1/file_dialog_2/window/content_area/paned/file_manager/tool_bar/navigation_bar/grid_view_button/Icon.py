# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.icon import Icon
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import ViewerTypes
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaIcon(Gtk.Image, DeltaEntity):

    def _reset(self, setting):
        if setting == ViewerTypes.GRID_VIEW:
            self.add_css_class("kusozako-button-indicator-highlight")
        else:
            self.remove_css_class("kusozako-button-indicator-highlight")

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.SETTINGS_CHANGED:
            return
        key, setting = param
        if key != "viewer-type":
            return
        self._reset(setting)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, vexpand=True)
        paintable = Icon.get_paintable_for_name("view-grid-symbolic")
        self.set_from_paintable(paintable)
        setting = self._enquiry("delta > settings", "viewer-type")
        self._reset(setting)
        self._raise("delta > add to container", self)
        self._raise("delta > register file dialog object", self)
