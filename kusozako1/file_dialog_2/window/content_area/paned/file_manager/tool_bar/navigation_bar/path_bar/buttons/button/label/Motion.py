# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _idle(self, signal, extended, file_info=None):
        user_data = signal, file_info
        self._raise("delta > file dialog signal", user_data)
        self._raise("delta > extended changed", extended)
        return GLib.SOURCE_REMOVE

    def _on_enter(self, controller, x, y, file_info):
        args = FileDialogSignals.SHOW_PREVIEW, True, file_info
        GLib.timeout_add(1, self._idle, *args)

    def _on_leave(self, controller):
        args = FileDialogSignals.BACK_TO_BOOKMARK, False, None
        GLib.timeout_add(1, self._idle, *args)

    def _get_file_info(self):
        gfile = self._enquiry("delta > button gfile")
        file_info = gfile.query_info("*", 0)
        file_info.set_attribute_string("kusozako1::uri", gfile.get_uri())
        return file_info

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerMotion.__init__(self)
        file_info = self._get_file_info()
        self.connect("enter", self._on_enter, file_info)
        self.connect("leave", self._on_leave)
        self._raise("delta > add controller", self)
