# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaDeepestPath(DeltaEntity):

    def _try_delete(self, gfile):
        if gfile.query_exists():
            return
        self._raise("delta > directory lost", gfile)
        parent_gfile = gfile.get_parent()
        self._try_delete(parent_gfile)

    def _timeout(self):
        if not self._gfile.query_exists():
            self._try_delete(self._gfile)
        return GLib.SOURCE_CONTINUE

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != FileDialogSignals.DIRECTORY_MOVED:
            return
        current_path = gfile.get_uri()
        if self._gfile.get_uri().startswith(current_path):
            return
        self._gfile = gfile
        self._raise("delta > deepest path moved", gfile)

    def __init__(self, parent):
        self._parent = parent
        self._gfile = self._enquiry("delta > current gfile")
        GLib.timeout_add(100, self._timeout)
        self._raise("delta > register file dialog object", self)
