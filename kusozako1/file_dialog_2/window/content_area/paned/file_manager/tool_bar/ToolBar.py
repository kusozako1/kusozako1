# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import DialogTypes
from .navigation_bar.NavigationBar import DeltaNavigationBar
from .filename_bar.FileNameBar import DeltaFileNameBar


class DeltaToolBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)
        self._height += 48

    def __init__(self, parent):
        self._parent = parent
        self._height = 0
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaNavigationBar(self)
        dialog_type = self._enquiry("delta > config", "dialog_type")
        if dialog_type == DialogTypes.SAVE_FILE:
            DeltaFileNameBar(self)
        self._raise("delta > add overlay", self)

    @property
    def height(self):
        return self._height
