# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .CheckButton import FoxtrotCheckButton
from .Image import FoxtrotImage
from .info.Info import FoxtrotInfo


class FoxtrotListItemWidget(Gtk.Box):

    def bind(self, gfile, file_info):
        self._check_button.bind(file_info)
        self._image.bind(gfile, file_info)
        self._info.bind(gfile, file_info)

    def _on_pixbuf_options(self, image, pixbuf_options):
        self._info.set_pixbuf_options(pixbuf_options)

    def __init__(self, selection_model):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            margin_start=8,
            margin_end=8,
            spacing=8,
            )
        self._check_button = FoxtrotCheckButton(selection_model)
        self.append(self._check_button)
        self._image = FoxtrotImage()
        self._image.connect("pixbuf-options", self._on_pixbuf_options)
        self.append(self._image)
        self._info = FoxtrotInfo()
        self.append(self._info)
        self.set_size_request(-1, 48)
