# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import Pango
from kusozako1.util.ReadableTime import FoxtrotReadableTime


class FoxtrotProperties(Gtk.Box):

    def bind(self, file_info):
        content_type = file_info.get_content_type()
        if content_type == "inode/directory":
            self._size.set_label("-")
        else:
            size = GLib.format_size(file_info.get_size())
            self._size.set_label("{}".format(size))
        self._type.set_label(content_type)
        date_time = file_info.get_modification_date_time()
        readable_time = self._readable_time.get_label(date_time)
        self._last_modified.set_label(readable_time)

    def _get_label(self):
        label = Gtk.Label(ellipsize=Pango.EllipsizeMode.END, xalign=1)
        label.add_css_class("dim-label")
        self.append(label)
        return label

    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            spacing=8
            )
        self._size = self._get_label()
        self._type = self._get_label()
        self._last_modified = self._get_label()
        self._readable_time = FoxtrotReadableTime.get_default()
