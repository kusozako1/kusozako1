# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.file_dialog_2.const import ViewerTypes
from kusozako1.file_dialog_2.alfa.viewer.Viewer import AlfaViewer
from .factory.Factory import DeltaFactory


class DeltaListView(AlfaViewer):

    __top_margin__ = 64
    __viewer_type__ = ViewerTypes.LIST_VIEW

    def _on_realize(self, grid_view):
        root = grid_view.get_root()
        root.set_focus(grid_view)

    def _get_viewer(self):
        viewer = Gtk.ListView(
            margin_top=self._get_top_margin(),
            model=self._enquiry("delta > viewer model"),
            single_click_activate=True,
            margin_start=8,
            margin_end=8,
            show_separators=True,
            factory=DeltaFactory(self),
            )
        viewer.add_css_class("card")
        viewer.connect("realize", self._on_realize)
        return viewer
