# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .list_item_widget.ListItemWidget import FoxtrotListItemWidget


class DeltaFactory(Gtk.SignalListItemFactory, DeltaEntity):

    def _on_setup(self, factory, list_item):
        selection_model = self._enquiry("delta > selection model")
        list_item_widget = FoxtrotListItemWidget(selection_model)
        list_item.set_child(list_item_widget)

    def _on_bind(self, factory, list_item):
        file_info = list_item.get_item()
        gfile = self._enquiry("delta > gfile for file info", file_info)
        list_item_widget = list_item.get_child()
        list_item_widget.bind(gfile, file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SignalListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)
