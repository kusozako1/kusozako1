# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.const import PixbufOptionKeys


class FoxtrotSymlinkTarget(Gtk.Revealer):

    def _get_size(self, pixbuf_options):
        width = pixbuf_options.get(PixbufOptionKeys.WIDTH, None)
        height = pixbuf_options.get(PixbufOptionKeys.HEIGHT, None)
        if width is None or height is None:
            return ""
        return "{}x{}\t".format(width, height)

    def _get_pages(self, pixbuf_options):
        pages = pixbuf_options.get(PixbufOptionKeys.PAGES, None)
        if pages is None:
            return ""
        return "Pages : {}\t".format(pages)

    def _get_readable(self, total_seconds):
        total_seconds = total_seconds
        hours = total_seconds // 3600
        minutes = total_seconds % 3600 // 60
        seconds = total_seconds % 60
        if hours == 0:
            return "Duration : {}:{:02}".format(minutes, seconds)
        return "Duration : {}:{:02}:{:02}".format(hours, minutes, seconds)

    def _get_length(self, pixbuf_options):
        length = pixbuf_options.get(PixbufOptionKeys.LENGTH, None)
        if length is None:
            return ""
        total_seconds = int(float(length)/1000)
        return self._get_readable(total_seconds)

    def bind(self, file_info):
        if not file_info.get_is_symlink():
            return
        symlink_target = file_info.get_symlink_target()
        self._text = "Link to : `{}\t".format(symlink_target)
        self._label.set_label(self._text)
        self.set_reveal_child(True)

    def set_pixbuf_options(self, pixbuf_options):
        label = self._text
        label += self._get_size(pixbuf_options)
        label += self._get_pages(pixbuf_options)
        label += self._get_length(pixbuf_options)
        if label:
            self._label.set_label(label)
            self.set_reveal_child(True)

    def __init__(self):
        Gtk.Revealer.__init__(self)
        self._text = ""
        self.set_reveal_child(False)
        self._label = Gtk.Label(xalign=0)
        self._label.add_css_class("dim-label")
        self.set_child(self._label)
