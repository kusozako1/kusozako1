# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .tool_bar.ToolBar import DeltaToolBar
from .viewers.Viewers import DeltaViewers


class DeltaFileManager(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_overlay(self, widget):
        self.add_overlay(widget)

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def _on_get_child_position(self, overlay, widget, rectangle):
        rectangle.height = widget.height
        return True, rectangle

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self)
        self.connect("get-child-position", self._on_get_child_position)
        DeltaToolBar(self)
        DeltaViewers(self)
        self._raise("delta > add to container", self)
