# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .side_pane.SidePane import DeltaSidePane
from .file_manager.FileManager import DeltaFileManager


class DeltaPaned(Gtk.Paned, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        if self.get_start_child() is None:
            self.set_start_child(widget)
        else:
            self.set_end_child(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            hexpand=True,
            vexpand=True,
            wide_handle=True,
            position=224,
            )
        DeltaSidePane(self)
        DeltaFileManager(self)
        self._raise("delta > add to container", self)
