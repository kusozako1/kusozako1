# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from gi.repository import Gio


class FoxtrotBookmarkEntity(GObject.Object):

    @classmethod
    def new_for_uri(cls, uri):
        instance = cls()
        instance.set_uri(uri)
        return instance

    @classmethod
    def new_for_path(cls, path):
        instance = cls()
        instance.set_path(path)
        return instance

    def set_uri(self, uri):
        pass

    def set_path(self, path):
        self._gfile = Gio.File.new_for_path(path)

    def get_uri(self):
        return self._gfile.get_uri()

    def get_name(self):
        return self._gfile.get_basename()

    def __init__(self):
        GObject.Object.__init__(self)
