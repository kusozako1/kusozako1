# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from .bookmarks.Bookmarks import DeltaBookmarks
from .previewers.Previewers import DeltaPreviewers


class DeltaSidePane(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, name):
        self.set_visible_child_name(name)

    def _delta_call_close_preview(self):
        self.set_visible_child_name("bookmark")

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == FileDialogSignals.BACK_TO_BOOKMARK:
            self.set_visible_child_name("bookmark")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        DeltaBookmarks(self)
        DeltaPreviewers(self)
        self._raise("delta > register file dialog object", self)
        self._raise("delta > add to container", self)
