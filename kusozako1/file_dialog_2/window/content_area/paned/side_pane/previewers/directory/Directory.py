# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import PreviewerTypes
from .models.Models import DeltaModels
from .list_box.ListBox import DeltaListBox
from .EmptyStatus import DeltaEmptyStatus


class DeltaDirectory(Gtk.Stack, DeltaEntity):

    def _delta_info_model(self):
        return self._models.get_model()

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_has_item(self, has_item):
        name = "list-box" if has_item else "empty-status"
        self.set_visible_child_name(name)

    def set_file_info(self, file_info):
        self._models.set_file_info(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self._models = DeltaModels(self)
        DeltaListBox(self)
        DeltaEmptyStatus(self)
        user_data = self, PreviewerTypes.DIRECTORY
        self._raise("delta > add to stack", user_data)
