# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaListBox(Gtk.ListBox, DeltaEntity):

    def _create_widget_func(self, file_info):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=4)
        box.set_size_request(-1, 16)
        gicon = file_info.get_icon()
        image = Gtk.Image.new_from_gicon(gicon)
        box.append(image)
        name = file_info.get_display_name()
        label = Gtk.Label(label=name, xalign=0)
        box.append(label)
        return box

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.add_css_class("osd")
        Gtk.ListBox.__init__(self)
        self.add_css_class("osd")
        model = self._enquiry("delta > model")
        self.bind_model(model, self._create_widget_func)
        scrolled_window.set_child(self)
        user_data = scrolled_window, "list-box"
        self._raise("delta > add to stack", user_data)
