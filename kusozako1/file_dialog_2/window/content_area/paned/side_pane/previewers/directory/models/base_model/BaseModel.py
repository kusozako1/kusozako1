# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaBaseModel(Gtk.DirectoryList, DeltaEntity):

    def _has_item(self, gfile):
        for info in gfile.enumerate_children("*", 0):
            self._raise("delta > has item", True)
            return
        self._raise("delta > has item", False)

    def set_file_info(self, file_info):
        uri = file_info.get_attribute_string("kusozako1::uri")
        gfile = Gio.File.new_for_uri(uri)
        self._has_item(gfile)
        self.set_file(gfile)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DirectoryList.__init__(self, attributes="*")
