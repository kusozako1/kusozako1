# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import PreviewerTypes
from .PixbufLoader import DeltaPixbufLoader
from .DrawingArea import DeltaDrawingArea
from .PixelSize import DeltaPixelSize


class DeltaImage(Gtk.Box, DeltaEntity):

    def _delta_call_pixbuf_loaded(self, pixbuf):
        self._drawing_area.set_pixbuf(pixbuf)
        self._pixel_size.set_pixbuf(pixbuf)

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def set_file_info(self, file_info):
        self._pixbuf_loader.load_async(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=8,
            )
        self.add_css_class("osd")
        self._pixbuf_loader = DeltaPixbufLoader(self)
        self._drawing_area = DeltaDrawingArea(self)
        self._pixel_size = DeltaPixelSize(self)
        user_data = self, PreviewerTypes.IMAGE
        self._raise("delta > add to stack", user_data)
