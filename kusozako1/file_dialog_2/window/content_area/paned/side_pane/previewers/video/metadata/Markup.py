# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


class FoxtrotMarkup:

    def _get_readable(self, total_seconds):
        hours = total_seconds // 3600
        minutes = total_seconds % 3600 // 60
        seconds = total_seconds % 60
        if hours == 0:
            return "Duration : {}:{:02}".format(minutes, seconds)
        return "Duration : {}:{:02}:{:02}".format(hours, minutes, seconds)

    def build_(self, video_track):
        markup = "Size : {} x {}\n\n".format(
            video_track.width,
            video_track.height
            )
        markup += self._get_readable(int(float(video_track.duration)/1000))
        return markup
