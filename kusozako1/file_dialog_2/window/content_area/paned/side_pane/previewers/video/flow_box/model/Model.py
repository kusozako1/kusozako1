# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from gi.repository import Gio
from gi.repository import GdkPixbuf
from .PixbufLoader import DeltaPixbufLoader


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _get_uri(self, file_info):
        uri = file_info.get_attribute_string("kusozako1::uri")
        if not uri.startswith("recent://"):
            return uri
        gfile = file_info.get_attribute_object("kusozako1::target-gfile")
        return gfile.get_uri()

    def _delta_call_pixbuf_loaded(self, pixbuf):
        self.append(pixbuf)

    def set_file_info(self, file_info):
        self.remove_all()
        self._pixbuf_loader.load_async(file_info)

    def __init__(self):
        self._parent = None
        self._pixbuf_loader = DeltaPixbufLoader(self)
        Gio.ListStore.__init__(self, item_type=GdkPixbuf.Pixbuf)
