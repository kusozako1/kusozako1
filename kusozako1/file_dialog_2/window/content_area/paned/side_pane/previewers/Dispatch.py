# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from kusozako1.const import PreviewerTypes

MESSAGES = {
    "inode/directory": PreviewerTypes.DIRECTORY,
    "application/pdf": PreviewerTypes.PDF,
    "image": PreviewerTypes.IMAGE,
    "text": PreviewerTypes.TEXT,
    "video": PreviewerTypes.VIDEO,
    }


class DeltaDispatch(DeltaEntity):

    def _dispatch(self, file_info, content_type):
        for key, previewer_type in MESSAGES.items():
            if content_type.startswith(key):
                user_data = previewer_type, file_info
                self._raise("delta > show preview", user_data)
                return
        self._raise("delta > close preview")

    def receive_transmission(self, user_data):
        signal, file_info = user_data
        if signal != FileDialogSignals.SHOW_PREVIEW:
            return
        uri = file_info.get_attribute_string("kusozako1::uri")
        if self._previous_uri == uri:
            return
        self._previous_uri = uri
        content_type = file_info.get_content_type()
        self._dispatch(file_info, content_type)

    def __init__(self, parent):
        self._parent = parent
        self._previous_uri = None
        self._raise("delta > register file dialog object", self)
