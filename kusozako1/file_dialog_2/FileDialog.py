# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from kusozako1.file_dialog_2.const import DialogTypes
from .data_layer.DataLayer import DeltaDataLayer
from .models.Models import DeltaModels
from .window.Window import DeltaWindow


class DeltaFileDialog(DeltaEntity):

    @classmethod
    def select_file(cls, parent, **kwargs):
        instance = cls(parent)
        mime_types = kwargs.get("mime_types", [])
        if "inode/directory" in mime_types:
            kwargs["dialog_type"] = DialogTypes.SELECT_DIRECTORY
        else:
            kwargs["dialog_type"] = DialogTypes.SELECT_FILE
        instance.construct(**kwargs)

    @classmethod
    def select_directory(cls, parent, **kwargs):
        instance = cls(parent)
        kwargs["dialog_type"] = DialogTypes.SELECT_DIRECTORY
        instance.construct(**kwargs)

    @classmethod
    def save_file(cls, parent, **kwargs):
        instance = cls(parent)
        kwargs["dialog_type"] = DialogTypes.SAVE_FILE
        instance.construct(**kwargs)

    def _delta_call_file_dialog_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_file_dialog_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_loopback_models_ready(self, parent):
        DeltaWindow(parent)

    def _delta_call_loopback_data_layer_ready(self, parent):
        DeltaModels(parent)

    def construct(self, **kwargs):
        DeltaDataLayer.new(self, **kwargs)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
