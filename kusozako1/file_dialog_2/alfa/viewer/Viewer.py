# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import DialogTypes
from .watchers.Watchers import EchoWatchers
from .controllers.Controllers import EchoControllers


class AlfaViewer(DeltaEntity):

    __top_margin__ = "define top margin as int"
    __viewer_type__ = "define viewer type as ViewerTypes"

    def _delta_info_viewer(self):
        return self._viewer

    def _delta_call_add_controller(self, controller):
        self._viewer.add_controller(controller)

    def _get_top_margin(self):
        dialog_type = self._enquiry("delta > config", "dialog_type")
        if dialog_type == DialogTypes.SAVE_FILE:
            return self.__top_margin__ + 48
        return self.__top_margin__

    def _on_value_changed(self, vadjustment):
        # top_margin = max(2, self.__top_margin__-vadjustment.props.value)
        top_margin = max(2, self._get_top_margin()-vadjustment.props.value)
        self._viewer.set_margin_top(top_margin)

    def _get_viewer(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        vadjustment = scrolled_window.get_vadjustment()
        vadjustment.connect("value-changed", self._on_value_changed)
        self._viewer = self._get_viewer()
        EchoControllers(self)
        EchoWatchers(self)
        scrolled_window.set_child(self._viewer)
        user_data = scrolled_window, self.__viewer_type__
        self._raise("delta > add to stack", user_data)
