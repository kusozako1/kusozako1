# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaLongPress(Gtk.GestureLongPress, DeltaEntity):

    def _call_long_press_cancel_hook(self, gfile):
        user_data = FileDialogSignals.LONG_PRESS_CANCEL_HOOK, None
        self._raise("delta > file dialog signal", user_data)

    def _on_event(self, controller, x, y):
        viewer_model = self._enquiry("delta > viewer model")
        file_info = viewer_model.get_selected_item()
        gfile = self._enquiry("delta > gfile for file info", file_info)
        self._call_long_press_cancel_hook(gfile)
        file_info.set_attribute_string("kusozako1::uri", gfile.get_uri())
        file_info.set_attribute_string("kusozako1::path", gfile.get_path())
        user_data = FileDialogSignals.SHOW_PREVIEW, file_info
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureLongPress.__init__(self)
        self.connect("pressed", self._on_event)
        self._raise("delta > add controller", self)
