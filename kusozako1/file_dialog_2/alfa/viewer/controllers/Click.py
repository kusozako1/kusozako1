# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaClick(Gtk.GestureClick, DeltaEntity):

    def _on_event(self, gesture, n_press, x, y):
        if n_press != 1:
            return
        user_data = FileDialogSignals.RELEASE_ACTIVATION_HOOK, None
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(self)
        self.connect("released", self._on_event)
        self._raise("delta > add controller", self)
