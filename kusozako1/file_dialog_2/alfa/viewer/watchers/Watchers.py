# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Activate import DeltaActivate


class EchoWatchers:

    def __init__(self, parent):
        DeltaActivate(parent)
