# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaActivate(DeltaEntity):

    def _on_event(self, grid_view, index):
        user_data = FileDialogSignals.TOGGLE_INDEX, index
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        viewer = self._enquiry("delta > viewer")
        viewer.connect("activate", self._on_event)
