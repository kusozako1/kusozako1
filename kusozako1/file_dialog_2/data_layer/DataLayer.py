# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .settings.Settings import DeltaSettings


class DeltaDataLayer(DeltaEntity):

    @classmethod
    def new(cls, parent, **kwargs):
        instance = cls(parent)
        instance.construct(**kwargs)

    def _delta_info_config(self, key):
        return self._config.get(key, None)

    def _delta_info_settings(self, key):
        return self._settings[key]

    def construct(self, **kwargs):
        self._config = kwargs                   # read only data
        self._settings = DeltaSettings(self)    # read/write data
        self._raise("delta > loopback data layer ready", self)

    def __init__(self, parent):
        self._parent = parent
