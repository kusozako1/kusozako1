# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ToggleSettings import DeltaToggleSettings
from .ChangeSettings import DeltaChangeSettings


class EchoObservers:

    def __init__(self, parent):
        DeltaToggleSettings(parent)
        DeltaChangeSettings(parent)
