# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaToggleSelected(DeltaEntity):

    __signal__ = FileDialogSignals.TOGGLE_SELECTED

    def _signal_received(self, param=None):
        viewer_model = self._enquiry("delta > viewer model")
        selected_index = viewer_model.get_selected()
        user_data = FileDialogSignals.TOGGLE_INDEX, selected_index
        self._raise("delta > file dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != self.__signal__:
            return
        self._signal_received(param)

    def _initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._initialize()
        self._raise("delta > register file dialog object", self)
