# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaActivationHook(DeltaEntity):

    def _timeout(self, index):
        if self._index == index:
            self._index = None
        return GLib.SOURCE_REMOVE

    def _set_index(self, index):
        self._index = index
        GLib.timeout_add(500, self._timeout, index)

    def _try_queue(self):
        if self._index is None:
            return
        user_data = FileDialogSignals.QUEUE_INDEX, self._index
        self._raise("delta > file dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileDialogSignals.TOGGLE_INDEX:
            self._set_index(param)
        if signal == FileDialogSignals.RELEASE_ACTIVATION_HOOK:
            self._try_queue()

    def __init__(self, parent):
        self._parent = parent
        self._index = None
        self._raise("delta > register file dialog object", self)
