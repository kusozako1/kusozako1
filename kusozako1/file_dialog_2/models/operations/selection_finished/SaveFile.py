# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaSaveFile(DeltaEntity):

    def _signal_received(self, param=None):
        if not self._filename:
            return
        directory_gfile = self._enquiry("delta > current gfile")
        gfile = directory_gfile.get_child(self._filename)
        self._raise("delta > dialog response", gfile)
        recent_manager = Gtk.RecentManager.get_default()
        recent_manager.add_item(gfile.get_uri())
        user_data = FileDialogSignals.CLOSE_DIALOG, None
        self._raise("delta > file dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileDialogSignals.SELECTION_FINISHED:
            self._signal_received(param)
        if signal == FileDialogSignals.FILENAME_CHANGED:
            self._filename = param

    def __init__(self, parent):
        self._parent = parent
        self._filename = ""
        self._raise("delta > register file dialog object", self)
