# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaSelectDirectory(DeltaEntity):

    def _signal_received(self, param=None):
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model.get_selected()
        if file_info is not None:
            gfile = self._enquiry("delta > gfile for file info", file_info)
        else:
            gfile = self._enquiry("delta > current gfile")
        self._raise("delta > dialog response", gfile)
        user_data = FileDialogSignals.CLOSE_DIALOG, None
        self._raise("delta > file dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.SELECTION_FINISHED:
            return
        self._signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file dialog object", self)
