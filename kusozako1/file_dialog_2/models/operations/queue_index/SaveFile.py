# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaSaveFile(DeltaEntity):

    def _queue_selection(self, file_info):
        gfile = self._enquiry("delta > gfile for file info", file_info)
        user_data = FileDialogSignals.MOVE_DIRECTORY, gfile
        self._raise("delta > file dialog signal", user_data)

    def _signal_received(self, index):
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model[index]
        if file_info.get_file_type() == Gio.FileType.DIRECTORY:
            self._queue_selection(file_info)

    def receive_transmission(self, user_data):
        signal, index = user_data
        if signal != FileDialogSignals.QUEUE_INDEX:
            return
        self._signal_received(index)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file dialog object", self)
