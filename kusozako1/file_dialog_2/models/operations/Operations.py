# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ActivationHook import DeltaActivationHook
from .ToggleSelected import DeltaToggleSelected
from .UnselectAll import DeltaUnselectAll
from .toggle_index.ToggleIndex import DeltaToggleIndex
from .queue_index.QueueIndex import DeltaQueueIndex
from .selection_finished.SelectionFinished import DeltaSelectionFinished


class EchoOperations:

    def __init__(self, parent):
        DeltaActivationHook(parent)
        DeltaToggleSelected(parent)
        DeltaUnselectAll(parent)
        DeltaToggleIndex(parent)
        DeltaQueueIndex(parent)
        DeltaSelectionFinished(parent)
