# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import ConfigKeys
from kusozako1.file_dialog_2.const import DialogTypes
from .ShowHidden import DeltaShowHidden
from .FileFilter import DeltaFileFilter
# from .Keyword import DeltaKeyword


class DeltaFilterModel(Gtk.FilterListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, parent_model):
        instance = cls(parent)
        instance.construct(parent_model)
        return instance

    def _delta_call_add_filter(self, filter_):
        self._filter.append(filter_)

    def construct(self, parent_model):
        self.set_model(parent_model)

    def __init__(self, parent):
        self._parent = parent
        self._filter = Gtk.EveryFilter.new()
        Gtk.FilterListModel.__init__(
            self,
            filter=self._filter,
            incremental=False,
            )
        DeltaShowHidden(self)
        dialog_type = self._enquiry("delta > config", ConfigKeys.DIALOG_TYPE)
        if dialog_type != DialogTypes.SELECT_DIRECTORY:
            DeltaFileFilter(self)
        # DeltaKeyword(self)
