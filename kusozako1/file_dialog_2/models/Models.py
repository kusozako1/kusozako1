# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel
from .filter_model.FilterModel import DeltaFilterModel
from .sort_model.SortModel import DeltaSortModel
from .SelectionModel import DeltaSelectionModel
from .operations.Operations import EchoOperations


class DeltaModels(DeltaEntity):

    def _delta_info_viewer_model(self):
        return self._viewer_model

    def _delta_info_selection_model(self):
        return self._selection_model

    def _delta_info_gfile_for_file_info(self, file_info):
        return self._base_model.get_gfile_for_file_info(file_info)

    def _delta_info_current_gfile(self):
        return self._base_model.get_file()

    def __init__(self, parent):
        self._parent = parent
        self._base_model = DeltaBaseModel(self)
        filter_model = DeltaFilterModel.new(self, self._base_model)
        sort_model = DeltaSortModel.new(self, filter_model)
        self._viewer_model = Gtk.SingleSelection.new(sort_model)
        self._selection_model = DeltaSelectionModel.new(self, sort_model)
        EchoOperations(self)
        self._raise("delta > loopback models ready", self)
