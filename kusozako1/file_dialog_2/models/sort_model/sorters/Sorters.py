# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from .CollateKey import FoxtrotCollateKey
from .LastAccessed import FoxtrotLastAccessed


class DeltaSorters(DeltaEntity):

    def _set_sorter(self, scheme):
        self._scheme = scheme
        if self._scheme == "file":
            sorter = FoxtrotCollateKey()
        if self._scheme == "recent":
            sorter = FoxtrotLastAccessed()
        self._raise("delta > set sorter", sorter)

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != FileDialogSignals.DIRECTORY_MOVED:
            return
        scheme = gfile.get_uri_scheme()
        if self._scheme != scheme:
            self._set_sorter(scheme)

    def __init__(self, parent):
        self._parent = parent
        current_gfile = self._enquiry("delta > current gfile")
        scheme = current_gfile.get_uri_scheme()
        self._set_sorter(scheme)
        self._raise("delta > register file dialog object", self)
