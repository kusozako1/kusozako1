# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib


class FoxtrotCollateKey(Gtk.CustomSorter):

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_name = alfa.get_display_name()
        alfa_key = GLib.utf8_collate_key_for_filename(alfa_name, -1)
        bravo_name = bravo.get_display_name()
        bravo_key = GLib.utf8_collate_key_for_filename(bravo_name, -1)
        return 1 if alfa_key >= bravo_key else -1

    def __init__(self):
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
