# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotLastAccessed(Gtk.CustomSorter):

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_recent = alfa.get_attribute_int64("recent::modified")
        bravo_recent = bravo.get_attribute_int64("recent::modified")
        return bravo_recent - alfa_recent

    def __init__(self):
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
