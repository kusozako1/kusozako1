# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .sorters.Sorters import DeltaSorters


class DeltaSortModel(Gtk.SortListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def _section_sort_func(self, alfa, bravo, user_data=None):
        return bravo.get_file_type() - alfa.get_file_type()

    def _delta_call_set_sorter(self, sorter):
        self.set_sorter(sorter)

    def construct(self, model):
        self.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SortListModel.__init__(
            self,
            section_sorter=Gtk.CustomSorter.new(self._section_sort_func),
            incremental=False
            )
        DeltaSorters(self)
