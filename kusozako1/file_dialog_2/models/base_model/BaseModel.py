# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from kusozako1.file_dialog_2.const import ConfigKeys


class DeltaBaseModel(Gtk.DirectoryList, DeltaEntity):

    def _get_default_directory(self):
        gfile = self._enquiry("delta > config", ConfigKeys.DEFAULT_DIRECTORY)
        if gfile is not None:
            return gfile
        home_directory = GLib.get_home_dir()
        return Gio.File.new_for_path(home_directory)

    def get_gfile_for_filename(self, filename):
        directory_gfile = self.get_file()
        return directory_gfile.get_child(filename)

    def get_gfile_for_file_info(self, file_info):
        target_uri = file_info.get_attribute_string("standard::target-uri")
        if target_uri is not None:
            return Gio.File.new_for_uri(target_uri)
        filename = file_info.get_name()
        directory_gfile = self.get_file()
        return directory_gfile.get_child(filename)

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != FileDialogSignals.MOVE_DIRECTORY:
            return
        self.set_file(gfile)

    def _notify_loading(self, directory_list, param_spec):
        print("loading ?", directory_list.is_loading())

    def _notify_file(self, directory_list, param_spec):
        gfile = directory_list.get_file()
        user_data = FileDialogSignals.DIRECTORY_MOVED, gfile
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.DirectoryList.__init__(
            self,
            file=self._get_default_directory(),
            attributes="*",
            )
        self.connect("notify::loading", self._notify_loading)
        self.connect("notify::file", self._notify_file)
        self._raise("delta > register file dialog object", self)
