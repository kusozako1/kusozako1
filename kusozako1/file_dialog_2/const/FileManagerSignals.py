# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MOVE_DIRECTORY = "move-directory"           # gfile
DIRECTORY_MOVED = "directory-moved"         # gfile
TOGGLE_INDEX = "toggle-index"               # int
QUEUE_INDEX = "queue-index"                 # int
BACK_TO_BOOKMARK = "back-to-bookmark"       # None
SELECTION_FINISHED = "selection-finished"   # None
CHANGE_CONFIG = "change-config"             # (key, value)
TOGGLE_CONFIG = "toggle-config"             # key
CONFIG_CHANGED = "config-changed"           # (key, value)
SHOW_OVERLAY = "show-overlay"               # (page_name, user_data)
SHOW_PREVIEW = "show-preview"               # file_info
CLOSE_WINDOW = "close-window"               # None
