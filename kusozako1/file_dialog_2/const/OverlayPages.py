# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

GREETER = "greeter"
BLANK = "blank"
CREATE_DIRECTORY = "create-directory"
