# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

TITLE = "title"
DIALOG_TYPE = "dialog_type"
ACCEPT_LABEL = "accept_label"
DEFAULT_DIRECTORY = "default_directory"
DEFAULT_FILENAME = "default_filename"
PIXBUF_FORMATS_ONLY = "pixbuf_formats_only"
