# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

UNSELECT_ALL = "unselect-all"               # None
TOGGLE_SELECTED = "toggle-selected"         # None
TOGGLE_INDEX = "toggle-index"               # index as int
INDEX_TOGGLED = "index-toggled"             # index as int
QUEUE_INDEX = "queue-index"                 # insex as int
RELEASE_ACTIVATION_HOOK = "release-activation-hook"     # None
LONG_PRESS_CANCEL_HOOK = "long-press-cancel-hook"       # None
MOVE_DIRECTORY = "move-directry"            # gfile
DIRECTORY_MOVED = "directory-moved"         # gfile
FILENAME_CHANGED = "filename-changed"       # str
SHOW_PREVIEW = "show-preview"               # file_info
BACK_TO_BOOKMARK = "back-to-bookmark"       # None
SHOW_OVERLAY = "show-overlay"               # page_name, param
TOGGLE_SETTINGS = "toggle-settings"         # key
CHANGE_SETTINGS = "change-settings"         # key, value
SETTINGS_CHANGED = "settings-changed"       # key, value
SELECTION_FINISHED = "selection-finished"   # None
CLOSE_DIALOG = "close-dialog"               # None
