# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# OPEN = "open"
# OPEN_MULTIPLE_FILES = "open-multiple-files"
SELECT_FILE = "select-file"
SELECT_DIRECTORY = "select-directory"
# SELECT_MULTIPLE_DIRECTORIES = "select-multiple-directories"
SAVE_FILE = "save-file"


def is_multiple_selection(type_):
    return False
    # return type_ in (OPEN_MULTIPLE_FILES, SELECT_MULTIPLE_DIRECTORIES)


def has_create_directory_button(type_):
    return type_ in (SELECT_DIRECTORY, SAVE_FILE)
