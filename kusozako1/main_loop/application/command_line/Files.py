# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.util import Mime


class DeltaFiles(DeltaEntity):

    @classmethod
    def new_for_options(cls, parent, options):
        files = cls(parent)
        files.construct(options)
        return files

    def _matches(self, acceptable_mime, file_mime):
        if GLib.pattern_match_simple(acceptable_mime, file_mime):
            return True
        if acceptable_mime == "text/plain" and file_mime.startswith("text"):
            return True
        return False

    def _add_for_list(self, acceptable_mime_list, mime, gfile):
        for acceptable_mime in acceptable_mime_list:
            if self._matches(acceptable_mime, mime):
                self._files.append(gfile)
                break

    def _try_add_file(self, gfile):
        if not gfile.query_exists():
            return
        mime = Mime.from_path(gfile.get_path())
        acceptable_mime = self._enquiry("delta > data", "mime")
        if isinstance(acceptable_mime, str):
            acceptable_mime = [acceptable_mime]
        self._add_for_list(acceptable_mime, mime, gfile)

    def _parse_files(self, file_names, max_files):
        for file_name in file_names:
            gio_file = Gio.File.new_for_commandline_arg(file_name)
            self._try_add_file(gio_file)
            if len(self._files) == max_files:
                break

    def get_files(self):
        return self._files

    def construct(self, options):
        max_files = self._enquiry("delta > data", "max-files")
        if max_files == 0:
            return
        file_names = options.lookup_value(GLib.OPTION_REMAINING)
        if file_names is None:
            return
        self._parse_files(file_names, max_files)

    def __init__(self, parent):
        self._parent = parent
        self._files = []
