# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from gi.repository import Adw
from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .command_line.CommandLine import DeltaCommandLine

STANDARD_FLAGS = Gio.ApplicationFlags.HANDLES_COMMAND_LINE
NON_UNIQUE_FLAGS = STANDARD_FLAGS | Gio.ApplicationFlags.NON_UNIQUE


class DeltaApplication(Adw.Application, DeltaEntity):

    def _delta_call_add_to_container(self, window):
        self.add_window(window)

    def _can_open(self):
        if self._enquiry("delta > data", "non-unique-application"):
            return True
        return not self.get_windows()

    def _on_activate(self, application, parent):
        if self._can_open():
            self._raise("delta > loopback application ready", parent)

    def _delta_call_command_line_ready(self, parent):
        self.connect("activate", self._on_activate, parent)
        self.activate()

    def _delta_info_application(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        if self._enquiry("delta > data", "non-unique-application"):
            flags = NON_UNIQUE_FLAGS
        else:
            flags = STANDARD_FLAGS
        Adw.Application.__init__(
            self,
            application_id=self._enquiry("delta > data", "application-id"),
            flags=flags,
            )
        DeltaCommandLine(self)
        self.run(sys.argv)
