# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .data_layer.DataLayer import DeltaDataLayer
from .application.Application import DeltaApplication
from .window.Window import DeltaWindow


class AlfaMainLoop(DeltaEntity):

    def _delta_call_loopback_application_window_ready(self, parent_object):
        raise NotImplementedError

    def _delta_call_loopback_application_ready(self, parent_object):
        DeltaWindow(parent_object)

    def _delta_call_loopback_data_layer_ready(self, parent_object):
        DeltaApplication(parent_object)

    def _delta_info_data(self, key):
        raise NotImplementedError

    def _delta_info_resource_directory(self):
        return None

    def _delta_info_main_options(self):
        return None

    def _request_synchronization(self, message, user_data=None):
        print("delta > message uncaught", message, user_data)

    def _on_startup(self):
        user_name = GLib.get_user_name()
        print("delta > welcome back {}.".format(user_name))

    def _on_finished(self):
        print("delta > bye...")

    def __init__(self):
        self._parent = None
        self._on_startup()
        DeltaDataLayer(self)
        self._on_finished()
