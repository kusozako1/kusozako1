# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals

TEMPLATE = _("Close Window ?")
DESCRIPTION = _("Please confirm that there are no unfinished operations.")


class DeltaTerminator(DeltaEntity):

    def _on_close(self):
        self._closable = True
        user_data = MainWindowSignals.TRY_CLOSE, None
        self._raise("delta > main window signal", user_data)

    def is_closable(self):
        if not self._closable:
            user_data = MainWindowSignals.SHOW_DIALOG, self._dialog_model
            self._raise("delta > main window signal", user_data)
        return self._closable

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.FORCE_CLOSE:
            return
        self._on_close()

    def __init__(self, parent):
        self._parent = parent
        self._closable = False
        buttons = [
            (_("Cancel"), None),
            (_("Close"), self._on_close)
            ]
        self._dialog_model = {
            "heading": TEMPLATE,
            # "description": DESCRIPTION,
            "buttons": buttons
            }
        self._raise("delta > register main window signal object", self)
