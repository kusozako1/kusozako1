# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from .Terminator import DeltaTerminator


class DeltaCloseRequest(DeltaEntity):

    def _on_close_request(self, window):
        if not self._terminator.is_closable():
            return Gdk.EVENT_STOP
        user_data = MainWindowSignals.ABOUT_TO_CLOSE, None
        self._raise("delta > main window signal", user_data)
        return Gdk.EVENT_PROPAGATE

    def receive_transmission(self, user_data):
        signal, terminator = user_data
        if signal != MainWindowSignals.REPLACE_TERMINATOR:
            return
        self._terminator = terminator

    def __init__(self, parent):
        self._parent = parent
        self._terminator = DeltaTerminator(self)
        application_window = self._enquiry("delta > application window")
        application_window.connect("close-request", self._on_close_request)
        self._raise("delta > register main window signal object", self)
