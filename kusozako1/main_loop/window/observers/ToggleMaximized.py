# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaToggleMaximized(AlfaObserver):

    SIGNAL = MainWindowSignals.TOGGLE_MAXIMIZED

    def _on_signal_received(self, param=None):
        user_data = "window", "is_maximized"
        self._raise("delta > toggle boolean settings", user_data)
