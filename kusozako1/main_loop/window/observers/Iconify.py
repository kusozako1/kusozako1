# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaIconify(AlfaObserver):

    SIGNAL = MainWindowSignals.ICONIFY

    def _on_signal_received(self, param=None):
        application_window = self._enquiry("delta > application window")
        application_window.minimize()
