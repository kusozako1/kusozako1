# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaChangeTitle(AlfaObserver):

    SIGNAL = MainWindowSignals.CHANGE_TITLE

    def _on_signal_received(self, title=None):
        application_name = self._enquiry("delta > data", "application-name")
        if title is None:
            window_title = application_name
        else:
            window_title = "{} — {}".format(title, application_name)
        application_window = self._enquiry("delta > application window")
        application_window.set_title(window_title)
