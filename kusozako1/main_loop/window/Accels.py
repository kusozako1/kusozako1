# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage

KEYBINDS = {
    "F10": (MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU),
    "F11": (MainWindowSignals.TOGGLE_FULLSCREEN, None),
    "Shift+F11": (MainWindowSignals.TOGGLE_MAXIMIZED, None),
    "Ctrl+K": (MainWindowSignals.SHOW_OVERLAY, OverlayPage.SHORTCUTS),
    "Ctrl+Q": (MainWindowSignals.TRY_CLOSE, None),
}


class DeltaAccels(Gtk.EventControllerKey, DeltaEntity):

    def _on_key_released(self, controller_key, keyval, keycode, state):
        accel = Gtk.accelerator_get_label(keyval, state)
        user_data = self._keybinds.get(accel, None)
        if user_data is None:
            return
        self._raise("delta > main window signal", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != MainWindowSignals.ADD_GLOBAL_ACCEL:
            return
        shortcut, signal, signal_param = param
        self._keybinds[shortcut] = (signal, signal_param)

    def _callback(self, widget, args):
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.SHORTCUTS
        self._raise("delta > main window signal", user_data)

    def _add_shortcuts(self):
        shortcut_trigger = Gtk.ShortcutTrigger.parse_string("<Ctrl>K")
        callback_action = Gtk.CallbackAction.new(self._callback)
        shortcut = Gtk.Shortcut.new(shortcut_trigger, callback_action)
        window = self._enquiry("delta > application window")
        window.add_shortcut(shortcut)

    def __init__(self, parent):
        self._parent = parent
        # self._add_shortcuts()
        self._keybinds = KEYBINDS.copy()
        Gtk.EventControllerKey.__init__(self)
        self.connect("key-released", self._on_key_released)
        self._raise("delta > add controller", self)
        self._raise("delta > register main window signal object", self)
