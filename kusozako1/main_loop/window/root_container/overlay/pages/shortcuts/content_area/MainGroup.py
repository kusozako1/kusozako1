# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaMainGroup(Gtk.Box, DeltaEntity):

    def _get_shortcut(self, accelerator):
        shortcut = Gtk.ShortcutsShortcut(accelerator=accelerator)
        return shortcut

    def add_shortcut(self, accelerator, title):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, homogeneous=True)
        sub_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        sub_box.append(Gtk.Box(hexpand=True))
        sub_box.append(self._get_shortcut(accelerator))
        box.append(sub_box)
        box.append(Gtk.ShortcutsShortcut(title=title))
        self.append(box)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=16
            )
        label = Gtk.Label(label=_("Main"), xalign=0.5, margin_top=32)
        label.add_css_class("heading")
        self.append(label)
        self.add_shortcut("F10", _("Show/Close Primary Menu"))
        self.add_shortcut("<Shift>F11", _("Toggle Window Maximized"))
        self.add_shortcut("F11", _("Toggle Fullscreen"))
        self.add_shortcut("<Ctrl>K", _("Show Shortcuts List"))
        self.add_shortcut("<Ctrl>Q", _("Quit Application"))
        self._raise("delta > add shortcut group", ("main", self))
