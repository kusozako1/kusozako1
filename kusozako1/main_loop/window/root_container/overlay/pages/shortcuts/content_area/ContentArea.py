# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from .MainGroup import DeltaMainGroup
from .ShortcutGroup import DeltaShortCutGroup


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_shortcut_group(self, user_data):
        group_name, shortcut_group = user_data
        self._shortcut_groups[group_name] = shortcut_group
        self.append(shortcut_group)

    def _add_shortcut_group(self, name, label):
        if name not in self._shortcut_groups:
            DeltaShortCutGroup.new(self, name, label)

    def _add_shortcut(self, name, accelerator, title):
        shortcut_group = self._shortcut_groups.get(name, None)
        if shortcut_group is None:
            shortcut_group = DeltaShortCutGroup.new(self, name, name)
        shortcut_group.add_shortcut(accelerator, title)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == MainWindowSignals.ADD_SHORTCUT_GROUP:
            self._add_shortcut_group(*param)
        if signal == MainWindowSignals.ADD_SHORTCUT:
            self._add_shortcut(*param)

    def __init__(self, parent):
        self._parent = parent
        self._shortcut_groups = {}
        scrolled_window = Gtk.ScrolledWindow(hexpand=True)
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=True,
            )
        self.add_css_class("osd")
        DeltaMainGroup(self)
        scrolled_window.set_child(self)
        self._raise("delta > register main window signal object", self)
        self._raise("delta > add to container center", scrolled_window)
