# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaShortCutGroup(Gtk.Box, DeltaEntity):

    @classmethod
    def new(cls, parent, group_name, label):
        shortcut_group = cls(parent)
        shortcut_group.construct(group_name, label)
        return shortcut_group

    def add_shortcut(self, accelerator, title):
        if title in self._titles:
            return
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, homogeneous=True)
        sub_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        sub_box.append(Gtk.Box(hexpand=True))
        sub_box.append(Gtk.ShortcutsShortcut(accelerator=accelerator))
        box.append(sub_box)
        box.append(Gtk.ShortcutsShortcut(title=title))
        self.append(box)
        self._titles.append(title)

    def construct(self, group_name, label_text):
        label = Gtk.Label(label=label_text, xalign=0.5, margin_top=32)
        label.add_css_class("heading")
        self.append(label)
        self._raise("delta > add shortcut group", (group_name, self))

    def __init__(self, parent):
        self._parent = parent
        self._titles = []
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=16
            )
