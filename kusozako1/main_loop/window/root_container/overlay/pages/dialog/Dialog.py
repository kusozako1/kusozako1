# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from .Spacer import DeltaSpacer
from .content_area.ContentArea import DeltaContentArea


class DeltaDialog(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _on_map(self, box):
        return False

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.Box.__init__(
            self,
            hexpand=True,
            vexpand=True,
            orientation=Gtk.Orientation.VERTICAL,
            )
        DeltaSpacer(self)
        DeltaContentArea(self)
        DeltaSpacer(self)
        scrolled_window.set_child(self)
        user_data = scrolled_window, OverlayPage.DIALOG
        self._raise("delta > add to stack", user_data)
        self.connect("map", self._on_map)
