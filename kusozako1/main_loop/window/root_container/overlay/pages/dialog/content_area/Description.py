# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaDescription(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, model = user_data
        if signal != MainWindowSignals.SHOW_DIALOG:
            return
        title = model.get("description", "")
        self.set_text(title)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, margin_bottom=16)
        # self.add_css_class("title-4")
        self._raise("delta > add to container", self)
        self._raise("delta > register main window signal object", self)
