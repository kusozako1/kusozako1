# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from .FirstButton import AlfaFirstButton
from .LastButton import AlfaLastButton


class DeltaButton(Gtk.Button, DeltaEntity, AlfaFirstButton, AlfaLastButton):

    @classmethod
    def new(cls, parent, model, index, length):
        button = cls(parent)
        button.construct(model, index, length)
        return button

    def _close(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _on_clicked(self, button, callback, callback_data=None):
        self._close()
        if callback_data is None:
            callback()
        else:
            callback(callback_data)

    def _set_callback(self, model):
        callback = model[1] if len(model) >= 2 else None
        if callback is None:
            callback = self._close
        callback_data = model[2] if len(model) == 3 else None
        self.connect("clicked", self._on_clicked, callback, callback_data)

    def construct(self, model, index, length):
        self.props.label = model[0]
        self._set_callback(model)
        if index == 0:
            AlfaFirstButton.__init__(self)
        if index == length:
            AlfaLastButton.__init__(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.set_size_request(160, -1)
        self.add_css_class("kusozako-primary-widget")
