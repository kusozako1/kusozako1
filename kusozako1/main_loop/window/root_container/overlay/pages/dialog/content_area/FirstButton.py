# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class AlfaFirstButton:

    def _on_map(self, button):
        button.grab_focus()

    def _on_pressed(self, controller_key, keyval, keycode, state):
        accel = Gtk.accelerator_get_label(keyval, state)
        return accel not in ("Right", "Return", "Space")

    def __init__(self):
        self.connect("map", self._on_map)
        controller_key = Gtk.EventControllerKey.new()
        controller_key.connect("key-pressed", self._on_pressed)
        self.add_controller(controller_key)
