# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Apply import DeltaApply


class EchoObservers:

    def __init__(self, parent):
        DeltaApply(parent)
