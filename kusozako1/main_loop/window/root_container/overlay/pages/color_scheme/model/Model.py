# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .ColorSchemeEntity import DeltaColorSchemeEntity
from .Data import DATA
from .observers.Observers import EchoObservers


class DeltaModel(Gio.ListStore, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=DeltaColorSchemeEntity)
        for scheme_data in DATA:
            color_scheme_entity = DeltaColorSchemeEntity.new(self, scheme_data)
            self.append(color_scheme_entity)
        EchoObservers(self)
