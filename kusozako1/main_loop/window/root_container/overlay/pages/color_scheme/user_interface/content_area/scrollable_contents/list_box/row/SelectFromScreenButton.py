# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
from kusozako1.Entity import DeltaEntity

FLAGS_IN_OUT = Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDIN_PIPE
SLURP = ["slurp", "-p", "-b", "00aaff00"]
GRIM = ["grim", "-g", "", "-t", "ppm", "-"]
CONVERT = ["convert", "-", "-format", "%[pixel:p{0,0}]", "txt:-"]


class DeltaSelectFromScreenButton(Gtk.Button, DeltaEntity):

    @classmethod
    def new(cls, parent, color_scheme_entity):
        instance = cls(parent)
        instance.construct(color_scheme_entity)

    def _on_clicked(self, button, color_scheme_entity):
        slurp = Gio.Subprocess.new(SLURP, Gio.SubprocessFlags.STDOUT_PIPE)
        _, slurp_out, _ = slurp.communicate_utf8(None, None)
        if not slurp_out:
            return
        command = GRIM.copy()
        command[2] = slurp_out.replace("\n", "")
        grim = Gio.Subprocess.new(command, Gio.SubprocessFlags.STDOUT_PIPE)
        _, grim_out, _ = grim.communicate(None, None)
        convert = Gio.Subprocess.new(CONVERT, FLAGS_IN_OUT)
        _, convert_out, _ = convert.communicate(grim_out, None)
        selected_color = convert_out.get_data().decode("utf8").split(" ")[7]
        rgba = Gdk.RGBA()
        rgba.parse(selected_color)
        color_scheme_entity.set_rgba(rgba)

    def construct(self, color_scheme_entity):
        self.connect("clicked", self._on_clicked, color_scheme_entity)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            icon_name="color-select-symbolic",
            margin_top=8,
            margin_bottom=8,
            margin_start=16,
            margin_end=16,
            has_frame=False,
            tooltip_text=_("Pick Color from Screen"),
            )
        self.add_css_class("kusozako-primary-widget")
        self._raise("delta > add to container", self)
