# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .row.Row import DeltaRow


class DeltaListBox(Gtk.ListBox, DeltaEntity):

    def _create_widget_func(self, color_scheme_entity, user_data=None):
        return DeltaRow.new(self, color_scheme_entity)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListBox.__init__(self)
        self.bind_model(
            self._enquiry("delta > model"),
            self._create_widget_func,
            None,
            )
        self.add_css_class("boxed-list")
        self._raise("delta > add to container", self)
