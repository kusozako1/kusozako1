# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .FileButton import AlfaFileButton


class DeltaSaveButton(AlfaFileButton):

    START_ICON = "document-save-symbolic"
    LABEL = _("Save")
    SHORTCUT = "Ctrl+S"
    SIGNAL = MainWindowSignals.TRY_SAVE_FILE
