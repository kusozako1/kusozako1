# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .FindButton import DeltaFindButton


class DeltaEditItems(DeltaEntity):

    def _try_set_find_button(self):
        if self._enquiry("delta > data", "use-global-new"):
            DeltaFindButton(self)
            return 1
        return 0

    def __init__(self, parent):
        self._parent = parent
        buttons = 0
        buttons += self._try_set_find_button()
        if buttons > 0:
            DeltaSeparator(self)
