# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# from gi.repository import Gtk
# from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaShortCutsButton(AlfaButton):

    START_ICON = "preferences-desktop-keyboard-shortcuts-symbolic"
    LABEL = _("Shortcuts")
    SHORTCUT = "Ctrl+K"

    def _on_clicked(self, *args):
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.SHORTCUTS
        self._raise("delta > main window signal", user_data)
