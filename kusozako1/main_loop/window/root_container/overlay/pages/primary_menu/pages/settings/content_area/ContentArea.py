# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.Label import DeltaLabel
from .BackgroundStyleButton import DeltaBackgroundStyleButton
from .SelectBackgroundImageButton import DeltaSelectBackgroundImageButton
from .OpacityScale import DeltaOpacityScale


class EchoContentArea:

    def __init__(self, parent):
        DeltaLabel.new_for_label(parent, _("Content Area"))
        DeltaBackgroundStyleButton(parent)
        DeltaSelectBackgroundImageButton(parent)
        DeltaOpacityScale(parent)
