# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaToggleFullscreenButton(AlfaButton):

    SHORTCUT = "F11"

    def _on_clicked(self, button):
        user_data = MainWindowSignals.TOGGLE_FULLSCREEN, None
        self._raise("delta > main window signal", user_data)

    def _refresh(self, is_maximized):
        label = "Unfullscreen" if is_maximized else "Fullscreen"
        self._label.set_label(label)

    def _on_initialize(self):
        query = "window", "is_fullscreen", False
        is_maximized = self._enquiry("delta > settings", query)
        self._refresh(is_maximized)
        self._raise("delta > register settings object", self)

    def receive_transmission(self, user_data):
        group, key, is_maximized = user_data
        if group == "window" and key == "is_fullscreen":
            self._refresh(is_maximized)
