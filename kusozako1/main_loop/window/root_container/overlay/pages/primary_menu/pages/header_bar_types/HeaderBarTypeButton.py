# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.SettingsButton import AlfaSettingsButton


class AlfaHeaderBarTypeButton(AlfaSettingsButton):

    GROUP = "header_bar"
    KEY = "container_type"
