# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPrimaryMenuPage
from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .CloseMenuButton import DeltaCloseMenuButton
from .file_items.FileItems import DeltaFileItems
from .ExtraMenuBox import DeltaExtraMenuBox
from .standard_items.StandardItems import EchoStandardItems


class DeltaPortal(Gtk.ScrolledWindow, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self, propagate_natural_width=True)
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add_css_class("osd")
        DeltaCloseMenuButton(self)
        DeltaSeparator(self)
        DeltaFileItems(self)
        DeltaExtraMenuBox(self)
        EchoStandardItems(self)
        self.set_child(self._box)
        user_data = self, OverlayPrimaryMenuPage.PORTAL
        self._raise("delta > add to stack", user_data)
