# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaExtraMenuBox(Gtk.Box, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, widget = user_data
        if signal != MainWindowSignals.ADD_EXTRA_MENU:
            return
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._raise("delta > add to container", self)
        self._raise("delta > register main window signal object", self)
