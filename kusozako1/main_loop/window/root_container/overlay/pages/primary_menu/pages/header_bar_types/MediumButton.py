# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import HeaderBarType
from .HeaderBarTypeButton import AlfaHeaderBarTypeButton


class DeltaMediumButton(AlfaHeaderBarTypeButton):

    LABEL = _("Medium")
    MATCH_VALUE = HeaderBarType.MEDIUM
