# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .portal.Portal import DeltaPortal
from .settings.Settings import DeltaSettings
from .header_bar_types.HeaderBarTypes import DeltaHeaderBarTypes
from .background_styles.BackgroundStyles import DeltaBackgroudStyles


class EchoPages:

    def __init__(self, parent):
        DeltaPortal(parent)
        DeltaSettings(parent)
        DeltaHeaderBarTypes(parent)
        DeltaBackgroudStyles(parent)
