# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.overlay_item.scale.Scale import DeltaScale


class DeltaOpacityScale(DeltaEntity):

    MODEL = {
        "label": _("Opacity : {:.0%}"),
        "query": ("content_area", "opacity"),
        "default": 0.9,
        "max": 1,
        "min": 0.2
    }

    def __init__(self, parent):
        self._parent = parent
        scale = DeltaScale.new_for_model(parent, self.MODEL)
        self._raise("delta > add to container", scale)
