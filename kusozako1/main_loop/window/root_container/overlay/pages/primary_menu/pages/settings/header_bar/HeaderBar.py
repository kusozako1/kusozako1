# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.Label import DeltaLabel
from .TypeButton import DeltaTypeButton
from .OpacityScale import DeltaOpacityScale


class EchoHeaderBar:

    def __init__(self, parent):
        DeltaLabel.new_for_label(parent, _("Header Bar"))
        DeltaTypeButton(parent)
        DeltaOpacityScale(parent)
