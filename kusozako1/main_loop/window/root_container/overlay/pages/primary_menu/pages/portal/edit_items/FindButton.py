# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .FileButton import AlfaFileButton


class DeltaNewButton(AlfaFileButton):

    START_ICON = "edit-find-symbolic"
    LABEL = _("Find")
    SHORTCUT = "Ctrl+F"
    SIGNAL = MainWindowSignals.EDIT_FIND
