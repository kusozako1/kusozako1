# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .BackgroundStyleButton import AlfaBackgroundStyleButton


class DeltaContainButton(AlfaBackgroundStyleButton):

    LABEL = _("Contain")
    MATCH_VALUE = "contain"
