# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .NewButton import DeltaNewButton
from .OpenButton import DeltaOpenButton
from .SaveButton import DeltaSaveButton
from .SaveAsButton import DeltaSaveAsButton


class DeltaFileItems(DeltaEntity):

    def _try_set_new_button(self):
        if self._enquiry("delta > data", "use-global-new"):
            DeltaNewButton(self)
            return 1
        return 0

    def _try_set_open_button(self):
        if self._enquiry("delta > data", "max-files") > 0:
            DeltaOpenButton(self)
            return 1
        return 0

    def _try_set_save_button(self):
        if self._enquiry("delta > data", "use-global-save"):
            DeltaSaveButton(self)
            return 1
        return 0

    def _try_set_save_as_button(self):
        if self._enquiry("delta > data", "use-global-save-as"):
            DeltaSaveAsButton(self)
            return 1
        return 0

    def __init__(self, parent):
        self._parent = parent
        buttons = 0
        buttons += self._try_set_new_button()
        buttons += self._try_set_open_button()
        buttons += self._try_set_save_button()
        buttons += self._try_set_save_as_button()
        if buttons > 0:
            DeltaSeparator(self)
