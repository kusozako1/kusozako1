# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.const import UserSpecialDirectories
from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog


class DeltaSelectBackgroundImageButton(AlfaButton):

    LABEL = _("Select Background Image")

    def _delta_call_dialog_response(self, gfile):
        path = gfile.get_path()
        user_data = "css", "window_background_image", path
        self._raise("delta > settings", user_data)

    def _on_clicked(self, button):
        gfile = Gio.File.new_for_path(UserSpecialDirectories.PICTURES)
        DeltaFileDialog.select_file(
            self,
            title=_("Select Background Image"),
            pixbuf_formats_only=True,
            default_directory=gfile,
            )
