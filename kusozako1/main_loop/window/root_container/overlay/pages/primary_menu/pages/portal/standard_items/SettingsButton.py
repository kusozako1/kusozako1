# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import OverlayPrimaryMenuPage


class DeltaSettingsButton(AlfaButton):

    START_ICON = "preferences-other-symbolic"
    LABEL = _("Settings")
    END_ICON = "go-next-symbolic"

    def _on_clicked(self, button):
        self._raise("delta > move page to", OverlayPrimaryMenuPage.SETTINGS)
