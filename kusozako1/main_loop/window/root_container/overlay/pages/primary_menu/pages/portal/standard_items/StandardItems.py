# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from .SettingsButton import DeltaSettingsButton
from .IconifyButton import DeltaIconifyButton
from .ToggleMaximizedButton import DeltaToggleMaximizedButton
from .ToggleFullscreenButton import DeltaToggleFullscreenButton
from .ShortcutsButton import DeltaShortCutsButton
from .AboutButton import DeltaAboutButton
from .QuitApplicationButton import DeltaQuitApplicationButton


class EchoStandardItems:

    def __init__(self, parent):
        DeltaSettingsButton(parent)
        DeltaSeparator(parent)
        DeltaIconifyButton(parent)
        DeltaToggleMaximizedButton(parent)
        DeltaToggleFullscreenButton(parent)
        DeltaSeparator(parent)
        DeltaShortCutsButton(parent)
        DeltaAboutButton(parent)
        DeltaQuitApplicationButton(parent)
