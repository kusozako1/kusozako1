# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals


class DeltaAboutButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, *args):
        self._raise(
            "delta > main window signal",
            (MainWindowSignals.SHOW_OVERLAY, OverlayPage.ABOUT_APPLICATION)
            )

    def __init__(self, parent):
        self._parent = parent
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        image = Gtk.Image.new_from_icon_name("")
        box.append(image)
        label = Gtk.Label(
            label=_("About"),
            xalign=0,
            hexpand=True,
            margin_start=8,
            margin_end=8,
            )
        box.append(label)
        Gtk.Button.__init__(self, has_frame=False)
        self.set_child(box)
        self.add_css_class("kusozako-primary-widget")
        self.add_css_class("flat")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
