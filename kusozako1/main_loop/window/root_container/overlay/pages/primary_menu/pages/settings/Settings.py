# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import OverlayPrimaryMenuPage
from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako1.overlay_item.Separator import DeltaSeparator
from .ColorSchemeButton import DeltaColorSchemeButton
from .header_bar.HeaderBar import EchoHeaderBar
from .content_area.ContentArea import EchoContentArea


class DeltaSettings(AlfaSubPage):

    PAGE_NAME = OverlayPrimaryMenuPage.SETTINGS

    def _on_initialize(self):
        DeltaColorSchemeButton(self)
        DeltaSeparator(self)
        EchoHeaderBar(self)
        DeltaSeparator(self)
        EchoContentArea(self)

    def _add_to_container(self):
        user_data = self, OverlayPrimaryMenuPage.SETTINGS
        self._raise("delta > add to stack", user_data)
