# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const.OverlayPrimaryMenuPage import BACKGROUND_STYLES


class DeltaBackgroundStyleButton(AlfaButton):

    END_ICON = "go-next-symbolic"
    LABEL = _("Style")

    def _on_clicked(self, button):
        self._raise("delta > move page to", BACKGROUND_STYLES)
