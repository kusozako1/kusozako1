# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class AlfaObserver(DeltaEntity):

    SIGNAL = "define signal to handle here."

    def _on_signal_received(self, param=None):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.SIGNAL:
            self._on_signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
