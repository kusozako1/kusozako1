# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaAddExtraPrimaryMenu(AlfaObserver):

    SIGNAL = MainWindowSignals.ADD_EXTRA_PRIMARY_MENU

    def _on_signal_received(self, user_data):
        # widget, name = user_data
        self._raise("delta > add to stack", user_data)
