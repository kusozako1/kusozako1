# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage
from .Observer import AlfaObserver


class DeltaShowExtraPrimaryMenuWithParam(AlfaObserver):

    SIGNAL = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM

    def _ensure(self):
        param = OverlayPage.PRIMARY_MENU, None
        user_data = MainWindowSignals.SHOW_EXTRA_OVERLAY, param
        self._raise("delta > main window signal", user_data)

    def _on_signal_received(self, user_data):
        self._ensure()
        page_name, _ = user_data
        self._raise("delta > move page to", page_name)
