# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPrimaryMenuPage
from .Observer import AlfaObserver


class DeltaCloseOverlay(AlfaObserver):

    SIGNAL = MainWindowSignals.CLOSE_OVERLAY

    def _on_signal_received(self, param=None):
        self._raise("delta > move page to", OverlayPrimaryMenuPage.PORTAL)
