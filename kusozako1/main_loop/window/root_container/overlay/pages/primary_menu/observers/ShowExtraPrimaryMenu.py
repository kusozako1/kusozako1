# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaShowExtraPrimaryMenu(AlfaObserver):

    SIGNAL = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU

    def _on_signal_received(self, page_name):
        self._raise("delta > move page to", page_name)
