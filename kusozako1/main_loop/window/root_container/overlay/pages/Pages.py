# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Blank import DeltaBlank
from .about_application.AboutApplication import DeltaAboutApplication
from .Greeter import DeltaGreeter
from .primary_menu.PrimaryMenu import DeltaPrimaryMenu
from .color_scheme.ColorScheme import DeltaColorScheme
from .dialog.Dialog import DeltaDialog
from .shortcuts.Shortcuts import DeltaShortCuts


class EchoPages:

    def __init__(self, parent):
        DeltaBlank(parent)
        DeltaGreeter(parent)
        DeltaShortCuts(parent)
        DeltaPrimaryMenu(parent)
        DeltaColorScheme(parent)
        DeltaAboutApplication(parent)
        DeltaDialog(parent)
