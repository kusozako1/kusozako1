# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _get_license_text(self):
        names = [GLib.path_get_dirname(__file__), "GPL3.txt"]
        path = GLib.build_filenamev(names)
        _, bytes_ = GLib.file_get_contents(path)
        return bytes_.decode("utf-8")

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(
            vexpand=True,
            hexpand=True,
            propagate_natural_width=True
            )
        Gtk.Label.__init__(self, label=self._get_license_text())
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
