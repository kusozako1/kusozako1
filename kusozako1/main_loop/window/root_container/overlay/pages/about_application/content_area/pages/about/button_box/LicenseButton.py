# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaLicenseButton(AlfaButton):

    END_ICON = "go-next-symbolic"
    LABEL = _("License (GPL v3)")

    def _on_clicked(self, button):
        self._raise("delta > move page to", "license")

    def _on_initialize(self):
        self._label.props.xalign = 1
