# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaIcon(Gtk.Image, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(
            self,
            icon_name=self._enquiry("delta > data", "rdnn-name"),
            pixel_size=128,
            )
        self._raise("delta > add to container", self)
