# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Label import DeltaLabel
from .BackButton import DeltaBackButton


class DeltaLicense(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._inner_box.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._inner_box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=32,
            margin_top=16,
            margin_bottom=16,
            margin_start=16,
            margin_end=16,
            )
        self.append(self._inner_box)
        self.add_css_class("osd")
        DeltaLabel(self)
        DeltaBackButton(self)
        self._raise("delta > add to stack", (self, "license"))
