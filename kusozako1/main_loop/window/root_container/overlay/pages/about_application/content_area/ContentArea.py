# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .pages.Pages import EchoPages
from .OverlayClosed import DeltaOverlayClosed


class DeltaContentArea(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_move_page_to(self, page_name):
        self.set_visible_child_name(page_name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            hhomogeneous=False,
            transition_type=Gtk.StackTransitionType.SLIDE_LEFT_RIGHT,
            transition_duration=400,
            )
        EchoPages(self)
        DeltaOverlayClosed(self)
        self._raise("delta > add to container", self)
