# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .about.About import DeltaAbout
from .license.License import DeltaLicense


class EchoPages:

    def __init__(self, parent):
        DeltaAbout(parent)
        DeltaLicense(parent)
