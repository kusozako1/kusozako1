# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Icon import DeltaIcon
from .ApplicationName import DeltaApplicationName
from .VersionNumber import DeltaVersionNumber
from .Description import DeltaDescription
from .WebSite import DeltaWebSite
from .Copyright import DeltaCopyright
from .License import DeltaLicense
from .button_box.ButtonBox import DeltaButtonBox


class DeltaAbout(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._inner_box.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._inner_box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL,
            spacing=32,
            margin_top=16,
            margin_bottom=16,
            margin_start=16,
            margin_end=16,
            )
        self.append(self._inner_box)
        self.add_css_class("osd")
        DeltaIcon(self)
        DeltaApplicationName(self)
        DeltaVersionNumber(self)
        DeltaDescription(self)
        DeltaWebSite(self)
        DeltaCopyright(self)
        DeltaLicense(self)
        self._inner_box.append(Gtk.Box(vexpand=True, hexpand=True))
        DeltaButtonBox(self)
        self._raise("delta > add to stack", (self, "about"))
