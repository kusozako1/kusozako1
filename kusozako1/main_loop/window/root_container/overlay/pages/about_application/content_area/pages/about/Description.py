# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaDescription(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            justify=Gtk.Justification.CENTER,
            label=self._enquiry("delta > data", "long-description"),
            )
        self.add_css_class("body")
        self._raise("delta > add to container", self)
