# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ShowOverlay import DeltaShowOverlay
from .CloseOverlay import DeltaCloseOverlay
from .ShowDialog import DeltaShowDialog
from .AddExtraOverlay import DeltaAddExtraOverlay
from .ShowExtraOverlay import DeltaShowExtraOverlay
from .GreeterClosed import DeltaGreeterClosed


class EchoObservers:

    def __init__(self, parent):
        DeltaShowOverlay(parent)
        DeltaShowDialog(parent)
        DeltaCloseOverlay(parent)
        DeltaAddExtraOverlay(parent)
        DeltaShowExtraOverlay(parent)
        DeltaGreeterClosed(parent)
