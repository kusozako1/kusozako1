# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage
from .Observer import AlfaObserver


class DeltaCloseOverlay(AlfaObserver):

    SIGNAL = MainWindowSignals.CLOSE_OVERLAY

    def _timeout(self):
        user_data = MainWindowSignals.OVERLAY_CLOSED, None
        self._raise("delta > main window signal", user_data)

    def _on_signal_received(self, page_name):
        self._raise("delta > switch stack to", OverlayPage.BLANK)
        GLib.timeout_add(800, self._timeout)
