# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage
from .Observer import AlfaObserver


class DeltaShowDialog(AlfaObserver):

    SIGNAL = MainWindowSignals.SHOW_DIALOG

    def _on_signal_received(self, model):
        stack = self._enquiry("delta > stack")
        stack.show()
        stack.set_visible_child_name(OverlayPage.DIALOG)
