# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .containers.Containers import EchoContainers
from .controllers.Controllers import EchoControllers
from .settings.Settings import EchoSettings


class DeltaHeaderBar(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def _delta_info_header_bar(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self, vhomogeneous=False)
        EchoContainers(self)
        EchoControllers(self)
        EchoSettings(self)
        self._raise("delta > add to container", self)
