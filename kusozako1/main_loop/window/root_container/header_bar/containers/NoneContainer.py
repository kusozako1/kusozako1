# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import CssName
from kusozako1.const import HeaderBarType


class DeltaNoneContainer(Gtk.CenterBox, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            css_classes=[CssName.PRIMARY_CONTAINER],
            )
        self._raise("delta > add to stack", (self, HeaderBarType.NONE))
