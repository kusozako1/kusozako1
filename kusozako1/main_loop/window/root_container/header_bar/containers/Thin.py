# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import HeaderBarType


class DeltaThin(Gtk.CenterBox, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.add_css_class("kusozako-primary-container")
        self.set_size_request(-1, 24)
        self._raise("delta > add to stack", (self, HeaderBarType.THIN))
