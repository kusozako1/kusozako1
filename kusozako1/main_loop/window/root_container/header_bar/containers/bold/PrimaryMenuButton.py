# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.const import CssName
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals


class DeltaPrimaryMenuButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            icon_name="open-menu-symbolic",
            css_classes=[CssName.PRIMARY_WIDGET],
            has_frame=False,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container end", self)
