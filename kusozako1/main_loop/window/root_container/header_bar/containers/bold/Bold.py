# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import HeaderBarType
from .OpenButton import DeltaOpenButton
from .TitleBar import DeltaTitleBar
from .PrimaryMenuButton import DeltaPrimaryMenuButton


class DeltaBold(Gtk.CenterBox, DeltaEntity):

    def _delta_call_add_to_container_start(self, widget):
        self.set_start_widget(widget)

    def _delta_call_add_to_container_center(self, widget):
        self.set_center_widget(widget)

    def _delta_call_add_to_container_end(self, widget):
        self.set_end_widget(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.add_css_class("kusozako-primary-container")
        self.set_size_request(-1, 48)
        if self._enquiry("delta > data", "max-files") > 0:
            DeltaOpenButton(self)
        DeltaTitleBar(self)
        DeltaPrimaryMenuButton(self)
        self._raise("delta > add to stack", (self, HeaderBarType.BOLD))
