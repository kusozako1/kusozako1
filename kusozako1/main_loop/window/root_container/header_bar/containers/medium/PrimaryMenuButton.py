# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals
from .Button import AlfaButton


class DeltaPrimaryMenuButton(AlfaButton):

    ICON_NAME = "open-menu-symbolic"
    REGISTRATION_MESSAGE = "delta > add to container start"
    SIGNAL_DATA = MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU
