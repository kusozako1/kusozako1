# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Button import AlfaButton


class DeltaCloseButton(AlfaButton):

    ICON_NAME = "window-close-symbolic"
    SIGNAL_DATA = MainWindowSignals.TRY_CLOSE, None
