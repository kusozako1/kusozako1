# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity

EDGE = Gdk.SurfaceEdge

EDGES = [
    [EDGE.NORTH_WEST, EDGE.NORTH, EDGE.NORTH_EAST],
    [EDGE.WEST, "default", EDGE.EAST],
    [EDGE.WEST, "default", EDGE.EAST]
    ]

BORDER_WIDTH = 4
BUTTON = 1


class DeltaGestureDrag(Gtk.GestureDrag, DeltaEntity):

    def _get_index(self, size, position):
        if BORDER_WIDTH > position:
            return 0
        elif size-BORDER_WIDTH > position > BORDER_WIDTH:
            return 1
        return 2

    def _on_drag_update(self, gesture_drag, offset_x, offset_y):
        widget = gesture_drag.get_widget()
        _, x, y = gesture_drag.get_start_point()
        x_index = self._get_index(widget.get_allocated_width(), x)
        y_index = self._get_index(widget.get_allocated_height(), y)
        edge = EDGES[y_index][x_index]
        surface = widget.get_native().get_surface()
        device = gesture_drag.get_device()
        if edge == "default":
            surface.begin_move(device, BUTTON, x, y, Gdk.CURRENT_TIME)
        else:
            surface.begin_resize(edge, device, BUTTON, x, y, Gdk.CURRENT_TIME)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureDrag.__init__(self)
        self.connect("drag-update", self._on_drag_update)
        self._raise("delta > add controller", self)
