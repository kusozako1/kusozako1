# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaGestureClick(Gtk.GestureClick, DeltaEntity):

    def _on_pressed(self, gesture_click, n_press, x, y):
        if n_press == 2:
            user_data = "window", "is_maximized"
            self._raise("delta > toggle boolean settings", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(self, button=1)
        self.connect("pressed", self._on_pressed)
        self._raise("delta > add controller", self)
