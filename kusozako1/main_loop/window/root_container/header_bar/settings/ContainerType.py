# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.alfa.SettingsEntity import AlfaSettingsEntity
from kusozako1.const import HeaderBarType


class DeltaContainerType(AlfaSettingsEntity):

    GROUP = "header_bar"
    KEY = "container_type"
    DEFAULT = HeaderBarType.BOLD

    def _reset(self, visible_child_name):
        stack = self._enquiry("delta > header bar")
        stack.set_visible_child_name(visible_child_name)
