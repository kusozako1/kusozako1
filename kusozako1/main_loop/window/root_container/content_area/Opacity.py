# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.alfa.SettingsEntity import AlfaSettingsEntity


class DeltaOpacity(AlfaSettingsEntity):

    GROUP = "content_area"
    KEY = "opacity"
    DEFAULT = 0.9

    def _reset(self, opacity):
        inner_box = self._enquiry("delta > inner box")
        inner_box.set_opacity(opacity)
