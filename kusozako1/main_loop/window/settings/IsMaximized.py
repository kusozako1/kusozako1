# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.alfa.SettingsEntity import AlfaSettingsEntity


class DeltaIsMaximized(AlfaSettingsEntity):

    GROUP = "window"
    KEY = "is_maximized"
    DEFAULT = False

    def _reset(self, is_maximized):
        application_window = self._enquiry("delta > application window")
        application_window.props.maximized = is_maximized

    def _bind(self):
        pass
