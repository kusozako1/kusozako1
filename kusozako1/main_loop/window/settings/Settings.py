# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Size import DeltaSize
from .IsMaximized import DeltaIsMaximized
from .Fullscreen import DeltaFullscreen


class EchoSettings:

    def __init__(self, parent):
        DeltaSize(parent)
        DeltaIsMaximized(parent)
        DeltaFullscreen(parent)
