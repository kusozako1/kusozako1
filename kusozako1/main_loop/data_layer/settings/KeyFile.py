# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaKeyFile(DeltaEntity):

    @classmethod
    def new_for_directory(cls, parent, directory):
        key_file = cls(parent)
        key_file.construct(directory)
        return key_file

    def _convert_value(self, value):
        try:
            return eval(value)
        except (SyntaxError, NameError, TypeError):
            return value

    def _try_get_value(self, group, key):
        try:
            value = self._key_file.get_value(group, key)
            return self._convert_value(value)
        except GLib.Error:
            return None

    def get(self, group, key, default=None):
        value = self._try_get_value(group, key)
        if value is not None:
            return value
        else:
            self._key_file.set_value(group, key, str(default))
            return self._convert_value(default)

    def set(self, group, key, value):
        current_value = self._try_get_value(group, key)
        if current_value == value:
            return
        self._key_file.set_value(group, key, str(value))
        self._raise("delta > settings changed", (group, key, value))

    def toggle_boolean(self, group, key):
        value = not self._try_get_value(group, key)
        self.set(group, key, value)

    def save(self):
        self._key_file.save_to_file(self._path)

    def construct(self, directory):
        self._path = GLib.build_filenamev([directory, "application.conf"])
        if GLib.file_test(self._path, GLib.FileTest.EXISTS):
            self._key_file.load_from_file(self._path, GLib.KeyFileFlags.NONE)

    def __init__(self, parent):
        self._parent = parent
        self._key_file = GLib.KeyFile()
