# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.const import ColorSchemeKeys

NAMES = [GLib.path_get_dirname(__file__), "background.png"]
DEFAULT_PATH = GLib.build_filenamev(NAMES)


REPLACEMENTS = [
    {
        "target": "/*KUSOZAKO1_PRIMARY_FG_COLOR*/",
        "key": ColorSchemeKeys.PRIMARY_FG_COLOR,
        "template": "{}",
        "default": "White",
    },
    {
        "target": "/*KUSOZAKO1_PRIMARY_BG_COLOR*/",
        "key": ColorSchemeKeys.PRIMARY_BG_COLOR,
        "template": "{}",
        "default": "MediumSlateBlue",
    },
    {
        "target": "/*KUSOZAKO1_HIGHLIGHT_FG_COLOR*/",
        "key": ColorSchemeKeys.HIGHLIGHT_FG_COLOR,
        "template": "{}",
        "default": "White",
    },
    {
        "target": "/*KUSOZAKO1_HIGHLIGHT_BG_COLOR*/",
        "key": ColorSchemeKeys.HIGHLIGHT_BG_COLOR,
        "template": "{}",
        "default": "Orange",
    },
    {
        "target": "/*KUSOZAKO1_WINDOW_BACKGROUND_IMAGE*/",
        "key": "window_background_image",
        "template": "background-image: url('file://{}');",
        "default": DEFAULT_PATH,
    },
    {
        "target": "/*KUSOZAKO1_WINDOW_BACKGROUND_SIZE*/",
        "key": "window_background_size",
        "template": "background-size: {};",
        "default": "auto",
    },
]
