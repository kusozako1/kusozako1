# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PRIMARY_CONTAINER = "kusozako-primary-container"
PRIMARY_WIDGET = "kusozako-primary-widget"
