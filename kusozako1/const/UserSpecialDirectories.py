# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


def get_path(directory):
    return GLib.get_user_special_dir(directory)


HOME = GLib.get_home_dir()
CONFIG = GLib.get_user_config_dir()
CACHE = GLib.get_user_cache_dir()
DESKTOP = get_path(GLib.UserDirectory.DIRECTORY_DESKTOP)
DOCUMENTS = get_path(GLib.UserDirectory.DIRECTORY_DOCUMENTS)
DOWNLOAD = get_path(GLib.UserDirectory.DIRECTORY_DOWNLOAD)
MUSIC = get_path(GLib.UserDirectory.DIRECTORY_MUSIC)
PICTURES = get_path(GLib.UserDirectory.DIRECTORY_PICTURES)
PUBLIC_SHARE = get_path(GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE)
TEMPLATES = get_path(GLib.UserDirectory.DIRECTORY_TEMPLATES)
VIDEOS = get_path(GLib.UserDirectory.DIRECTORY_VIDEOS)
ALL = (
    HOME,
    DESKTOP,
    DOCUMENTS,
    DOWNLOAD,
    MUSIC,
    PICTURES,
    PUBLIC_SHARE,
    TEMPLATES,
    VIDEOS,
)
