# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

DIRECTORY = "directory"
PDF = "pdf"
IMAGE = "image"
TEXT = "text"
VIDEO = "video"
