# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

GREETER = "greeter"
BLANK = "blank"
PRIMARY_MENU = "primary-menu"
COLOR_SCHEME = "color-scheme"
ABOUT_APPLICATION = "about-application"
DIALOG = "dialog"
SHORTCUTS = "shortcuts"
