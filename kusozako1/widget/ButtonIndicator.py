# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity

DEFAULT_RGBA = 1, 0.65, 0, 0.8


class DeltaButtonIndicator(Gtk.DrawingArea, DeltaEntity):

    @classmethod
    def new_for_rgba(cls, parent, rgba=None):
        instance = cls(parent)
        if rgba is None:
            rgba = DEFAULT_RGBA
        instance.construct(rgba)
        return instance

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if not self._show:
            return
        cairo_context.set_source_rgba(*self._rgba)
        cairo_context.rectangle(0, 0, width, height)
        cairo_context.fill()

    def set_show(self, show):
        self._show = show
        self.queue_draw()

    def construct(self, rgba):
        self._rgba = rgba
        self.set_draw_func(self._draw_func)

    def __init__(self, parent):
        self._parent = parent
        self._show = False
        Gtk.DrawingArea.__init__(self)
        self.set_size_request(-1, 4)
        self._raise("delta > add to container", self)
