# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class AlfaIndicatorButton(Gtk.Button, DeltaEntity):

    __tooltip_text__ = None

    def _on_clicked(self, button):
        pass

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)

    def _on_initialize(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            can_focus=False,
            vexpand=False,
            )
        if self.__tooltip_text__ is not None:
            self.props.tooltip_text = self.__tooltip_text__
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.set_child(self._box)
        self._on_initialize()
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
