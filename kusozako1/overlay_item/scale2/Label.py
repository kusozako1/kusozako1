# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import CssName


class DeltaLabel(Gtk.Label, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        item = cls(parent)
        item.construct(model)
        item.add_css_class(CssName.PRIMARY_WIDGET)
        return item

    def construct(self, model):
        self._template = model["label"]
        self._raise("delta > add to container", self)

    def receive_transmission(self, value):
        label = self._template.format(value)
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            use_markup=True,
            hexpand=True,
            xalign=0,
            )
