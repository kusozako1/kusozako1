# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import CssName
from .Data import DeltaData
from .Label import DeltaLabel
from .drawing_area.DrawingArea import DeltaDrawingArea


class DeltaScale(Gtk.Button, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        item = cls(parent)
        item.construct(model)
        item.add_css_class(CssName.PRIMARY_WIDGET)
        return item

    def _delta_call_data_changed(self, value):
        self._data.set_value(value)

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)
        self._data.register(widget)

    def construct(self, model):
        self._data = DeltaData.new_for_model(self, model)
        self._box.append(Gtk.Image.new_from_icon_name(""))
        DeltaLabel.new_for_model(self, model)
        DeltaDrawingArea.new_for_model(self, model)

    def __init__(self, parent):
        self._parent = parent
        self._box = Gtk.Box(
            hexpand=True,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=False,
            )
        Gtk.Button.__init__(self, has_frame=False)
        self.set_child(self._box)
