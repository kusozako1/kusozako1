# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity

MARGIN = 8


class DeltaGestureSingle(Gtk.GestureSingle, DeltaEntity):

    def _on_begin(self, gesture_single, user_data):
        _, x, _ = gesture_single.get_point()
        value = max(0, min(100, x-MARGIN))
        self._raise("delta > data changed", value/100)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureSingle.__init__(self, button=1)
        self.connect("begin", self._on_begin)
        self._raise("delta > add controller", self)
