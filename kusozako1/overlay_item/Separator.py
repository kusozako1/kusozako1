# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaSeparator(Gtk.Separator, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Separator.__init__(self, margin_top=8, margin_bottom=8)
        self._raise("delta > add to container", self)
