# (c) copyright 2022-20242, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1 import SymbolicIcon


class FoxtrotStartImage:

    @classmethod
    def new(cls, box, image_name):
        instance = cls()
        image = instance.construct(image_name)
        box.append(image)
        return image

    def construct(self, image_name):
        if image_name:
            return SymbolicIcon.get_image_for_name(image_name)
        else:
            return Gtk.Image.new_from_icon_name("")

    def __init__(self):
        pass
