# (c) copyright 2022-20242, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .StartImage import FoxtrotStartImage
from .Label import FoxtrotLabel
from .EndWidget import FoxtrotEndWidget


class AlfaButton(Gtk.Button, DeltaEntity):

    START_ICON = ""
    LABEL = ""
    SHORTCUT = ""
    END_ICON = ""

    def _on_clicked(self, *args):
        pass

    def _on_initialize(self):
        pass

    def _set_to_container(self):
        self._raise("delta > add to container", self)

    def __init__(self, parent):
        self._parent = parent
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self._start_image = FoxtrotStartImage.new(box, self.START_ICON)
        self._label = FoxtrotLabel.new(box, self.LABEL)
        FoxtrotEndWidget.new(box, self.SHORTCUT, self.END_ICON)
        Gtk.Button.__init__(self, has_frame=False)
        self.set_child(box)
        self.add_css_class("kusozako-primary-widget")
        self.add_css_class("flat")
        self._on_initialize()
        self.connect("clicked", self._on_clicked)
        self._set_to_container()
