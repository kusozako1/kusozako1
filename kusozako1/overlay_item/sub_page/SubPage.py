# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPrimaryMenuPage
from .fixed_widgets.FixedWidgets import EchoFixedWidgets
from .SubPageContainer import DeltaSubpageContainer


class AlfaSubPage(Gtk.Box, DeltaEntity):

    PAGE_NAME = "define-page-name-here."
    BACK_TO = OverlayPrimaryMenuPage.PORTAL     # default

    def _delta_info_back_to(self):
        return self.BACK_TO

    def _delta_call_add_to_base_container(self, widget):
        self.append(widget)

    def _delta_call_add_to_container(self, widget):
        self._container.append(widget)

    def _on_initialize(self):
        raise NotImplementedError

    def _add_to_container(self):
        param = self, self.PAGE_NAME
        user_data = MainWindowSignals.ADD_EXTRA_PRIMARY_MENU, param
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_size_request(240, 0)
        self.add_css_class("osd")
        EchoFixedWidgets(self)
        self._container = DeltaSubpageContainer(self)
        self._on_initialize()
        self._add_to_container()
