# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity

MARGIN = 8


class DeltaGestureDrag(Gtk.GestureDrag, DeltaEntity):

    def _on_drag_begin(self, gesture_drag, x, y):
        self._start_x = x

    def _on_drag_update(self, gesture_drag, offset_x, offset_y):
        if self._start_x is None:
            return
        value = max(0, min(100, self._start_x+offset_x-MARGIN))
        self._raise("delta > data changed", value/100)

    def __init__(self, parent):
        self._parent = parent
        self._start_x = None
        Gtk.GestureDrag.__init__(self)
        self.connect("drag-begin", self._on_drag_begin)
        self.connect("drag-update", self._on_drag_update)
        self._raise("delta > add controller", self)
