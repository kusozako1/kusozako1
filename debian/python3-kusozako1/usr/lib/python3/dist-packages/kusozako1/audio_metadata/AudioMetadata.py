# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from .Directory import FoxtrotDirectory
from .Loader import FoxtrotLoader
from .builder.Builder import FoxtrotBuilder


class FoxtrotAudioMetadata:

    @classmethod
    def get_default(cls):
        if "_default_instance" not in dir(cls):
            cls._default_instance = cls()
        return cls._default_instance

    def _load_async(self, file_info, callback, user_data=None):
        md5_checksum, metadata_path = self._directory.get_path(file_info)
        metadata = self._loader.load_metadata(file_info, metadata_path)
        if metadata is None:
            metadata = self._builder.build_(file_info, metadata_path)
        callback(*metadata, user_data)
        return GLib.SOURCE_REMOVE

    def load(self, file_info, callback):
        md5_checksum, metadata_path = self._directory.get_path(file_info)
        metadata = self._loader.load_metadata(file_info, metadata_path)
        if metadata is None:
            metadata = self._builder.build_(file_info, metadata_path)
        callback(file_info, *metadata)

    def load_async(self, file_info, callback, user_data=None):
        GLib.idle_add(self._load_async, file_info, callback, user_data)

    def __init__(self):
        self._directory = FoxtrotDirectory()
        self._loader = FoxtrotLoader()
        self._builder = FoxtrotBuilder()
