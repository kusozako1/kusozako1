# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .model.Model import DeltaModel
from .user_interface.UserInterface import DeltaUserInterface


class DeltaColorScheme(DeltaEntity):

    def _delta_info_model(self):
        return self._model

    def _delta_call_register_color_scheme_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_color_scheme_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        self._model = DeltaModel(self)
        DeltaUserInterface(self)
