# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from .Spacer import DeltaSpacer
from .pages.Pages import EchoPages
from .observers.Observers import EchoObservers


class DeltaPrimaryMenu(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self._stack.add_named(widget, name)

    def _delta_call_move_page_to(self, page_name):
        self._stack.set_visible_child_name(page_name)

    def _delta_call_register_default_widget(self, widget):
        self._default_widget = widget

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaSpacer(self)
        self._stack = Gtk.Stack(
            hhomogeneous=False,
            hexpand=False,
            transition_type=Gtk.StackTransitionType.SLIDE_LEFT_RIGHT,
            transition_duration=400,
            )
        self.set_size_request(240, -1)
        self.append(self._stack)
        EchoPages(self)
        EchoObservers(self)
        self._raise("delta > add to stack", (self, OverlayPage.PRIMARY_MENU))
