# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import ColorSchemeKeys

DATA = (
    {
        "title": _("Primary Foreground Color"),
        "key": ColorSchemeKeys.PRIMARY_FG_COLOR,
    },
    {
        "title": _("Primary Background Color"),
        "key": ColorSchemeKeys.PRIMARY_BG_COLOR,
    },
    {
        "title": _("Highlight Foreground Color"),
        "key": ColorSchemeKeys.HIGHLIGHT_FG_COLOR,
    },
    {
        "title": _("Highlight Background Color"),
        "key": ColorSchemeKeys.HIGHLIGHT_BG_COLOR,
    },
)
