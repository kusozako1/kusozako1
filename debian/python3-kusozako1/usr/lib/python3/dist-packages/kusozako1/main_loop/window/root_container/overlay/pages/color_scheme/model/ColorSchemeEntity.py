# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import ColorSchemeSignals


class DeltaColorSchemeEntity(GObject.Object, DeltaEntity):

    __gsignals__ = {
        "color-changed": (GObject.SIGNAL_RUN_FIRST, None, (Gdk.RGBA,))
    }

    @classmethod
    def new(cls, parent, scheme_data):
        instance = cls(parent)
        instance.construct(scheme_data)
        return instance

    def set_rgba(self, rgba):
        self._current_rgba = rgba
        self.emit("color-changed", rgba)
        user_data = ColorSchemeSignals.COLOR_SELECTED, None
        self._raise("delta > color scheme signal", user_data)

    def construct(self, scheme_data):
        self._title = scheme_data["title"]
        self._key = scheme_data["key"]
        query = "css", self._key
        original_rgba = self._enquiry("delta > settings", query)
        rgba = Gdk.RGBA()
        rgba.parse(original_rgba)
        self._original_rgba = rgba.copy()
        self._current_rgba = rgba.copy()

    def __init__(self, parent):
        self._parent = parent
        GObject.Object.__init__(self)

    @property
    def key(self):
        return self._key

    @property
    def title(self):
        return self._title

    @property
    def rgba(self):
        return self._current_rgba

    @property
    def original_rgba(self):
        return self._original_rgba

    @property
    def changed(self):
        return not self._original_rgba.equal(self._current_rgba)
