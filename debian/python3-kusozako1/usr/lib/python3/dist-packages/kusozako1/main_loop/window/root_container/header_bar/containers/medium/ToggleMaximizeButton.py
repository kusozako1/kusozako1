# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Button import AlfaButton


class DeltaToggleMaximizeButton(AlfaButton):

    ICON_NAME = "window-maximize-symbolic"
    SIGNAL_DATA = MainWindowSignals.TOGGLE_MAXIMIZED, None
