# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.SettingsButton import AlfaSettingsButton


class AlfaBackgroundStyleButton(AlfaSettingsButton):

    GROUP = "css"
    KEY = "window_background_size"
