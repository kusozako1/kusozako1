# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from .Button import DeltaButton


class DeltaButtonBox(Gtk.CenterBox, DeltaEntity):

    def _clear_children(self):
        for button in self._buttons:
            self._inner_box.remove(button)
        self._buttons.clear()

    def _set_model(self, model):
        button_models = model["buttons"]
        length = len(button_models)
        for index in range(0, length):
            button_model = button_models[index]
            button = DeltaButton.new(self, button_model, index, length)
            self._buttons.append(button)
            self._inner_box.append(button)

    def receive_transmission(self, user_data):
        signal, model = user_data
        if signal != MainWindowSignals.SHOW_DIALOG:
            return
        self._clear_children()
        self._set_model(model)

    def __init__(self, parent):
        self._parent = parent
        self._buttons = []
        Gtk.CenterBox.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self._inner_box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            spacing=16,
            )
        self.set_center_widget(self._inner_box)
        self._raise("delta > add to container", self)
        self._raise("delta > register main window signal object", self)
