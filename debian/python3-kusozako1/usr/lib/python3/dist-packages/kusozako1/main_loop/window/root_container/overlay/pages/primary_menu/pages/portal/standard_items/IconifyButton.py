# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaIconifyButton(AlfaButton):

    LABEL = _("Iconify")

    def _on_clicked(self, button):
        user_data = MainWindowSignals.ICONIFY, None
        self._raise("delta > main window signal", user_data)
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)
