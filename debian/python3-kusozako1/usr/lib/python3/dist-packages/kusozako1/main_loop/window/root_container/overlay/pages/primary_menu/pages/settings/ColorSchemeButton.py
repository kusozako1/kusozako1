# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals


class DeltaColorSchemeButton(AlfaButton):

    END_ICON = "go-next-symbolic"
    LABEL = _("Color Scheme")

    def _on_clicked(self, button):
        self._raise(
            "delta > main window signal",
            (MainWindowSignals.SHOW_OVERLAY, OverlayPage.COLOR_SCHEME)
            )
