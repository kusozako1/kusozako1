# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.alfa.SettingsEntity import AlfaSettingsEntity


class DeltaFullscreen(AlfaSettingsEntity):

    GROUP = "window"
    KEY = "is_fullscreen"
    DEFAULT = False

    def _reset(self, is_fullscreen):
        application_window = self._enquiry("delta > application window")
        application_window.props.fullscreened = is_fullscreen

    def _bind(self):
        pass
