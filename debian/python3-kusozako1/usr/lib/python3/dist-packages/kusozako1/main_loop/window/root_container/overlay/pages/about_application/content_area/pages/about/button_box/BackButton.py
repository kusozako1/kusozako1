# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage


class DeltaBackButton(AlfaButton):

    START_ICON = "go-previous-symbolic"
    LABEL = _("Back")

    def _on_clicked(self, button):
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU
        self._raise("delta > main window signal", user_data)
