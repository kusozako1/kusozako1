# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaAdditionalCss(DeltaEntity):

    @classmethod
    def new(cls, parent, resource_directory):
        instance = cls(parent)
        instance.construct(resource_directory)
        return instance

    def _get_contents(self, resource_directory, file_name):
        names = [resource_directory, file_name]
        path = GLib.build_filenamev(names)
        _, bytes_ = GLib.file_get_contents(path)
        return bytes_

    def get_css(self):
        return self._css

    def construct(self, resource_directory):
        css_contents = self._get_contents(resource_directory, "additional.css")
        self._css = css_contents.decode("utf-8")

    def __init__(self, parent):
        self._parent = parent
