# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .BackgroundStyleButton import AlfaBackgroundStyleButton


class DeltaAutoButton(AlfaBackgroundStyleButton):

    LABEL = _("Auto")
    MATCH_VALUE = "auto"
