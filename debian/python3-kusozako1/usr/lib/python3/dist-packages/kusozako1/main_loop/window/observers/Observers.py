# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Iconify import DeltaIconify
from .ToggleMaximized import DeltaToggleMaximized
from .ToggleFullscreen import DeltaToggleFullscreen
from .TryClose import DeltaTryClose
from .ChangeTitle import DeltaChangeTitle
from .OpenFile import DeltaOpenFile


class EchoObservers:

    def __init__(self, parent):
        DeltaIconify(parent)
        DeltaToggleMaximized(parent)
        DeltaToggleFullscreen(parent)
        DeltaTryClose(parent)
        DeltaChangeTitle(parent)
        DeltaOpenFile(parent)
