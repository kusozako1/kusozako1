# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals


class DeltaGreeter(Gtk.Label, DeltaEntity):

    def _timeout(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)
        user_data_2 = MainWindowSignals.GREETER_CLOSED, None
        self._raise("delta > main window signal", user_data_2)
        return GLib.SOURCE_REMOVE

    def __init__(self, parent):
        self._parent = parent
        text = "Welcome Back, {}.".format(GLib.get_user_name())
        Gtk.Label.__init__(self, label=text, wrap=True)
        self.add_css_class("kusozako-large-title")
        self.add_css_class("osd")
        self._raise("delta > add to stack", (self, OverlayPage.GREETER))
        GLib.timeout_add(2000, self._timeout)
