# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaHide(DeltaEntity):

    def _on_hide(self, stack):
        stack.set_can_focus(False)
        user_data = MainWindowSignals.OVERLAY_STACK_CLOSED, None
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        stack = self._enquiry("delta > stack")
        stack.connect("hide", self._on_hide)
