# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .Builder import DeltaBuilder
from .Files import DeltaFiles


class DeltaCommandLine(DeltaEntity):

    def _on_command_line(self, application, command_line):
        # self._options is GVariantDict
        self._options = command_line.get_options_dict()
        if self._options.lookup_value("version"):
            version = self._enquiry("delta > data", "version")
            print("version :", version)
        else:
            self._files = DeltaFiles.new_for_options(self, self._options)
            self._raise("delta > command line ready", self)
        return 0

    def _delta_call_options_built(self, application):
        application.connect("command-line", self._on_command_line)

    def _delta_info_command_line_option(self, key=GLib.OPTION_REMAINING):
        value = self._options.lookup_value(key)
        return None if value is None else value.unpack()

    def _delta_info_command_line_files(self):
        return self._files.get_files()

    def __init__(self, parent):
        self._parent = parent
        DeltaBuilder(self)
