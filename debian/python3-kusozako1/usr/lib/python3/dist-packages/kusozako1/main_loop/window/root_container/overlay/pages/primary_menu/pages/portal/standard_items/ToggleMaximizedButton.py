# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaToggleMaximizedButton(AlfaButton):

    SHORTCUT = "Shift+F11"

    def _on_clicked(self, button):
        user_data = MainWindowSignals.TOGGLE_MAXIMIZED, None
        self._raise("delta > main window signal", user_data)

    def _refresh(self, is_maximized):
        self._label.props.label = "Unmaximize" if is_maximized else "Maximize"

    def _on_initialize(self):
        query = "window", "is_maximized", False
        is_maximized = self._enquiry("delta > settings", query)
        self._refresh(is_maximized)
        self._raise("delta > register settings object", self)

    def receive_transmission(self, user_data):
        group, key, is_maximized = user_data
        if group == "window" and key == "is_maximized":
            self._refresh(is_maximized)
