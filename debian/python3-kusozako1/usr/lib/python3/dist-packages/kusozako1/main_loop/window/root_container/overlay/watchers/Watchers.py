# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Hide import DeltaHide
from .NotifyVisibleChildName import DeltaNotifyVisibleChildName


class EchoWatchers:

    def __init__(self, parent):
        DeltaHide(parent)
        DeltaNotifyVisibleChildName(parent)
