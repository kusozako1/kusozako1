# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .CancelButton import DeltaCancelButton
from .ApplyButton import DeltaApplyButton


class DeltaButtonBox(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        clamp = Adw.Clamp()
        Gtk.Box.__init__(self, homogeneous=True, spacing=8)
        DeltaCancelButton(self)
        DeltaApplyButton(self)
        clamp.set_child(self)
        self._raise("delta > add overlay", clamp)
