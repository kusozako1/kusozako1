# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaColorIndicatorButton(Gtk.ColorButton, DeltaEntity):

    @classmethod
    def new(cls, parent, color_scheme_entity):
        instance = cls(parent)
        instance.construct(color_scheme_entity)

    def _on_color_changed(self, color_scheme_entity, new_rgba):
        self.set_rgba(new_rgba)

    def _on_color_set(self, color_button, color_scheme_entity):
        color_scheme_entity.set_rgba(self.props.rgba)

    def construct(self, color_scheme_entity):
        self.set_rgba(color_scheme_entity.rgba)
        color_scheme_entity.connect("color-changed", self._on_color_changed)
        self.connect("color-set", self._on_color_set, color_scheme_entity)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ColorButton.__init__(self)
        self._raise("delta > add to container", self)
