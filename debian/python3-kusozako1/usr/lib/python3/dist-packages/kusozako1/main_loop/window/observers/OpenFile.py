# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.const import MainWindowSignals
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog
from .Observer import AlfaObserver


class DeltaOpenFile(AlfaObserver):

    SIGNAL = MainWindowSignals.OPEN_FILE

    def _delta_call_dialog_response(self, gfile):
        user_data = MainWindowSignals.FILE_OPENED, gfile
        self._raise("delta > main window signal", user_data)

    def _override_integrated_sequence(self):
        user_data = MainWindowSignals.TRY_OPEN_FILE, None
        self._raise("delta > main window signal", user_data)

    def _get_gfile(self):
        directory = self._enquiry("delta > data", "default-file-directory")
        if directory is None:
            directory = GLib.get_home_dir()
        return Gio.File.new_for_path(directory)

    def _do_integrated_sequence(self):
        DeltaFileDialog.select_file(
            self,
            title=_("Open"),
            mime_types=self._enquiry("delta > data", "mime"),
            default_directory=self._get_gfile(),
            )

    def _on_signal_received(self, param=None):
        if self._enquiry("delta > data", "use-global-open"):
            self._override_integrated_sequence()
        else:
            self._do_integrated_sequence()
