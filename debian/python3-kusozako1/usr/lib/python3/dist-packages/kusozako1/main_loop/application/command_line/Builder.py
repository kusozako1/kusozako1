# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity

FILES = (
    GLib.OPTION_REMAINING,
    0,
    GLib.OptionFlags.NONE,
    GLib.OptionArg.STRING_ARRAY,
    "",
    "[URI…]"
)

VERSION = (
    "version",
    ord("V"),
    GLib.OptionFlags.NONE,
    GLib.OptionArg.NONE,
    _("show version"),
    None
)


class DeltaBuilder(DeltaEntity):

    def _set_main_options(self, application, main_options):
        for main_option in main_options:
            application.add_main_option(*main_option)

    def __init__(self, parent):
        self._parent = parent
        application = self._enquiry("delta > application")
        application.add_main_option(*VERSION)
        main_options = self._enquiry("delta > main options")
        if main_options is not None:
            self._set_main_options(application, main_options)
        if self._enquiry("delta > data", "max-files") > 0:
            application.add_main_option(*FILES)
        long_description = self._enquiry("delta > data", "long-description")
        application.set_option_context_summary(long_description)
        self._raise("delta > options built", application)
