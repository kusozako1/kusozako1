# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class AlfaLastButton:

    def _on_pressed(self, controller_key, keyval, keycode, state):
        accel = Gtk.accelerator_get_label(keyval, state)
        return accel not in ("Left", "Return", "Space")

    def __init__(self):
        controller_key = Gtk.EventControllerKey.new()
        controller_key.connect("key-pressed", self._on_pressed)
        self.add_controller(controller_key)
