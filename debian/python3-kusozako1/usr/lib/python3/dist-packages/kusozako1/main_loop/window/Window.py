# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .watchers.close_request.CloseRequest import DeltaCloseRequest
from .observers.Observers import EchoObservers
from .settings.Settings import EchoSettings
from .Accels import DeltaAccels
from .root_container.RootContainer import DeltaRootContainer


class DeltaWindow(Adw.ApplicationWindow, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_content(widget)

    def _delta_call_content_area_ready(self, end_point):
        self._end_point = end_point
        self.present()

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def _delta_info_application_window(self):
        return self

    def _delta_call_register_main_window_signal_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_unregister_main_window_signal_object(self, object_):
        self._transmitter.unregister_listener(object_)

    def _delta_call_main_window_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        Adw.ApplicationWindow.__init__(
            self,
            icon_name=self._enquiry("delta > data", "rdnn-name"),
            default_height=400,
            default_width=640,
            handle_menubar_accel=False,
            )
        DeltaCloseRequest(self)
        EchoObservers(self)
        EchoSettings(self)
        DeltaAccels(self)
        DeltaRootContainer(self)
        self._raise("delta > add to container", self)
        self._raise(
            "delta > loopback application window ready",
            self._end_point
            )
