# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.alfa.SettingsEntity import AlfaSettingsEntity


class DeltaSize(AlfaSettingsEntity):

    GROUP = "window"
    KEY = "size"
    DEFAULT = [400, 400]

    def _reset(self, value):
        application_window = self._enquiry("delta > application window")
        application_window.set_default_size(*value)

    def _on_notify(self, application_window, param):
        if not param.name.startswith("default-"):
            return
        width, height = application_window.get_default_size()
        user_data = self.GROUP, self.KEY, (width, height)
        self._raise("delta > settings", user_data)

    def _bind(self):
        application_window = self._enquiry("delta > application window")
        application_window.connect("notify", self._on_notify)
