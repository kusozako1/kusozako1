# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals
from .BackButton import DeltaBackButton
from .Spacer import DeltaSpacer
from .content_area.ContentArea import DeltaContentArea


class DeltaShortCuts(Gtk.CenterBox, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        if isinstance(widget, DeltaBackButton):
            self.set_start_widget(widget)
        if isinstance(widget, DeltaSpacer):
            self.set_end_widget(widget)

    def _delta_call_add_to_container_center(self, widget):
        self.set_center_widget(widget)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.OVERLAY_CLOSED:
            return
        user_data = self, OverlayPage.SHORTCUTS
        self._raise("delta > add to stack", user_data)
        self._raise("delta > unregister main window signal object", self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaBackButton(self)
        DeltaContentArea(self)
        DeltaSpacer(self)
        self._raise("delta > register main window signal object", self)
