# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .BackgroundStyleButton import AlfaBackgroundStyleButton


class DeltaCoverButton(AlfaBackgroundStyleButton):

    LABEL = _("Cover")
    MATCH_VALUE = "cover"
