# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from kusozako1.overlay_item.button.Button import AlfaButton


class AlfaFileButton(AlfaButton):

    START_ICON = "define start icon here"
    LABEL = "define label here"
    SHORTCUT = "define shortcut here"
    SIGNAL = "define signal to raise here"

    def _close_overlay(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _on_clicked(self, button):
        self._close_overlay()
        user_data = self.SIGNAL, None
        self._raise("delta > main window signal", user_data)

    def _add_accel(self):
        param = self.SHORTCUT, self.SIGNAL, None
        user_data = MainWindowSignals.ADD_GLOBAL_ACCEL, param
        self._raise("delta > main window signal", user_data)

    def _get_accelerator(self):
        accelerator = self.SHORTCUT.replace("+", "")
        accelerator = accelerator.replace("Alt", "<Alt>")
        accelerator = accelerator.replace("Ctrl", "<Ctrl>")
        accelerator = accelerator.replace("Shift", "<Shift>")
        return accelerator

    def _add_shortcut(self):
        param = "File", self._get_accelerator(), self.LABEL
        user_data = MainWindowSignals.ADD_SHORTCUT, param
        self._raise("delta > main window signal", user_data)

    def _on_initialize(self):
        self._add_accel()
        self._add_shortcut()
