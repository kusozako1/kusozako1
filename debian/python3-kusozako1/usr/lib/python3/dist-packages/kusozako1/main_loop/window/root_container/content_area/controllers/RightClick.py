# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.alfa.ControllerContextMenu import AlfaControllerContextMenu
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage

POINTER_NAMES = [
    ["border", "border", "border"],
    ["border", "content", "border"],
    ["border", "border", "border"]
    ]
BORDER_WIDTH = 16


class DeltaRightClick(AlfaControllerContextMenu):

    def _get_index(self, size, position):
        if BORDER_WIDTH > position:
            return 0
        elif size-BORDER_WIDTH > position > BORDER_WIDTH:
            return 1
        return 2

    def _on_pressed(self, controller, x, y):
        widget = controller.get_widget()
        x_index = self._get_index(widget.get_allocated_width(), x)
        y_index = self._get_index(widget.get_allocated_height(), y)
        if "border" != POINTER_NAMES[y_index][x_index]:
            return
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU
        self._raise("delta > main window signal", user_data)
