# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import OverlayPrimaryMenuPage
from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from .BoldButton import DeltaBoldButton
from .MediumButton import DeltaMediumButton
from .ThinButton import DeltaThinButton
from .NoneButton import DeltaNoneButton


class DeltaHeaderBarTypes(AlfaSubPage):

    PAGE_NAME = OverlayPrimaryMenuPage.HEADER_BAR_TYPES
    BACK_TO = OverlayPrimaryMenuPage.SETTINGS

    def _on_initialize(self):
        DeltaBoldButton(self)
        DeltaMediumButton(self)
        DeltaThinButton(self)
        DeltaNoneButton(self)

    def _add_to_container(self):
        user_data = self, self.PAGE_NAME
        self._raise("delta > add to stack", user_data)
