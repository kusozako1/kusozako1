# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage


class DeltaNotifyVisibleChildName(DeltaEntity):

    def _on_notify(self, stack, param_spec):
        can_target = (stack.get_visible_child_name() != OverlayPage.BLANK)
        stack.set_can_target(can_target)

    def __init__(self, parent):
        self._parent = parent
        stack = self._enquiry("delta > stack")
        stack.connect("notify::visible-child-name", self._on_notify)
