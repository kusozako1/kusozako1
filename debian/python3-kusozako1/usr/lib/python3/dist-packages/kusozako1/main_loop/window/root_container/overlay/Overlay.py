# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from .observers.Observers import EchoObservers
from .watchers.Watchers import EchoWatchers
from .pages.Pages import EchoPages
from .EventControllerKey import DeltaEventControllerKey


class DeltaOverlay(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def _delta_call_switch_stack_to(self, page_name):
        self.set_visible_child_name(page_name)

    def _delta_info_stack(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.CROSSFADE,
            transition_duration=1500,
            )
        EchoWatchers(self)
        EchoPages(self)
        EchoObservers(self)
        DeltaEventControllerKey(self)
        self._raise("delta > add to overlay", self)
        self.set_visible_child_name(OverlayPage.GREETER)
