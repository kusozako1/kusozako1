# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from .Spacer import DeltaSpacer
from .content_area.ContentArea import DeltaContentArea


class DeltaAboutApplication(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaSpacer(self)
        DeltaContentArea(self)
        DeltaSpacer(self)
        user_data = self, OverlayPage.ABOUT_APPLICATION
        self._raise("delta > add to stack", user_data)
