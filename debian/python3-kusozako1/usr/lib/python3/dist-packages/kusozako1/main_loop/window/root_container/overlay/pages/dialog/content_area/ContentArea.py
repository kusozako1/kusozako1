# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Heading import DeltaHeading
from .Description import DeltaDescription
from .ButtonBox import DeltaButtonBox


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            )
        self.add_css_class("osd")
        DeltaHeading(self)
        DeltaDescription(self)
        DeltaButtonBox(self)
        self._raise("delta > add to container", self)
