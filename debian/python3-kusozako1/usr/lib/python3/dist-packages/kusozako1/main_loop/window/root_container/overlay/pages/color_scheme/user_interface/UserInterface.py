# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from .BackButton import DeltaBackButton
from .content_area.ContentArea import DeltaContentArea
from .Spacer import DeltaSpacer


class DeltaUserInterface(Gtk.CenterBox, DeltaEntity):

    def _delta_call_add_to_container_start(self, widget):
        self.set_start_widget(widget)

    def _delta_call_add_to_container_center(self, widget):
        self.set_center_widget(widget)

    def _delta_call_add_to_container_end(self, widget):
        self.set_end_widget(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaBackButton(self)
        DeltaContentArea(self)
        DeltaSpacer(self)
        user_data = self, OverlayPage.COLOR_SCHEME
        self._raise("delta > add to stack", user_data)
