# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaShowExtraOverlay(AlfaObserver):

    SIGNAL = MainWindowSignals.SHOW_EXTRA_OVERLAY

    def _on_signal_received(self, user_data):
        page_name, _ = user_data
        stack = self._enquiry("delta > stack")
        stack.show()
        self._raise("delta > switch stack to", page_name)
