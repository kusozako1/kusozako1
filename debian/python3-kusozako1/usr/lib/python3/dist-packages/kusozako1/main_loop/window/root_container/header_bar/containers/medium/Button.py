# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.const import CssName
from kusozako1.Entity import DeltaEntity


class AlfaButton(Gtk.Button, DeltaEntity):

    ICON_NAME = "define icon name here"
    REGISTRATION_MESSAGE = "delta > add to container end"
    SIGNAL_DATA = "signal", "nullable-user-data"

    def do_clicked(self):
        user_data = self.SIGNAL_DATA
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            icon_name=self.ICON_NAME,
            css_classes=[CssName.PRIMARY_WIDGET],
            has_frame=False,
            margin_top=1,
            margin_bottom=1,
            margin_start=1,
            margin_end=1,
            )
        self._raise(self.REGISTRATION_MESSAGE, self)
