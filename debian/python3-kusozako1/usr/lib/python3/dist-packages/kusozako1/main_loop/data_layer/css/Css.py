# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Replacements import DeltaReplacements
from .local_replacements.LocalReplacements import DeltaLocalReplacements


class DeltaCss(DeltaEntity):

    def _reload(self):
        css = self._replacements.replace_all()
        local_css = self._local_replacements.replace_all()
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(bytes(css+local_css, "utf-8"))
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )

    def receive_transmission(self, user_data):
        group, _, _ = user_data
        if group == "css":
            self._reload()

    def __init__(self, parent):
        self._parent = parent
        self._replacements = DeltaReplacements(self)
        self._local_replacements = DeltaLocalReplacements(self)
        self._reload()
        self._raise("delta > register settings object", self)
        self._raise("delta > loopback data layer ready", self)
