# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .TitleLabel import DeltaTitleLabel
from .list_box.ListBox import DeltaListBox
from .BottomSpacer import DeltaBottomSpacer


class EchoScrollableContents:

    def __init__(self, parent):
        DeltaTitleLabel(parent)
        DeltaListBox(parent)
        DeltaBottomSpacer(parent)
