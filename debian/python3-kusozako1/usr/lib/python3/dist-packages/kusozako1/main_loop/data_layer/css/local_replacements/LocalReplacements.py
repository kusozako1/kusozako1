# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .FileValidation import DeltaFileValidation
from .AdditionalCss import DeltaAdditionalCss
from .Replacements import DeltaReplacements


class DeltaLocalReplacements(DeltaEntity):

    def replace_all(self):
        if not self._has_data:
            return ""
        css = self._additional_css.get_css()
        return self._replacements.replace_all(css)

    def _delta_call_invalid(self):
        self._has_data = False

    def _delta_call_valid(self, resource_directory):
        self._has_data = True
        self._additional_css = DeltaAdditionalCss.new(self, resource_directory)
        self._replacements = DeltaReplacements.new(self, resource_directory)

    def __init__(self, parent):
        self._parent = parent
        DeltaFileValidation(self)
