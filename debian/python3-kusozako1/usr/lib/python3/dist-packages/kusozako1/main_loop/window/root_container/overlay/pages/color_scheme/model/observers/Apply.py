# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import ColorSchemeSignals


class DeltaApply(DeltaEntity):

    def _get_changed(self):
        for color_scheme_entity in self._enquiry("delta > model"):
            if not color_scheme_entity.changed:
                continue
            yield color_scheme_entity.key, color_scheme_entity.rgba.to_string()

    def _on_signal_received(self, param=None):
        for key, value in self._get_changed():
            user_data = "css", key, value
            self._raise("delta > settings", user_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != ColorSchemeSignals.APPLY:
            return
        self._on_signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register color scheme object", self)
