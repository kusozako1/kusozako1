# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Button import AlfaButton


class DeltaIconifyButton(AlfaButton):

    ICON_NAME = "window-minimize-symbolic"
    SIGNAL_DATA = MainWindowSignals.ICONIFY, None
