# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals


class DeltaBackButton(Gtk.Box, DeltaEntity):

    def _on_map(self, button):
        button.grab_focus()

    def _on_clicked(self, button):
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            vexpand=True,
            tooltip_text="Back to Primary Menu",
            )
        button = Gtk.Button.new_from_icon_name("go-previous-symbolic")
        button.add_css_class("kusozako-primary-widget")
        button.connect("map", self._on_map)
        button.connect("clicked", self._on_clicked)
        self.append(button)
        self.add_css_class("osd")
        self._raise("delta > add to container", self)
