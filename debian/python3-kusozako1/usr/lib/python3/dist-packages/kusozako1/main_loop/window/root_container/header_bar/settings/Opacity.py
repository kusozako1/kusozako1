# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.alfa.SettingsEntity import AlfaSettingsEntity


class DeltaOpacity(AlfaSettingsEntity):

    GROUP = "header_bar"
    KEY = "opacity"
    DEFAULT = 0.9

    def _reset(self, opacity):
        stack = self._enquiry("delta > header bar")
        stack.set_opacity(opacity)
