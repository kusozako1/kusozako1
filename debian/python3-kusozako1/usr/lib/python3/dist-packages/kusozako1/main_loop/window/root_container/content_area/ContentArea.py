# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .controllers.Controllers import EchoControllers
from .Opacity import DeltaOpacity


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def _delta_call_add_to_container(self, widget):
        self._inner_box.append(widget)

    def _delta_info_inner_box(self):
        return self._inner_box

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self)
        self._inner_box = Gtk.Box(
            margin_top=16,
            margin_bottom=16,
            margin_start=16,
            margin_end=16,
            )
        self.append(self._inner_box)
        EchoControllers(self)
        DeltaOpacity(self)
        self._raise("delta > add to container", self)
        self._raise("delta > content area ready", self)
