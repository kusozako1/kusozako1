# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .data.css_replacements import REPLACEMENTS


class DeltaReplacements(DeltaEntity):

    def _get_string_for_file_name(self, file_name):
        names = [GLib.path_get_dirname(__file__), "data", file_name]
        path = GLib.build_filenamev(names)
        _, bytes_ = GLib.file_get_contents(path)
        return bytes_.decode("utf-8")

    def _replace(self, css, item):
        query = "css", item["key"], item["default"]
        settings = self._enquiry("delta > settings", query)
        if settings == "KUSOZAKO1_NULL_SETTINGS":
            return css
        replacement = item["template"].format(settings)
        return css.replace(item["target"], replacement)

    def replace_all(self):
        css = self._get_string_for_file_name("application.css")
        for item in REPLACEMENTS:
            css = self._replace(css, item)
        return css

    def __init__(self, parent):
        self._parent = parent
