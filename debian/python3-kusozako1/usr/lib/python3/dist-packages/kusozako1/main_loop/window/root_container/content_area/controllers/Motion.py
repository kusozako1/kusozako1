# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity

POINTER_NAMES = [
    ["nw-resize", "default", "ne-resize"],
    ["default", "default", "default"],
    ["sw-resize", "default", "se-resize"]
    ]

BORDER_WIDTH = 16


class DeltaMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _get_index(self, size, position):
        if BORDER_WIDTH > position:
            return 0
        elif size-BORDER_WIDTH > position > BORDER_WIDTH:
            return 1
        return 2

    def _on_motion(self, controller, x, y):
        widget = controller.get_widget()
        x_index = self._get_index(widget.get_allocated_width(), x)
        y_index = self._get_index(widget.get_allocated_height(), y)
        pointer_name = POINTER_NAMES[y_index][x_index]
        cursor = Gdk.Cursor.new_from_name(pointer_name)
        widget.set_cursor(cursor)

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerMotion.__init__(self)
        self.connect("motion", self._on_motion)
        self._raise("delta > add controller", self)
