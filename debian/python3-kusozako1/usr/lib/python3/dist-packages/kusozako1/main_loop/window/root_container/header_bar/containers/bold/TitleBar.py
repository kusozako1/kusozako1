# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaTitleBar(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, title = user_data
        if signal != MainWindowSignals.CHANGE_TITLE:
            return
        if title is None:
            title = self._enquiry("delta > data", "application-name")
        self.set_label(title)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            label=self._enquiry("delta > data", "application-name"),
            margin_start=8,
            margin_end=8,
            hexpand=True,
            ellipsize=Pango.EllipsizeMode.END,
            )
        self._raise("delta > add to container center", self)
        self._raise("delta > register main window signal object", self)
