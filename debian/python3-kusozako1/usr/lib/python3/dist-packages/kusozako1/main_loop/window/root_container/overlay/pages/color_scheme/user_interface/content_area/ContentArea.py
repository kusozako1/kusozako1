# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .scrollable_contents.ScrollableContents import EchoScrollableContents
from .button_box.ButtonBox import DeltaButtonBox


class DeltaContentArea(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)

    def _delta_call_add_overlay(self, widget):
        self.add_overlay(widget)

    def _on_get_child_position(self, overlay, widget, rectangle):
        widget_height = 36
        bottom_margin = 16
        overlay_height = overlay.get_allocated_height()
        rectangle.y = overlay_height - widget_height - bottom_margin
        rectangle.height = widget_height
        return True, rectangle

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self, hexpand=True, vexpand=True)
        self.add_css_class("osd")
        scrolled_window = Gtk.ScrolledWindow()
        clamp = Adw.Clamp(orientation=Gtk.Orientation.HORIZONTAL)
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        EchoScrollableContents(self)
        DeltaButtonBox(self)
        clamp.set_child(self._box)
        scrolled_window.set_child(clamp)
        self.set_child(scrolled_window)
        self.connect("get-child-position", self._on_get_child_position)
        self._raise("delta > add to container center", self)
