# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPrimaryMenuPage


class DeltaOverlayClosed(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.OVERLAY_CLOSED:
            return
        self._raise("delta > move page to", OverlayPrimaryMenuPage.PORTAL)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
