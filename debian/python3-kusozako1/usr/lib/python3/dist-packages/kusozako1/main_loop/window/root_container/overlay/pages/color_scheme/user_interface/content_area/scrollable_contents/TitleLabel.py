# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaTitleLabel(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            label=_("Color Scheme"),
            margin_top=16,
            margin_bottom=16,
            )
        self.add_css_class("kusozako-large-title")
        self._raise("delta > add to container", self)
