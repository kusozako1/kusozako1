# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity

TEMPLATE = "<a href='{}'>WebSite</a>"


class DeltaWebSite(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            use_markup=True,
           )
        home_page = self._enquiry("delta > data", "homepage")
        self.set_markup(TEMPLATE.format(home_page))
        self.add_css_class("body")
        self._raise("delta > add to container", self)
