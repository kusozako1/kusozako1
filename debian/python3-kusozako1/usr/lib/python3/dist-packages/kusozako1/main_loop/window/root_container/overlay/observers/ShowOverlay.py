# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaShowOverlay(AlfaObserver):

    SIGNAL = MainWindowSignals.SHOW_OVERLAY

    def _on_signal_received(self, page_name):
        stack = self._enquiry("delta > stack")
        visible_child_name = stack.get_visible_child_name()
        if visible_child_name == page_name and stack.get_visible():
            user_data = MainWindowSignals.CLOSE_OVERLAY, None
            self._raise("delta > main window signal", user_data)
        else:
            stack.show()
            self._raise("delta > switch stack to", page_name)
