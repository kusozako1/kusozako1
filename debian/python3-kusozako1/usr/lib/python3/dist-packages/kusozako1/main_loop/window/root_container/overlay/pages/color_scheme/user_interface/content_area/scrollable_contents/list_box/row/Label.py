# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    @classmethod
    def new_for_title(cls, parent, title):
        instance = cls(parent)
        instance.construct(title)

    def construct(self, title):
        self.set_label(title)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            xalign=0,
            hexpand=True,
            margin_start=16,
            )
        self._raise("delta > add to container", self)
