# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import OverlayPrimaryMenuPage
from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from .AutoButton import DeltaAutoButton
from .ContainButton import DeltaContainButton
from .CoverButton import DeltaCoverButton


class DeltaBackgroudStyles(AlfaSubPage):

    PAGE_NAME = OverlayPrimaryMenuPage.BACKGROUND_STYLES
    BACK_TO = OverlayPrimaryMenuPage.SETTINGS

    def _on_initialize(self):
        DeltaAutoButton(self)
        DeltaContainButton(self)
        DeltaCoverButton(self)

    def _add_to_container(self):
        user_data = self, self.PAGE_NAME
        self._raise("delta > add to stack", user_data)
