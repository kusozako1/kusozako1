# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .overlay.Overlay import DeltaOverlay
from .header_bar.HeaderBar import DeltaHeaderBar
from .content_area.ContentArea import DeltaContentArea


class DeltaRootContainer(Gtk.Overlay, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)

    def _delta_call_add_to_overlay(self, widget):
        self.add_overlay(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Overlay.__init__(self, vexpand=True, hexpand=True)
        self._box = Gtk.Box(hexpand=True, orientation=Gtk.Orientation.VERTICAL)
        self.set_child(self._box)
        DeltaHeaderBar(self)
        DeltaContentArea(self)
        DeltaOverlay(self)
        self._raise("delta > add to container", self)
