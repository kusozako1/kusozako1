# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Label import DeltaLabel
from .ColorIndicatorButton import DeltaColorIndicatorButton
from .SelectFromScreenButton import DeltaSelectFromScreenButton


class DeltaRow(Gtk.Box, DeltaEntity):

    @classmethod
    def new(cls, parent, color_scheme_entity):
        instance = cls(parent)
        instance.construct(color_scheme_entity)
        return instance

    def _delta_call_color_selected(self, color):
        print("change color but do not apply yet", color)

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def construct(self, color_scheme_entity):
        DeltaLabel.new_for_title(self, color_scheme_entity.title)
        DeltaColorIndicatorButton.new(self, color_scheme_entity)
        DeltaSelectFromScreenButton.new(self, color_scheme_entity)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, 48)
