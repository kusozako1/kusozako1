# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .KeyFile import DeltaKeyFile


class DeltaSettings(DeltaEntity):

    def _delta_call_settings(self, user_data):
        # group, key, value = user_data
        self._key_file.set(*user_data)

    def _delta_call_toggle_boolean_settings(self, user_data):
        self._key_file.toggle_boolean(*user_data)

    def _delta_info_settings(self, user_data):
        # group, key, defaault_value = user_data
        # or
        # group, key = user_data
        return self._key_file.get(*user_data)

    def _delta_call_register_settings_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_settings_changed(self, user_data):
        self._transmitter.transmit(user_data)

    def _ensure_settings_directory(self):
        id_ = self._enquiry("delta > data", "application-id")
        names = [GLib.get_user_config_dir(), id_]
        directory = GLib.build_filenamev(names)
        gfile = Gio.File.new_for_path(directory)
        if not gfile.query_exists():
            gfile.make_directory()
        return directory

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        directory = self._ensure_settings_directory()
        self._key_file = DeltaKeyFile.new_for_directory(self, directory)
        self._raise("delta > loopback settings ready", self)
        self._key_file.save()
