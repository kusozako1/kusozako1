# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaMovePrimaryMenuPage(AlfaObserver):

    SIGNAL = MainWindowSignals.MOVE_PRIMARY_MENU_PAGE

    def _on_signal_received(self, page_name):
        self._raise("delta > move page to", page_name)
