# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaEventControllerKey(Gtk.EventControllerKey, DeltaEntity):

    def _on_key_released(self, controller_key, keyval, keycode, state):
        accel = Gtk.accelerator_get_label(keyval, state)
        if accel == "Esc":
            user_data = MainWindowSignals.CLOSE_OVERLAY, None
            self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerKey.__init__(self)
        self._raise("delta > add controller", self)
        self.connect("key-released", self._on_key_released)
