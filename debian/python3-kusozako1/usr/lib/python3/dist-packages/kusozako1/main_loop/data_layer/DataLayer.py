# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .settings.Settings import DeltaSettings
from .css.Css import DeltaCss


class DeltaDataLayer(DeltaEntity):

    def _delta_call_loopback_settings_ready(self, parent):
        DeltaCss(parent)

    def __init__(self, parent):
        self._parent = parent
        DeltaSettings(self)
