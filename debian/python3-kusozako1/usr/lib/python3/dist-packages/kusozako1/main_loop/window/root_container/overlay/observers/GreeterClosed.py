# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.const import MainWindowSignals
from .Observer import AlfaObserver


class DeltaGreeterClosed(AlfaObserver):

    SIGNAL = MainWindowSignals.GREETER_CLOSED

    def _on_signal_received(self, param):
        stack = self._enquiry("delta > stack")
        stack.set_transition_duration(800)
        stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
