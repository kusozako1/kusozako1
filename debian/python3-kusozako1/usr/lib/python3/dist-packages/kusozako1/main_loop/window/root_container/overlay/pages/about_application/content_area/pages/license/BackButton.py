# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaBackButton(AlfaButton):

    START_ICON = "go-previous-symbolic"
    LABEL = _("Back")

    def _on_clicked(self, button):
        self._raise("delta > move page to", "about")
