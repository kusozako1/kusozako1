# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ContainerType import DeltaContainerType
from .Opacity import DeltaOpacity


class EchoSettings:

    def __init__(self, parent):
        DeltaContainerType(parent)
        DeltaOpacity(parent)
