# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity

TEXT = """This software is licensed under GPL v3 or any later version.
Click License Button to learn more about GPL v3."""


class DeltaLicense(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            justify=Gtk.Justification.CENTER,
            label=TEXT,
            wrap=True,
            )
        self.add_css_class("body")
        self._raise("delta > add to container", self)
