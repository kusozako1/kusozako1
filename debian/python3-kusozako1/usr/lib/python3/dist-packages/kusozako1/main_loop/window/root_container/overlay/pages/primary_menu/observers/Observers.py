# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CloseOverlay import DeltaCloseOverlay
from .AddExtraPrimaryMenu import DeltaAddExtraPrimaryMenu
from .ShowExtraPrimaryMenu import DeltaShowExtraPrimaryMenu
from .ShowExtraPrimaryMenuWithParam import DeltaShowExtraPrimaryMenuWithParam
from .MovePrimaryMenuPage import DeltaMovePrimaryMenuPage


class EchoObservers:

    def __init__(self, parent):
        DeltaCloseOverlay(parent)
        DeltaAddExtraPrimaryMenu(parent)
        DeltaShowExtraPrimaryMenu(parent)
        DeltaShowExtraPrimaryMenuWithParam(parent)
        DeltaMovePrimaryMenuPage(parent)
