# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaSpacer(Gtk.Box, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            hexpand=True,
            vexpand=True
            )
        self.add_css_class("osd")
        self._raise("delta > add to container", self)
