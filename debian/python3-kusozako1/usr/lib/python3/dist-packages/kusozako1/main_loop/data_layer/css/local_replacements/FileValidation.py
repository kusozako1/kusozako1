# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity

FILENAMES = ["additional.css", "replacements.yaml"]


class DeltaFileValidation(DeltaEntity):

    def _get_resource_directory(self):
        resource_directory = self._enquiry("delta > resource directory")
        if resource_directory is None:
            self._raise("delta > invalid")
        return resource_directory

    def _check_files(self, resource_directory):
        for filename in FILENAMES:
            path = GLib.build_filenamev([resource_directory, filename])
            if not GLib.file_test(path, GLib.FileTest.EXISTS):
                self._raise("delta > invalid")
                return
        self._raise("delta > valid", resource_directory)

    def __init__(self, parent):
        self._parent = parent
        resource_directory = self._get_resource_directory()
        if resource_directory is None:
            return
        self._check_files(resource_directory)
