# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .FileButton import AlfaFileButton


class DeltaOpenButton(AlfaFileButton):

    START_ICON = "folder-open-symbolic"
    LABEL = _("Open")
    SHORTCUT = "Ctrl+O"
    SIGNAL = MainWindowSignals.OPEN_FILE
