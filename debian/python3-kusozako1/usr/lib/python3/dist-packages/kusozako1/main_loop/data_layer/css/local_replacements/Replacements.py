# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import yaml
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaReplacements(DeltaEntity):

    @classmethod
    def new(cls, parent, resource_directory):
        instance = cls(parent)
        instance.construct(resource_directory)
        return instance

    def _get_contents(self, resource_directory, file_name):
        names = [resource_directory, file_name]
        path = GLib.build_filenamev(names)
        _, bytes_ = GLib.file_get_contents(path)
        return bytes_

    def _replace(self, css, item):
        query = "css", item["key"], item["default"]
        settings = self._enquiry("delta > settings", query)
        if settings == "KUSOZAKO1_NULL_SETTINGS":
            return css
        replacement = item["template"].format(settings)
        return css.replace(item["target"], replacement)

    def replace_all(self, css):
        for item in self._replacements:
            css = self._replace(css, item)
        return css

    def construct(self, resource_directory):
        contents = self._get_contents(resource_directory, "replacements.yaml")
        self._replacements = yaml.safe_load(contents)

    def __init__(self, parent):
        self._parent = parent
