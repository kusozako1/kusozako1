# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaQuitApplicationButton(AlfaButton):

    START_ICON = "system-shutdown-symbolic"
    LABEL = _("Quit")
    SHORTCUT = "Ctrl+Q"

    def _on_clicked(self, button):
        user_data = MainWindowSignals.TRY_CLOSE, None
        self._raise("delta > main window signal", user_data)
