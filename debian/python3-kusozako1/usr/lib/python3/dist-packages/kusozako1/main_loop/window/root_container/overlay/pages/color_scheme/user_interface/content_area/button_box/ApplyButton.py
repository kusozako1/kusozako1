# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import ColorSchemeSignals
from kusozako1.const import OverlayPage
from kusozako1.const import MainWindowSignals


class DeltaApplyButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = ColorSchemeSignals.APPLY, None
        self._raise("delta > color scheme signal", user_data)
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, label=_("Apply"))
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
