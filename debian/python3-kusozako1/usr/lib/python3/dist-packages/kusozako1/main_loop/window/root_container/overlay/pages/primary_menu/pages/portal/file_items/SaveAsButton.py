# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from .FileButton import AlfaFileButton


class DeltaSaveAsButton(AlfaFileButton):

    START_ICON = "document-save-as-symbolic"
    LABEL = _("Save As")
    SHORTCUT = "Ctrl+Shift+S"
    SIGNAL = MainWindowSignals.TRY_SAVE_FILE_AS
