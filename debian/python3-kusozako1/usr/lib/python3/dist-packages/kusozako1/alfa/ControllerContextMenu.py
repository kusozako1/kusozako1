# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class AlfaControllerContextMenu(DeltaEntity):

    def _on_pressed(self, gesture, x, y):
        raise NotImplementedError

    def _on_initialize(self):
        pass

    def _on_right_click(self, gesture, n_press, x, y):
        self._on_pressed(gesture, x, y)

    def _on_long_press(self, gesture, x, y):
        self._on_pressed(gesture, x, y)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        right_click = Gtk.GestureClick(button=3)
        self._raise("delta > add controller", right_click)
        right_click.connect("pressed", self._on_right_click)
        long_press = Gtk.GestureLongPress.new()
        self._raise("delta > add controller", long_press)
        long_press.connect("pressed", self._on_long_press)
