# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import math

r = 12


def clip(cairo_context, width, height):
    x = 0
    y = 0
    cairo_context.arc(x+r, y+r, r, math.pi, 3*math.pi/2)
    cairo_context.arc(x+width-r, y+r, r, 3*math.pi/2, 0)
    cairo_context.arc(x+width, y, 0, 0, 0)
    cairo_context.arc(x+width-r, y+height-r, r, 0, math.pi/2)
    cairo_context.arc(x+r, y+height-r, r, math.pi/2, math.pi)
    cairo_context.clip()


def top(cairo_context, width, height):
    x = 0
    y = 0
    cairo_context.arc(x+r, y+r, r, math.pi, 3*math.pi/2)
    cairo_context.arc(x+width-r, y+r, r, 3*math.pi/2, 0)
    cairo_context.arc(x+width, x+height-r, r, 0, math.pi/2)
    cairo_context.arc(x, y+height-r, r, math.pi/2, math.pi)
    cairo_context.close_path()
    cairo_context.fill()


def bottom(cairo_context, x, y, width, height):
    cairo_context.arc(x, y, 0, 0, 0)
    cairo_context.arc(x+width, y, 0, 0, 0)
    cairo_context.arc(x+width-r, y+height-r, r, 0, math.pi/2)
    cairo_context.arc(x+r, y+height-r, r, math.pi/2, math.pi)
    cairo_context.close_path()
    cairo_context.fill()
