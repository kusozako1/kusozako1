# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Pango
from gi.repository import PangoCairo
from kusozako1.util import GtkFontDescription

MARGIN = 4


class FoxtrotLayoutFactory:

    @classmethod
    def get_default(cls):
        if "_layout_factory" not in dir(cls):
            cls._layout_factory = cls()
        return cls._layout_factory

    def build_(self, cairo_context, width, max_height):
        layout = PangoCairo.create_layout(cairo_context)
        layout.set_alignment(Pango.Alignment.CENTER)
        layout.set_font_description(self._font_description)
        layout.set_width((width-MARGIN*2)*Pango.SCALE)
        layout.set_height(max_height*Pango.SCALE)
        layout.set_wrap(Pango.WrapMode.WORD_CHAR)
        layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        return layout.copy()

    def __init__(self):
        self._font_description = GtkFontDescription.get_description()
