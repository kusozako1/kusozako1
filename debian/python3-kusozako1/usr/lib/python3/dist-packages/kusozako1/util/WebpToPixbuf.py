# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from PIL import Image
from gi.repository import GLib
from gi.repository import GdkPixbuf


def _convert_to_pixbuf(webp_image):
    width, height = webp_image.size
    glib_bytes = GLib.Bytes.new(webp_image.tobytes())
    return GdkPixbuf.Pixbuf.new_from_bytes(
        glib_bytes,                 # image data in 8-bit per sample
        GdkPixbuf.Colorspace.RGB,   # color space.
        False,                      # has alpha
        8,                          # bits per sample
        width,                      # width in pixel
        height,                     # height in pixel
        width*3                     # distance in bytes between row
        )


def from_path(source_path):
    webp_image = Image.open(source_path)
    return _convert_to_pixbuf(webp_image)


def from_path_at_size(source_path, width, height):
    webp_image = Image.open(source_path)
    resized_webp_image = webp_image.resize(width, height)
    return _convert_to_pixbuf(resized_webp_image)
