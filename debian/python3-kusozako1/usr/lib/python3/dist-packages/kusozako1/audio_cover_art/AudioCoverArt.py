# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from .Mp3 import FoxtrotMp3
from .Mp4 import FoxtrotMp4


class FoxtrotAudioCoverArt:

    @classmethod
    def new_for_maximum_size(cls, maximum_size):
        return cls(maximum_size)

    def _load_thumbnail(self, path, mime):
        if mime == "audio/mpeg":
            return self._mp3.load_thumbnail(path)
        elif mime == "audio/mp4":
            return self._mp4.load_thumbnail(path)
        return None

    def get_for_uri(self, uri):
        gfile = Gio.File.new_for_uri(uri)
        if not gfile.query_exists():
            return None
        file_info = gfile.query_info("*", 0)
        mime = file_info.get_content_type()
        return self._load_thumbnail(gfile.get_path(), mime)

    def __init__(self, maximum_size):
        self._mp3 = FoxtrotMp3(maximum_size)
        self._mp4 = FoxtrotMp4(maximum_size)
