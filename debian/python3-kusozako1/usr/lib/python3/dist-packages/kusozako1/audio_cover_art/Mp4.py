
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import mutagen
from gi.repository import Gio
from gi.repository import GLib
from .ThumbnailLoader import AlfaThumbnailLoader


class FoxtrotMp4(AlfaThumbnailLoader):

    def _get_for_mp4(self, mp4_cover):
        bytes_ = GLib.Bytes.new(mp4_cover)
        return Gio.MemoryInputStream.new_from_bytes(bytes_)

    def _get_stream_for_path(self, path):
        mutagen_file = mutagen.File(path)
        if "covr" in mutagen_file:
            return self._get_for_mp4(mutagen_file["covr"][0])
        return None
