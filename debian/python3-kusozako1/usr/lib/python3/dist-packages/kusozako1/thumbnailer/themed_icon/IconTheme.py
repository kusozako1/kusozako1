# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotIconTheme(Gtk.IconTheme):

    def _get_theme_name(self):
        settings = Gtk.Settings.get_default()
        theme_name = settings.props.gtk_icon_theme_name
        if theme_name is None:
            theme_name = "Adwaita"
        return theme_name

    def _get_paintable(self, icon_names):
        for icon_name in icon_names:
            icon_paintable = self.lookup_icon(
                icon_name,
                icon_names,
                512,
                1,
                Gtk.TextDirection.NONE,
                Gtk.IconLookupFlags.PRELOAD,
                )
            gfile = icon_paintable.get_file()
            path = gfile.get_path()
            if path is None:
                continue
            return icon_paintable, gfile, path

    def get_data_for_names(self, icon_names):
        response = self._get_paintable(icon_names)
        if response is None:
            return None, None, None
        icon_paintable, gfile, path = response
        file_info = gfile.query_info("standard::content-type", 0)
        content_type = file_info.get_content_type()
        is_svg = (content_type == "image/svg+xml")
        name = icon_paintable.get_icon_name()
        return path, is_svg, name

    def __init__(self, theme_name=None):
        if theme_name is None:
            theme_name = self._get_theme_name()
        Gtk.IconTheme.__init__(self, theme_name=theme_name)
