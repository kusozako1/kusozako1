# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from kusozako1.const import ThumbnailLoadingState
from kusozako1.const import PixbufOptionKeys
from .Builder import FoxtrotBuilder


class DeltaLoader(DeltaEntity):

    def _is_valid(self, pixbuf, thumbnail_path, file_info):
        pixbuf_mtime = int(pixbuf.get_option(PixbufOptionKeys.MTIME))
        file_mtime = file_info.get_attribute_uint64("time::modified")
        is_valid = (pixbuf_mtime == file_mtime)
        if not is_valid:
            pixbuf_file = Gio.File.new_for_path(thumbnail_path)
            pixbuf_file.delete()
        return is_valid

    def _get_pixbuf(self, thumbnail_path, file_info, callback):
        if not GLib.file_test(thumbnail_path, GLib.FileTest.EXISTS):
            self._builder.build(thumbnail_path, file_info, callback)
            return None
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(thumbnail_path)
        if not self._is_valid(pixbuf, thumbnail_path, file_info):
            self._builder.build(thumbnail_path, file_info, callback)
            return None
        return pixbuf

    def _idle(self, thumbnail_path, file_info, callback):
        pixbuf = self._get_pixbuf(thumbnail_path, file_info, callback)
        if pixbuf is None:
            return
        user_data = thumbnail_path, pixbuf
        self._raise("delta > thumbnail built", user_data)
        callback(pixbuf, ThumbnailLoadingState.FINISHED)

    def load_async(self, thumbnail_path, file_info, callback):
        GLib.idle_add(self._idle, thumbnail_path, file_info, callback)

    def __init__(self, parent):
        self._parent = parent
        self._builder = FoxtrotBuilder()
