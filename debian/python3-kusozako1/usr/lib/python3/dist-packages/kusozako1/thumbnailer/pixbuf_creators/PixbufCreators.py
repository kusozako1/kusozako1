# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Video import FoxtrotVideo
from .Webp import FoxtrotWebp
from .Svg import FoxtrotSvg
from .Image import FoxtrotImage
from .AudioXm4a import FoxtrotAudioXm4a
from .Pdf import FoxtrotPdf


class FoxtrotPixbufCreators:

    def dispatch_for_mime(self, mime):
        for key, creator in self._creators.items():
            if mime.startswith(key):
                return creator
        return None

    def __init__(self):
        self._creators = {}
        self._creators["image/webp"] = FoxtrotWebp()
        self._creators["image/svg"] = FoxtrotSvg()
        self._creators["image/"] = FoxtrotImage()
        self._creators["video/"] = FoxtrotVideo()
        self._creators["audio/mp4"] = FoxtrotAudioXm4a()
        self._creators["application/pdf"] = FoxtrotPdf()
