# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from gi.repository import Rsvg
from kusozako1.const import ThumbnailLoadingState
from .IconTheme import FoxtrotIconTheme

SIZE = 128
INTERP = GdkPixbuf.InterpType.BILINEAR


class FoxtrotThemedIcon:

    def _try_shrink_pixbuf(self, pixbuf):
        if pixbuf.get_width() > SIZE or pixbuf.get_height() > SIZE:
            return pixbuf.scale_simple(SIZE, SIZE, INTERP)
        return pixbuf

    def _build_pixbuf(self, icon_names):
        path, is_svg, name = self._icon_theme.get_data_for_names(icon_names)
        if is_svg:
            handle = Rsvg.Handle.new_from_file(path)
            pixbuf = handle.get_pixbuf()
        elif path is None:
            name = icon_names[0]
            pixbuf = self._build_pixbuf(["text-x-generic"])
        else:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        pixbuf = self._try_shrink_pixbuf(pixbuf)
        self._cache[name] = pixbuf
        return pixbuf

    def _from_cache(self, icon_names):
        for icon_name in icon_names:
            if icon_name in self._cache:
                return self._cache[icon_name]
        return None

    def load_icon(self, file_info, callback):
        gicon = file_info.get_icon()
        names = gicon.get_names()
        pixbuf = self._from_cache(names)
        if pixbuf is None:
            pixbuf = self._build_pixbuf(names)
        callback(pixbuf, ThumbnailLoadingState.ICON_LOADED)

    def __init__(self, theme_name=None):
        self._icon_theme = FoxtrotIconTheme(theme_name)
        self._cache = {}
