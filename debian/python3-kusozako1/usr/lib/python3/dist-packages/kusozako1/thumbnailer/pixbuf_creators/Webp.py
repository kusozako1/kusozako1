# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from kusozako1.util import WebpToPixbuf
from kusozako1.const import PixbufOptionKeys
from .PixbufCreator import BravoPixbufCreator


class FoxtrotWebp(BravoPixbufCreator):

    def _get_options(self, gfile, file_info, width, height):
        options = {}
        options[PixbufOptionKeys.URI] = gfile.get_uri()
        options[PixbufOptionKeys.SIZE] = str(file_info.get_size())
        mtime = file_info.get_attribute_uint64("time::modified")
        options[PixbufOptionKeys.MTIME] = str(mtime)
        options[PixbufOptionKeys.MIMETYPE] = file_info.get_content_type()
        options[PixbufOptionKeys.SOFTWARE] = PixbufOptionKeys.SOFTWARE_NAME
        options[PixbufOptionKeys.WIDTH] = str(width)
        options[PixbufOptionKeys.HEIGHT] = str(height)
        return options

    def create_from_file_info(self, file_info):
        gfile = self._get_gfile(file_info)
        path = gfile.get_path()
        pixbuf = WebpToPixbuf.from_path(path)
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(1, max(128/width, 128/height))
        new_pixbuf = pixbuf.scale_simple(
            int(width*scale),
            int(height*scale),
            GdkPixbuf.InterpType.BILINEAR
            )
        options = self._get_options(gfile, file_info, width, height)
        return new_pixbuf, options
