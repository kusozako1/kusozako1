# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from gi.repository import Poppler
from gi.repository import GdkPixbuf
from kusozako1.const import PixbufOptionKeys
from .PixbufCreator import BravoPixbufCreator

INTERP = GdkPixbuf.InterpType.BILINEAR


class FoxtrotPdf(BravoPixbufCreator):

    def _get_options(self, gfile, file_info, n_pages):
        options = {}
        options[PixbufOptionKeys.URI] = gfile.get_uri()
        options[PixbufOptionKeys.SIZE] = str(file_info.get_size())
        mtime = file_info.get_attribute_uint64("time::modified")
        options[PixbufOptionKeys.MTIME] = str(mtime)
        options[PixbufOptionKeys.MIMETYPE] = file_info.get_content_type()
        options[PixbufOptionKeys.SOFTWARE] = PixbufOptionKeys.SOFTWARE_NAME
        options[PixbufOptionKeys.PAGES] = str(n_pages)
        return options

    def create_from_file_info(self, file_info):
        gfile = self._get_gfile(file_info)
        document = Poppler.Document.new_from_file(gfile.get_uri(), None)
        page = document.get_page(0)
        size = page.get_size()
        width, height = int(size.width), int(size.height)
        surface = cairo.ImageSurface(cairo.Format.ARGB32, width, height)
        cairo_context = cairo.Context(surface)
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.rectangle(0, 0, width, height)
        cairo_context.fill()
        page.render(cairo_context)
        pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, width, height)
        ratio = min(1, max(128/width, 128/height))
        pixbuf = pixbuf.scale_simple(width*ratio, height*ratio, INTERP)
        options = self._get_options(gfile, file_info, document.get_n_pages())
        return pixbuf, options
