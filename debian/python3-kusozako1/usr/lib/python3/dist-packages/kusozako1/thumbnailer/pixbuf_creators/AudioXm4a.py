# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import mutagen
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako1.const import PixbufOptionKeys
from .PixbufCreator import BravoPixbufCreator

INTERP_TYPE = GdkPixbuf.InterpType.BILINEAR


class FoxtrotAudioXm4a(BravoPixbufCreator):

    def _get_options(self, gfile, file_info):
        options = {}
        mime = file_info.get_content_type()
        options[PixbufOptionKeys.URI] = gfile.get_uri()
        options[PixbufOptionKeys.SIZE] = str(file_info.get_size())
        mtime = file_info.get_attribute_uint64("time::modified")
        options[PixbufOptionKeys.MTIME] = str(mtime)
        options[PixbufOptionKeys.MIMETYPE] = mime
        options[PixbufOptionKeys.SOFTWARE] = PixbufOptionKeys.SOFTWARE_NAME
        return options

    def _get_for_mp4(self, mp4_cover):
        bytes_ = GLib.Bytes.new(mp4_cover)
        return Gio.MemoryInputStream.new_from_bytes(bytes_)

    def _get_thumbnail_size(self, pixbuf):
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        rate = min(width/128, height/128)
        return rate, (width/rate), (height/rate)

    def _get_scaled(self, stream):
        pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream, None)
        rate, width, height = self._get_thumbnail_size(pixbuf)
        if 1 >= rate:
            return pixbuf
        return pixbuf.scale_simple(width, height, INTERP_TYPE)

    def create_from_file_info(self, file_info):
        gfile = self._get_gfile(file_info)
        path = gfile.get_path()
        mutagen_file = mutagen.File(path)
        if "covr" not in mutagen_file:
            return None
        options = self._get_options(gfile, file_info)
        stream = self._get_for_mp4(mutagen_file["covr"][0])
        pixbuf = self._get_scaled(stream)
        return pixbuf, options
