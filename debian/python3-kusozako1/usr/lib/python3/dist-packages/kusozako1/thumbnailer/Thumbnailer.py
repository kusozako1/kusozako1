# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import ThumbnailLoadingState
from kusozako1.Entity import DeltaEntity
from .Checksum import FoxtrotChecksum
from .Loader import DeltaLoader
from .themed_icon.ThemedIcon import FoxtrotThemedIcon
from .Cache import FoxtrotCache

NEEDS_THUMBNAIL = ("audio", "video", "image", "application/pdf")


class FoxtrotThumbnailer(DeltaEntity):

    @classmethod
    def get_default(cls):
        if "_default" not in dir(cls):
            cls._default = cls()
        return cls._default

    def _load_thumbnail(self, thumbnail_path, file_info, callback):
        pixbuf = self._cache.get(thumbnail_path, file_info)
        if pixbuf is not None:
            callback(pixbuf, ThumbnailLoadingState.FINISHED)
        else:
            self._themed_icon.load_icon(file_info, callback)
            self._loader.load_async(thumbnail_path, file_info, callback)

    def _delta_call_thumbnail_built(self, user_data):
        thumbnail_path, pixbuf = user_data
        self._cache[thumbnail_path] = pixbuf

    def change_thumbnail_cache(self, thumbnail_path, pixbuf):
        self._cache[thumbnail_path] = pixbuf

    def load_from_file_info_async(self, file_info, callback):
        path = file_info.get_attribute_string("kusozako1::thumbnail-path")
        if path is None:
            self._themed_icon.load_icon(file_info, callback)
        else:
            self._load_thumbnail(path, file_info, callback)

    def _needs_thumbnail(self, file_info):
        content_type = file_info.get_content_type()
        for key in NEEDS_THUMBNAIL:
            if content_type.startswith(key):
                return True
        return False

    def _get_file_target_uri(self, file_info):
        gfile = file_info.get_attribute_object("standard::file")
        if not gfile.query_exists():
            print("thumbnailer:gfile not exist", gfile.get_path())
            return
        if gfile.get_uri_scheme() == "recent":
            return file_info.get_attribute_string("standard::target-uri")
        return gfile.get_uri()

    def load_from_file_info_async_2(self, file_info, callback):
        if not self._needs_thumbnail(file_info):
            self._themed_icon.load_icon(file_info, callback)
        else:
            uri = self._get_file_target_uri(file_info)
            if uri is None:
                return
            thumbnail_path = self._checksum.get_thumbnail_path_for_uri(uri)
            self._load_thumbnail(thumbnail_path, file_info, callback)

    def __init__(self):
        self._parent = None
        self._themed_icon = FoxtrotThemedIcon()
        self._checksum = FoxtrotChecksum.get_default()
        self._cache = FoxtrotCache()
        self._loader = DeltaLoader(self)
