# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.const import ThumbnailLoadingState
from .pixbuf_creators.PixbufCreators import FoxtrotPixbufCreators

NAMES = [GLib.get_user_cache_dir(), "kusozako1", "thumbnail", "128"]
DIRECTORY = GLib.build_filenamev(NAMES)


class FoxtrotBuilder:

    def _save(self, pixbuf, path, options):
        pixbuf.savev(path, "png", list(options.keys()), list(options.values()))

    def _build(self, thumbnail_path, file_info, callback):
        mime = file_info.get_content_type()
        creator = self._pixbuf_creators.dispatch_for_mime(mime)
        if creator is None:
            return
        response = creator.create_from_file_info(file_info)
        if response is None:
            return
        thumbnail, options = response
        GLib.idle_add(self._save, thumbnail, thumbnail_path, options)
        callback(thumbnail, ThumbnailLoadingState.FINISHED)

    def build(self, thumbnail_path, file_info, callback):
        GLib.idle_add(self._build, thumbnail_path, file_info, callback)
        return None

    def __init__(self):
        self._pixbuf_creators = FoxtrotPixbufCreators()
