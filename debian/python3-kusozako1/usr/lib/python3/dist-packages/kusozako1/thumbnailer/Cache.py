# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import PixbufOptionKeys


class FoxtrotCache:

    def _is_valid(self, pixbuf, thumbnail_path, file_info):
        option = pixbuf.get_option(PixbufOptionKeys.MTIME)
        if option is None:
            return False
        pixbuf_mtime = int(option)
        file_mtime = file_info.get_attribute_uint64("time::modified")
        return pixbuf_mtime == file_mtime

    def __setitem__(self, key, value):
        self._cache[key] = value

    def get(self, thumbnail_path, file_info):
        pixbuf = self._cache.get(thumbnail_path, None)
        if pixbuf is None:
            return None
        is_valid = self._is_valid(pixbuf, thumbnail_path, file_info)
        if not is_valid:
            del self._cache[thumbnail_path]
            return None
        return pixbuf

    def __init__(self):
        self._cache = {}
