# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PRIMARY_FG_COLOR = "primary_fg_color"
PRIMARY_BG_COLOR = "primary_bg_color"
HIGHLIGHT_FG_COLOR = "highlight_fg_color"
HIGHLIGHT_BG_COLOR = "highlight_bg_color"
