# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

URI = "tEXt::Thumb::URI"
MTIME = "tEXt::Thumb::MTime"
SIZE = "tEXt::Thumb::Size"
MIMETYPE = "tEXt::Thumb::Mimetype"
SOFTWARE = "tEXt::Software"

# IMAGE FILE ONLY
WIDTH = "tEXt::Image::Width"
HEIGHT = "tEXt::Image::Height"

# DOCUMENT FILE ONLY
PAGES = "tEXt::Thumb::Document::Pages"

# VIDEO FILES ONLY
LENGTH = "tEXt::Thumb::Movie::Length"

# VALUE FOR SOFTWARE KEY
SOFTWARE_NAME = "kusozako1::kusozako1"
