# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

NOT_STARTED = "not-started"
ICON_LOADED = "icon-loaded"
PENDING = "pending"
FINISHED = "finished"
ERROR = "error"
