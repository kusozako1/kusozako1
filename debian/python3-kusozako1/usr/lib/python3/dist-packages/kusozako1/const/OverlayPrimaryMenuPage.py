# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PORTAL = "portal"
SETTINGS = "settings"
HEADER_BAR_TYPES = "header-bar-types"
BACKGROUND_STYLES = "background-styles"
