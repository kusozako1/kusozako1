# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SELECTED = 0
CREATED = 1
MOVED_IN = 2
MOVED_OUT = 3
RENAMED = 4
SYMLINK = 5
READ_ONLY = 6
EXECUTABLE = 7
INACCESIBLE = 8
