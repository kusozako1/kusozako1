# (c) copyright 2022-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SHOW_OVERLAY = "show-overlay"                   # page_name as string
SHOW_DIALOG = "show-dialog"                     # dict as model
CLOSE_OVERLAY = "close-overlay"                 # None
GREETER_CLOSED = "greeter-closed"               # None
OVERLAY_CLOSED = "overlay-closed"               # None
OVERLAY_STACK_CLOSED = "overlay-stack-closed"   # None
TOGGLE_MAXIMIZED = "toggle-maximized"           # None
TOGGLE_FULLSCREEN = "toggle-fullscreen"         # None
ICONIFY = "iconify"                             # None
TRY_CLOSE = "try-close"                         # None
ABOUT_TO_CLOSE = "about-to-close"               # None
FORCE_CLOSE = "force-close"                     # None
NEW_FILE = "new-file"                           # None
OPEN_FILE = "open-file"                         # None
TRY_OPEN_FILE = "try-open-file"                 # None
FILE_OPENED = "file-opened"                     # gfile
TRY_SAVE_FILE = "try-save-file"                 # None
TRY_SAVE_FILE_AS = "try-save-file-as"           # None
EDIT_FIND = "edit-find"                         # None
ADD_EXTRA_OVERLAY = "add-extra-overlay"         # (widget, page_name)
ADD_EXTRA_MENU = "add-extra-menu"               # widget
ADD_EXTRA_PRIMARY_MENU = "add-extra-primary-menu"       # (widget, page_name)
SHOW_EXTRA_PRIMARY_MENU = "show-extra-primary-menu"     # page_name
SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM = "show-extra-primary-menu-with-param"
# (page_name, user_data)
MOVE_PRIMARY_MENU_PAGE = "move-primary-menu-page"   # page_name
SHOW_EXTRA_OVERLAY = "show-extra-overlay"       # (page_name, user_data)
ADD_SHORTCUT_GROUP = "add-shortcut-group"       # (group_name, label)
ADD_SHORTCUT = "add-shortcut"
# (group_name, accelerator, title)
ADD_SHORTCUT_WITH_SIGNAL = "add-shortcut-with-signal"
# shortcut, signal, param
CHANGE_TITLE = "change-title"                   # title as str
ADD_GLOBAL_ACCEL = "add-global-accel"           # shortcut, signal, param
REPLACE_TERMINATOR = "replace-terminator"       # terminator
