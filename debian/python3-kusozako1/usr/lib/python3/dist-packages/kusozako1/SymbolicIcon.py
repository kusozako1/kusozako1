# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk

ICON_THEME = Gtk.IconTheme(theme_name="Adwaita")


def get_image_for_name(icon_name):
    paintable = get_paintable_for_name(icon_name)
    return Gtk.Image.new_from_paintable(paintable)


def get_paintable_for_name(icon_name, icon_size=-1, display_scale=1):
    return ICON_THEME.lookup_icon(
        icon_name,
        None,                               # fallback
        icon_size,                          # icon size
        display_scale,                      # display scale
        Gtk.TextDirection.NONE,
        Gtk.IconLookupFlags.FORCE_SYMBOLIC
        )
