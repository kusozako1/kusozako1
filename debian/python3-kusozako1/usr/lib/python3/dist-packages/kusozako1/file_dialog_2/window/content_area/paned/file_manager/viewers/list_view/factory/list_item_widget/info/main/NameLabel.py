# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotNameLabel(Gtk.Inscription):

    def bind(self, file_info):
        display_name = file_info.get_display_name()
        self.set_text(display_name)

    def __init__(self):
        Gtk.Inscription.__init__(
            self,
            hexpand=True,
            text_overflow=Gtk.InscriptionOverflow.ELLIPSIZE_MIDDLE,
            nat_lines=3,
            )
