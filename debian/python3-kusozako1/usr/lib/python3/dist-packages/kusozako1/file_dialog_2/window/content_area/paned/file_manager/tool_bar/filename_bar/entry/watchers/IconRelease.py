# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaIconRelease(DeltaEntity):

    def _on_icon_release(self, entry, icon_position):
        if icon_position == Gtk.EntryIconPosition.SECONDARY:
            entry.set_text("")

    def __init__(self, parent):
        self._parent = parent
        entry = self._enquiry("delta > entry")
        entry.connect("icon-release", self._on_icon_release)
