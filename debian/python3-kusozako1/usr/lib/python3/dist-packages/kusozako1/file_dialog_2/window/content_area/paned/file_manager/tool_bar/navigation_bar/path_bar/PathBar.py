# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .buttons.Buttons import DeltaButtons
from .DeepestPath import DeltaDeepestPath


class DeltaPathBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.prepend(widget)

    def _delta_call_remove_from_container(self, widget):
        self.remove(widget)

    def _delta_call_deepest_path_moved(self, gfile):
        self._buttons.set_gfile(gfile)

    def _delta_call_directory_lost(self, gfile):
        self._buttons.remove(gfile)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(propagate_natural_height=True)
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            hexpand=True,
            )
        scrolled_window.set_child(self)
        DeltaDeepestPath(self)
        self._buttons = DeltaButtons(self)
        self._raise("delta > add to container", scrolled_window)
