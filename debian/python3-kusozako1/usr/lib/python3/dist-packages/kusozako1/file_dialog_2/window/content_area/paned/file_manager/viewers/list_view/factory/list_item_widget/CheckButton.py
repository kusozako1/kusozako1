# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotCheckButton(Gtk.CheckButton):

    def _is_changed(self, selection_model, position, length):
        for index in range(position, position+length):
            if selection_model[index].get_name() == self._filename:
                return True, selection_model.is_selected(index)
        return False, None

    def _on_selection_changed(self, selection_model, position, length):
        changed, selected = self._is_changed(selection_model, position, length)
        if not changed:
            return
        self.set_active(selected)

    def bind(self, file_info):
        self._filename = file_info.get_name()

    def __init__(self, model):
        model.connect("selection-changed", self._on_selection_changed)
        Gtk.CheckButton.__init__(self)
        self.set_size_request(32, -1)
        self.add_css_class("selection-mode")
