# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import OverlayPages


class DeltaGreeter(Gtk.Label, DeltaEntity):

    def _timeout(self):
        self._raise("delta > hide overlay")

    def __init__(self, parent):
        self._parent = parent
        text = self._enquiry("delta > config", "title")
        Gtk.Label.__init__(self, label=text, wrap=True)
        self.set_opacity(0.8)
        self.add_css_class("kusozako-large-title")
        self.add_css_class("osd")
        self._raise("delta > add to stack", (self, OverlayPages.GREETER))
        GLib.timeout_add(500, self._timeout)
