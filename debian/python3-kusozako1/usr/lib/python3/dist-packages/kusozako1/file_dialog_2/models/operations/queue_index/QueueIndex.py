# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import DialogTypes

CLASSES = {
    DialogTypes.SELECT_DIRECTORY: ("SelectDirectory", "DeltaSelectDirectory"),
    DialogTypes.SELECT_FILE: ("SelectFile", "DeltaSelectFile"),
    DialogTypes.SAVE_FILE: ("SaveFile", "DeltaSaveFile"),
    }


class DeltaQueueIndex(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        dialog_type = self._enquiry("delta > config", "dialog_type")
        module_name, class_name = CLASSES[dialog_type]
        target_module = "{}.{}".format(__package__, module_name)
        class_ = getattr(importlib.import_module(target_module), class_name)
        class_(self)
