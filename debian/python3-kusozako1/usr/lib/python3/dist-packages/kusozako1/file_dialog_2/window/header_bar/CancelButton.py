# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaCancelButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        window = self._enquiry("delta > dialog window")
        window.close()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Cancel"),
            has_frame=True,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            )
        self.add_css_class("kusozako-headerbar-button")
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > pack start", self)
