# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel
from .sort_model.SortModel import CharlieSortModel


class DeltaModels(DeltaEntity):

    def set_file_info(self, file_info):
        self._base_model.set_file_info(file_info)

    def get_model(self):
        return self._sort_model

    def __init__(self, parent):
        self._parent = parent
        self._base_model = DeltaBaseModel(self)
        self._sort_model = CharlieSortModel.new_for_model(self._base_model)
