# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import PreviewerTypes
from .flow_box.FlowBox import DeltaFlowBox
from .metadata.Metadata import DeltaMetadata


class DeltaVideo(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def set_file_info(self, file_info):
        self._flow_box.set_file_info(file_info)
        self._metadata.set_file_info_async(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=8,
            )
        self.add_css_class("osd")
        self._flow_box = DeltaFlowBox(self)
        self._metadata = DeltaMetadata(self)
        user_data = self, PreviewerTypes.VIDEO
        self._raise("delta > add to stack", user_data)
