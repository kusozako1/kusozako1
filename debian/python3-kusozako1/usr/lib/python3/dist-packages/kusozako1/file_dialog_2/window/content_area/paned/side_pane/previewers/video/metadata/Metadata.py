# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from pymediainfo import MediaInfo
from .Markup import FoxtrotMarkup


class DeltaMetadata(Gtk.Label, DeltaEntity):

    def set_file_info_async(self, file_info):
        path = file_info.get_attribute_string("kusozako1::path")
        media_info = MediaInfo.parse(path)
        video_track = media_info.video_tracks[0]
        markup = self._markup.build_(video_track)
        self.set_markup(markup)

    def __init__(self, parent):
        self._parent = parent
        self._markup = FoxtrotMarkup()
        Gtk.Label.__init__(
            self,
            wrap=True,
            margin_top=8,
            margin_bottom=16,
            margin_start=8,
            margin_end=8,
            use_markup=True,
            )
        self._raise("delta > add to container", self)
