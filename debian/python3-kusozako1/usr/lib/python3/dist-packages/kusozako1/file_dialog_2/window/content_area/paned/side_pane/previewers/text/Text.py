# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from kusozako1.const import PreviewerTypes
from .Loader import FoxtrotLoader
from .Buffer import FoxtrotBuffer


class DeltaText(GtkSource.View, DeltaEntity):

    def set_file_info(self, file_info):
        uri = file_info.get_attribute_string("kusozako1::uri")
        self._buffer.guess_language_for_uri(uri)
        self._loader.load_async_for_uri(uri, self._buffer)

    def __init__(self, parent):
        self._parent = parent
        self._loader = FoxtrotLoader()
        self._buffer = FoxtrotBuffer()
        scrolled_window = Gtk.ScrolledWindow()
        GtkSource.View.__init__(self, buffer=self._buffer)
        scrolled_window.set_child(self)
        user_data = scrolled_window, PreviewerTypes.TEXT
        self._raise("delta > add to stack", user_data)
