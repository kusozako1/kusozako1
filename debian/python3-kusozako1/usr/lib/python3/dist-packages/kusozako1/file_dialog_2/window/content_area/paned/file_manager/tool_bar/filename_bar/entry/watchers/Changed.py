# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaChanged(DeltaEntity):

    def _on_changed(self, entry):
        filename = entry.get_text()
        user_data = FileDialogSignals.FILENAME_CHANGED, filename
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        entry = self._enquiry("delta > entry")
        entry.connect("changed", self._on_changed)
