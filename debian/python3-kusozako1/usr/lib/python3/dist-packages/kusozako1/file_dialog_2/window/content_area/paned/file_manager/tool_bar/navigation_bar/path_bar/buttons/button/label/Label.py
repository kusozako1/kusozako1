# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako1.Entity import DeltaEntity
from .Motion import DeltaMotion
from .CurrentGFile import DeltaCurrentGFile


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _delta_call_directory_moved(self, equal):
        if equal:
            self.add_css_class("kusozako-button-indicator-highlight")
        else:
            self.remove_css_class("kusozako-button-indicator-highlight")

    def _delta_call_extended_changed(self, extended):
        if extended or self._current_gfile.equal:
            self.set_ellipsize(Pango.EllipsizeMode.NONE)
        else:
            self.set_ellipsize(Pango.EllipsizeMode.END)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def __init__(self, parent):
        self._parent = parent
        gfile = self._enquiry("delta > button gfile")
        Gtk.Label.__init__(
            self,
            label=gfile.get_basename(),
            ellipsize=Pango.EllipsizeMode.END,
            tooltip_text=gfile.get_basename()
            )
        self.set_size_request(16, -1)
        self._current_gfile = DeltaCurrentGFile(self)
        self._current_gfile.start()
        DeltaMotion(self)
        self._raise("delta > add to container", self)
