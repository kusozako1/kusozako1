# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Motion import DeltaMotion
from .GestureDrag import DeltaGestureDrag


class EchoControllers:

    def __init__(self, parent):
        DeltaMotion(parent)
        DeltaGestureDrag(parent)
