# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject

SELECTION_CHANGED = (GObject.SIGNAL_RUN_FIRST, None, (bool,))


class FoxtrotSelectionModelWatcher(GObject.Object):

    __gsignals__ = {
        "selection-changed": SELECTION_CHANGED
        }

    def _is_changed(self, selection_model, position, length):
        for index in range(position, position+length):
            file_info = selection_model[index]
            if file_info.get_name() == self._filename:
                return True, selection_model.is_selected(index)
        return False, None

    def _on_selection_changed(self, selection_model, position, length):
        changed, selected = self._is_changed(selection_model, position, length)
        if changed:
            self.emit("selection-changed", selected)

    def bind(self, file_info):
        self._filename = file_info.get_name()

    def __init__(self, model):
        GObject.Object.__init__(self)
        model.connect("selection-changed", self._on_selection_changed)
