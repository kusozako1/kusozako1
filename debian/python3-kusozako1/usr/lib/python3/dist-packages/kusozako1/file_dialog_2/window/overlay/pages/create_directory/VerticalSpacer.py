# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaVerticalSpacer(Gtk.Box, DeltaEntity):

    def _on_pressed(self, *args):
        self._raise("delta > hide overlay")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, hexpand=True, vexpand=True)
        gesture = Gtk.GestureClick()
        gesture.connect("pressed", self._on_pressed)
        self.add_controller(gesture)
        self._raise("delta > add to container", self)
