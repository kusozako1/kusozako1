# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import PreviewerTypes
from .directory.Directory import DeltaDirectory
from .image.Image import DeltaImage
from .text.Text import DeltaText
from .pdf.Pdf import DeltaPdf
from .video.Video import DeltaVideo
from .Dispatch import DeltaDispatch


class DeltaPreviewers(DeltaEntity):

    def _delta_call_show_preview(self, user_data):
        previewer_type, file_info = user_data
        if previewer_type not in self._previewers:
            return
        previewer = self._previewers[previewer_type]
        previewer.set_file_info(file_info)
        self._raise("delta > switch stack to", previewer_type)

    def __init__(self, parent):
        self._parent = parent
        self._previewers = {}
        self._previewers[PreviewerTypes.DIRECTORY] = DeltaDirectory(self)
        self._previewers[PreviewerTypes.IMAGE] = DeltaImage(self)
        self._previewers[PreviewerTypes.TEXT] = DeltaText(self)
        self._previewers[PreviewerTypes.PDF] = DeltaPdf(self)
        self._previewers[PreviewerTypes.VIDEO] = DeltaVideo(self)
        DeltaDispatch(self)
