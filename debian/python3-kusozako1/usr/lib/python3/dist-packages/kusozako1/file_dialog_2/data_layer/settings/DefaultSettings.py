# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import ViewerTypes

DEFAULT_SETTINGS = {
    "show-hidden": False,
    "viewer-type": ViewerTypes.GRID_VIEW,
}


class DeltaDefaultSettings(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        for key_value in DEFAULT_SETTINGS.items():
            self._raise("delta > set setting", key_value)
