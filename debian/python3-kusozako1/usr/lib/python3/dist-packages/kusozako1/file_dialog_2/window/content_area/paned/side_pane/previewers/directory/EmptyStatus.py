# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from kusozako1.Entity import DeltaEntity


class DeltaEmptyStatus(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        status_page = Adw.StatusPage(
            icon_name="dialog-warning-symbolic",
            description="Empty Directory",
            )
        status_page.add_css_class("compact")
        status_page.add_css_class("osd")
        user_data = status_page, "empty-status"
        self._raise("delta > add to stack", user_data)
