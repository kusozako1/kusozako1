# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaXdgUserDirs(DeltaEntity):

    def _build_gfile(self, path):
        gfile = Gio.File.new_for_path(path)
        self._raise("delta > gfile found", gfile)

    def __init__(self, parent):
        self._parent = parent
        self._build_gfile(GLib.get_home_dir())
        for index in range(0, GLib.UserDirectory.N_DIRECTORIES):
            path = GLib.get_user_special_dir(index)
            self._build_gfile(path)
