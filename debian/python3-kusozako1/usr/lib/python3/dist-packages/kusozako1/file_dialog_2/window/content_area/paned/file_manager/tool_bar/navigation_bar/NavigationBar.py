# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import DialogTypes
from .path_bar.PathBar import DeltaPathBar
from .CreateDirectoryButton import DeltaCreateDirectoryButton
from .grid_view_button.GridViewButton import DeltaGridViewButton
from .list_view_button.ListViewButton import DeltaListViewButton
from .toggle_show_hidden.ToggleShowHidden import DeltaToggleShowHidden


class DeltaNavigationBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.add_css_class("kusozako-primary-surface")
        self.set_size_request(-1, 48)
        DeltaPathBar(self)
        dialog_type = self._enquiry("delta > config", "dialog_type")
        if DialogTypes.has_create_directory_button(dialog_type):
            DeltaCreateDirectoryButton(self)
        DeltaGridViewButton(self)
        DeltaListViewButton(self)
        DeltaToggleShowHidden(self)
        self._raise("delta > add to container", self)
