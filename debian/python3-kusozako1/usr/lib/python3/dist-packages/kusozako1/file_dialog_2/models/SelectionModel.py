# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaSelectionModel(Gtk.MultiSelection, DeltaEntity):

    @classmethod
    def new(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def enumerate_selection(self):
        selection = self.get_selection()
        for nth in range(0, selection.get_size()):
            index = selection.get_nth(nth)
            yield self[index]

    def get_selected(self):
        selection = self.get_selection()
        if selection.get_size() == 0:
            return None
        index = selection.get_nth(0)
        return self[index]

    def construct(self, model):
        self.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.MultiSelection.__init__(self)
