# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.thumbnailer.Thumbnailer import FoxtrotThumbnailer
from kusozako1.thumbnailer.Checksum import FoxtrotChecksum


class DeltaDrawingArea(Gtk.DrawingArea):

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._pixbuf is None:
            return
        x = (width-self._pixbuf.get_width())/2
        y = min(16, (height-self._pixbuf.get_height())/2)
        Gdk.cairo_set_source_pixbuf(cairo_context, self._pixbuf, x, y)
        cairo_context.paint()

    def _loaded(self, pixbuf, state):
        self._pixbuf = pixbuf
        self.queue_draw()

    def bind(self, gfile, file_info):
        self._thumbnailer.load_from_file_info_async_2(file_info, self._loaded)

    def __init__(self):
        self._checksum = FoxtrotChecksum.get_default()
        self._pixbuf = None
        self._thumbnailer = FoxtrotThumbnailer.get_default()
        Gtk.DrawingArea.__init__(self)
        self.set_draw_func(self._draw_func)
        self.set_size_request(128, 128)
