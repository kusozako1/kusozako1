# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from kusozako1.util import WebpToPixbuf


class DeltaPixbufLoader(DeltaEntity):

    def _idle(self, file_info):
        path = file_info.get_attribute_string("kusozako1::path")
        content_type = file_info.get_content_type()
        if content_type == "image/webp":
            pixbuf = WebpToPixbuf.from_path(path)
        else:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        self._raise("delta > pixbuf loaded", pixbuf)

    def load_async(self, file_info):
        GLib.idle_add(self._idle, file_info)

    def __init__(self, parent):
        self._parent = parent
