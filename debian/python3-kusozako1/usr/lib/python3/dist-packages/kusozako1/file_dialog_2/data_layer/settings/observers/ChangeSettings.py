# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaChangeSettings(DeltaEntity):

    def _signal_received(self, param):
        # key, value = param
        self._raise("delta > set setting", param)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.CHANGE_SETTINGS:
            return
        self._signal_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file dialog object", self)
