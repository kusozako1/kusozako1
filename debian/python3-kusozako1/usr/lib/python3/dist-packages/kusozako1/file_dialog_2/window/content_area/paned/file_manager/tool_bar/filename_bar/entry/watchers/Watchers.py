# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Changed import DeltaChanged
from .Activate import DeltaActivate
from .IconRelease import DeltaIconRelease
from .SelectionChanged import DeltaSelectionChanged


class EchoWatchers:

    def __init__(self, parent):
        DeltaChanged(parent)
        DeltaActivate(parent)
        DeltaIconRelease(parent)
        DeltaSelectionChanged(parent)
