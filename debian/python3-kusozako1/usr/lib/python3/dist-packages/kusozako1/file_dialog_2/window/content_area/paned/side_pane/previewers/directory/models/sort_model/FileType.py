# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaFileType(DeltaEntity):

    def _sort_func(self, alfa, bravo, user_data=None):
        return bravo.get_file_type() - alfa.get_file_type()

    def __init__(self, parent):
        self._parent = parent
        sorter = Gtk.CustomSorter.new(self._sort_func)
        self._raise("delta > add sorter", sorter)
