# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .overlay.Overlay import DeltaOverlay
from .header_bar.HeaderBar import DeltaHeaderBar
from .content_area.ContentArea import DeltaContentArea
from .observers.CloseWindow import DeltaCloseWindow


class DeltaWindow(Adw.Window, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self._box.append(widget)

    def _delta_call_add_overlay(self, widget):
        self._overlay.add_overlay(widget)

    def _delta_info_dialog_window(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Adw.Window.__init__(
            self,
            modal=True,
            transient_for=self._enquiry("delta > application window"),
            )
        self.set_size_request(1000, 600)
        self._overlay = Gtk.Overlay()
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self._box.set_opacity(0.9)
        self._overlay.set_child(self._box)
        self.set_content(self._overlay)
        DeltaOverlay(self)
        DeltaHeaderBar(self)
        DeltaContentArea(self)
        DeltaCloseWindow(self)
        self.present()
