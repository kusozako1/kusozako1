# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaFileName(DeltaEntity):

    def _get_collate_key(self, file_info):
        name = file_info.get_display_name()
        return GLib.utf8_collate_key_for_filename(name, -1)

    def _sort_func(self, alfa, bravo, user_data=None):
        compare = (self._get_collate_key(alfa) > self._get_collate_key(bravo))
        return 1 if compare else -1

    def __init__(self, parent):
        self._parent = parent
        sorter = Gtk.CustomSorter.new(self._sort_func)
        self._raise("delta > add sorter", sorter)
