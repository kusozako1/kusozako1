# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .DrawingArea import DeltaDrawingArea
from .status_label.StatusLabel import FoxtrotStatusLabel
from .NameLabel import FoxtrotNameLabel


class FoxtrotOverlay(Gtk.Overlay):

    def bind(self, gfile, file_info):
        self._drawing_area.bind(gfile, file_info)
        self._status_label.bind(file_info)
        self._name_label.bind(file_info)

    def set_selected(self, selected):
        self._status_label.set_selected(selected)

    def __init__(self):
        Gtk.Overlay.__init__(self, overflow=Gtk.Overflow.HIDDEN)
        self.add_css_class("card")
        self._drawing_area = DeltaDrawingArea()
        self.set_child(self._drawing_area)
        self._status_label = FoxtrotStatusLabel()
        self.add_overlay(self._status_label)
        self._name_label = FoxtrotNameLabel()
        self.add_overlay(self._name_label)
