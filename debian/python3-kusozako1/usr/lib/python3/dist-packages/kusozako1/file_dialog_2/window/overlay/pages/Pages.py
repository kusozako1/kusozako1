# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Blank import DeltaBlank
from .Greeter import DeltaGreeter
from .create_directory.CreateDirectory import DeltaCreateDirectory


class EchoPages:

    def __init__(self, parent):
        DeltaGreeter(parent)
        DeltaBlank(parent)
        DeltaCreateDirectory(parent)
