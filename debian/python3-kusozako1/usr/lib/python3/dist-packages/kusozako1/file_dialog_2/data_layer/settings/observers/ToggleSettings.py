# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaToggleSettings(DeltaEntity):

    def _signal_received(self, key):
        current_value = self._enquiry("delta > settings", key)
        user_data = key, not current_value
        self._raise("delta > set setting", user_data)

    def receive_transmission(self, user_data):
        signal, key = user_data
        if signal != FileDialogSignals.TOGGLE_SETTINGS:
            return
        self._signal_received(key)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file dialog object", self)
