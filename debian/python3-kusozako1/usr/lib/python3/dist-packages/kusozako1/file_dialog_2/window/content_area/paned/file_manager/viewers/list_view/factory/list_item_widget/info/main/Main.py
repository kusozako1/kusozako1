# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .NameLabel import FoxtrotNameLabel
from .SymlinkTarget import FoxtrotSymlinkTarget


class FoxtrotMain(Gtk.Box):

    def set_pixbuf_options(self, pixbuf_options):
        self._symlink_target.set_pixbuf_options(pixbuf_options)

    def bind(self, file_info):
        self._label.bind(file_info)
        self._symlink_target.bind(file_info)

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self._label = FoxtrotNameLabel()
        self.append(self._label)
        self._symlink_target = FoxtrotSymlinkTarget()
        self.append(self._symlink_target)
