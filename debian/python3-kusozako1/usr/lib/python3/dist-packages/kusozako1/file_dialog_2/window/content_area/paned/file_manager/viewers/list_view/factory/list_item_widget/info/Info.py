# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .main.Main import FoxtrotMain
from .Properties import FoxtrotProperties


class FoxtrotInfo(Gtk.Box):

    def _on_layout(self, top_level, width, height):
        if self.get_width() >= 600:
            self.set_orientation(Gtk.Orientation.HORIZONTAL)
            self.set_homogeneous(True)
        else:
            self.set_orientation(Gtk.Orientation.VERTICAL)
            self.set_homogeneous(False)

    def _on_realize(self, widget):
        native = self.get_native()
        surface = native.get_surface()
        surface.connect("layout", self._on_layout)

    def set_pixbuf_options(self, pixbuf_options):
        self._main.set_pixbuf_options(pixbuf_options)

    def bind(self, gfile, file_info):
        self._main.bind(file_info)
        self._properties.bind(file_info)

    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            homogeneous=True,
            )
        self._main = FoxtrotMain()
        self.append(self._main)
        self._properties = FoxtrotProperties()
        self.append(self._properties)
        self.connect("realize", self._on_realize)
