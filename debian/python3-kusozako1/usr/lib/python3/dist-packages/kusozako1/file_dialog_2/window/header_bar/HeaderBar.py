# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .controllers.Controllers import EchoControllers
from .CancelButton import DeltaCancelButton
from .TitleLabel import DeltaTitleLabel
from .select_button.SelectButton import DeltaSelectButton


class DeltaHeaderBar(Gtk.CenterBox, DeltaEntity):

    def _delta_call_pack_start(self, widget):
        self.set_start_widget(widget)

    def _delta_call_pack_center(self, widget):
        self.set_center_widget(widget)

    def _delta_call_pack_end(self, widget):
        self.set_end_widget(widget)

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(self)
        self.add_css_class("kusozako-primary-container")
        self.set_size_request(-1, 48)
        EchoControllers(self)
        DeltaCancelButton(self)
        DeltaTitleLabel(self)
        DeltaSelectButton(self)
        self._raise("delta > add to container", self)
