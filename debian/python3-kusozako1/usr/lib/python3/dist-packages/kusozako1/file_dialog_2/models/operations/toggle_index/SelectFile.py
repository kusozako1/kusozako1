# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from .LongPressCancelHook import DeltaLongPressCancelHook


class DeltaSelectFile(DeltaEntity):

    def _toggle_selection(self, selection_model, index):
        if selection_model.is_selected(index):
            selection_model.unselect_item(index)
        else:
            selection_model.select_item(index, True)

    def _signal_received(self, index):
        selection_model = self._enquiry("delta > selection model")
        file_info = selection_model[index]
        # released = self._long_press_cancel_hook.try_release_hook()
        if file_info.get_file_type() == Gio.FileType.DIRECTORY:
            return
        self._toggle_selection(selection_model, index)

    def _on_initialize(self):
        self._long_press_cancel_hook = DeltaLongPressCancelHook(self)

    def receive_transmission(self, user_data):
        signal, index = user_data
        if signal != FileDialogSignals.TOGGLE_INDEX:
            return
        self._signal_received(index)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._raise("delta > register file dialog object", self)
