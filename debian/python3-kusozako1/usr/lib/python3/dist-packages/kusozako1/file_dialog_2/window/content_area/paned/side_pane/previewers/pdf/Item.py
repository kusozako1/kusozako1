# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib


class FoxtrotItem(Gtk.Picture):

    @classmethod
    def new_for_page(cls, page):
        instance = cls()
        instance.construct(page)
        return instance

    def _idle(self, page):
        size = page.get_size()
        width, height = int(size.width), int(size.height)
        surface = cairo.ImageSurface(cairo.Format.ARGB32, width, height)
        cairo_context = cairo.Context(surface)
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.rectangle(0, 0, width, height)
        cairo_context.fill()
        page.render(cairo_context)
        pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, width, height)
        self.set_pixbuf(pixbuf)

    def construct(self, page):
        GLib.idle_add(self._idle, page)

    def __init__(self):
        Gtk.Picture.__init__(self, can_shrink=True)
