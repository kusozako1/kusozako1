# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .model.Model import DeltaModel
from .list_box.ListBox import DeltaListBox


class DeltaBookmarks(DeltaEntity):

    def _delta_info_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        DeltaListBox(self)
