# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaTitleLabel(Gtk.Label, DeltaEntity):

    def _get_title(self):
        title = self._enquiry("delta > config", "title")
        if title is not None:
            return title
        return "set fuckin title !!"

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            label=self._get_title(),
            )
        self._raise("delta > pack center", self)
