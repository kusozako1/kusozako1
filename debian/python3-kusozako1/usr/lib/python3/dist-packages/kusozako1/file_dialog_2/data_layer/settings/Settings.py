# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from .DefaultSettings import DeltaDefaultSettings
from .observers.Observers import EchoObservers


class DeltaSettings(DeltaEntity):

    def _raise_signal(self, key, value):
        param = key, value
        user_data = FileDialogSignals.SETTINGS_CHANGED, param
        self._raise("delta > file dialog signal", user_data)

    def _delta_call_set_setting(self, user_data):
        key, value = user_data
        self._settings[key] = value
        self._raise_signal(key, value)

    def __getitem__(self, key):
        return self._settings.get(key, None)

    def __init__(self, parent):
        self._parent = parent
        self._settings = {}
        DeltaDefaultSettings(self)
        EchoObservers(self)
