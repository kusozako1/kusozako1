# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaItem(Gtk.Box, DeltaEntity):

    @classmethod
    def new_for_gfile(cls, parent, gfile):
        instance = cls(parent)
        instance.construct(gfile)
        return instance

    def construct(self, gfile):
        file_info = gfile.query_info("*", 0)
        gicon = file_info.get_symbolic_icon()
        image = Gtk.Image.new_from_gicon(gicon)
        image.set_margin_start(8)
        self.append(image)
        uri = gfile.get_uri()
        if uri == "recent:///":
            basename = _("Recent Files")
        else:
            basename = gfile.get_basename()
        label = Gtk.Label(label=basename, margin_start=8)
        self.append(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=8,
            )
        self.add_css_class("kusozako-primary-widget")
        self.set_size_request(-1, 32)
