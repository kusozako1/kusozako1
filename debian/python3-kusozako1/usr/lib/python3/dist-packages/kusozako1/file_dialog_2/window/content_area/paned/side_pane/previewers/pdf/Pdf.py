# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import PreviewerTypes
from .Model import FoxtrotModel
from .Item import FoxtrotItem


class DeltaPdf(Gtk.FlowBox, DeltaEntity):

    def _create_widget_func(self, page):
        return FoxtrotItem.new_for_page(page)

    def set_file_info(self, file_info):
        self._model.set_file_info(file_info)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        scrolled_window.add_css_class("osd")
        Gtk.FlowBox.__init__(self, max_children_per_line=1)
        self._model = FoxtrotModel()
        self.bind_model(self._model, self._create_widget_func)
        scrolled_window.set_child(self)
        user_data = scrolled_window, PreviewerTypes.PDF
        self._raise("delta > add to stack", user_data)
