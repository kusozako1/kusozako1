# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaShowHidden(Gtk.CustomFilter, DeltaEntity):

    __key__ = "show-hidden"

    def _match_func(self, file_info, user_data=None):
        if self._show_hidden:
            return True
        return not file_info.get_is_hidden()

    def _change_filter(self, show_hidden):
        self._show_hidden = show_hidden
        if show_hidden:
            self.changed(Gtk.FilterChange.LESS_STRICT)
        else:
            self.changed(Gtk.FilterChange.MORE_STRICT)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.SETTINGS_CHANGED:
            return
        key, value = param
        if key != self.__key__:
            return
        self._change_filter(value)

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = self._enquiry("delta > settings", self.__key__)
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._match_func, None)
        self._raise("delta > add filter", self)
        self._raise("delta > register file dialog object", self)
