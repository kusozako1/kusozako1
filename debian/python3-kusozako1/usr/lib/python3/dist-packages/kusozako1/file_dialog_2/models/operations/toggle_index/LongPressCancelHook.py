# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaLongPressCancelHook(DeltaEntity):

    def _on_received(self, param=None):
        self._has_hook = True

    def _on_initialize(self):
        self._has_hook = False

    def try_release_hook(self):
        if not self._has_hook:
            return False
        self._has_hook = False
        user_data = FileDialogSignals.BACK_TO_BOOKMARK, None
        self._raise("delta > file dialog signal", user_data)
        return True

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.LONG_PRESS_CANCEL_HOOK:
            return
        self._on_received(param)

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._raise("delta > register file dialog object", self)
