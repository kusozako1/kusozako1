# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject
from kusozako1.thumbnailer.Thumbnailer import FoxtrotThumbnailer
from kusozako1.thumbnailer.Checksum import FoxtrotChecksum

PIXBUF_OPTIONS = (GObject.SIGNAL_RUN_FIRST, None, (GObject.TYPE_PYOBJECT,))


class FoxtrotImage(Gtk.DrawingArea):

    __gsignals__ = {
        "pixbuf-options": PIXBUF_OPTIONS
        }

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._pixbuf is None:
            return
        x = (width-self._pixbuf.get_width())/2
        y = min(16, (height-self._pixbuf.get_height())/2)
        Gdk.cairo_set_source_pixbuf(cairo_context, self._pixbuf, x, y)
        cairo_context.paint()

    def _loaded(self, pixbuf, state):
        pixbuf_options = pixbuf.get_options()
        if pixbuf_options:
            self.emit("pixbuf-options", pixbuf_options)
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        scale = min(64/width, 64/height)
        new_width = int(width*scale)
        new_height = int(height*scale)
        self._pixbuf = pixbuf.scale_simple(new_width, new_height, 2)
        self.queue_draw()

    def bind(self, gfile, file_info):
        self._thumbnailer.load_from_file_info_async_2(file_info, self._loaded)

    def __init__(self):
        self._checksum = FoxtrotChecksum.get_default()
        self._pixbuf = None
        self._thumbnailer = FoxtrotThumbnailer.get_default()
        Gtk.DrawingArea.__init__(self)
        self.set_draw_func(self._draw_func)
        self.set_size_request(64, 64)
