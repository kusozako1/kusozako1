# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import ConfigKeys
from kusozako1.file_dialog_2.const import DialogTypes
from .SaveFile import DeltaSaveFile


class DeltaSensitivities(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        dialog_type = self._enquiry("delta > config", ConfigKeys.DIALOG_TYPE)
        if dialog_type == DialogTypes.SAVE_FILE:
            DeltaSaveFile(self)
