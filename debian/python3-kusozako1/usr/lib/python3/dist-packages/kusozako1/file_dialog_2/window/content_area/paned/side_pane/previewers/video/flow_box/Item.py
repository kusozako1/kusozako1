# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib


class FoxtrotItem(Gtk.Picture):

    @classmethod
    def new(cls, pixbuf):
        instance = cls()
        instance.construct(pixbuf)
        return instance

    def _idle(self, pixbuf):
        self.set_pixbuf(pixbuf)

    def construct(self, pixbuf):
        GLib.idle_add(self._idle, pixbuf)

    def __init__(self):
        Gtk.Picture.__init__(self, can_shrink=True)
