# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import OverlayPages
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaModel(DeltaEntity):

    def _get_child_gfile(self, basename):
        if "/" in basename:
            return None
        child_gfile = self._gfile.get_child(basename)
        if child_gfile.query_exists():
            return None
        return child_gfile

    def change_name(self, name):
        self._basename = name

    def try_make_directory(self):
        if not self._basename:
            return
        child_gfile = self._get_child_gfile(self._basename)
        if child_gfile is None:
            return
        child_gfile.make_directory()
        self._raise("delta > hide overlay")
        user_data = FileDialogSignals.MOVE_DIRECTORY, child_gfile
        self._raise("delta > file dialog signal", user_data)

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != FileDialogSignals.SHOW_OVERLAY:
            return
        page_name, gfile = signal_param
        if page_name == OverlayPages.CREATE_DIRECTORY:
            self._gfile = gfile
            self._basename = ""

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file dialog object", self)
