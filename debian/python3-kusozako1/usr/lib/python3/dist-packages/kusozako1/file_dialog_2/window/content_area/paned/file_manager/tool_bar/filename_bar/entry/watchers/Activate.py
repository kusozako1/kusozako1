# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaActivate(DeltaEntity):

    def _on_activate(self, entry):
        if not entry.get_text():
            return
        user_data = FileDialogSignals.SELECTION_FINISHED, None
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        entry = self._enquiry("delta > entry")
        entry.connect("activate", self._on_activate)
