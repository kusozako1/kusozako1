# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaSaveFile(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, filename = user_data
        if signal != FileDialogSignals.FILENAME_CHANGED:
            return
        self._raise("delta > change sensitive", filename != "")

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file dialog object", self)
