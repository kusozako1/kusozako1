# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from .Item import DeltaItem


class DeltaListBox(Gtk.ListBox, DeltaEntity):

    def _create_widget_func(self, gfile):
        return DeltaItem.new_for_gfile(self, gfile)

    def _on_row_activated(self, list_box, row):
        if row is None:
            return
        index = row.get_index()
        model = self._enquiry("delta > model")
        gfile = model[index]
        user_data = FileDialogSignals.MOVE_DIRECTORY, gfile
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        Gtk.ListBox.__init__(self)
        model = self._enquiry("delta > model")
        self.bind_model(model, self._create_widget_func)
        self.add_css_class("kusozako-secondary-container")
        self.connect("row-activated", self._on_row_activated)
        scrolled_window.set_child(self)
        user_data = scrolled_window, "bookmark"
        self._raise("delta > add to stack", user_data)
