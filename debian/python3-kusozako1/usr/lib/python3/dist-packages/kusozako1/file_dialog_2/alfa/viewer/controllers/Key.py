# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals

FILE_MANAGER_SIGNALS = {
    "Escape": FileDialogSignals.UNSELECT_ALL,
    "Space": FileDialogSignals.TOGGLE_SELECTED,
    "Return": FileDialogSignals.RELEASE_ACTIVATION_HOOK,
    }


class DeltaKey(Gtk.EventControllerKey, DeltaEntity):

    def _on_key_released(self, controller, keyval, keycode, modifier):
        accel = Gtk.accelerator_get_label(keyval, modifier)
        signal = FILE_MANAGER_SIGNALS.get(accel, None)
        if signal is None:
            return
        user_data = signal, None
        self._raise("delta > file dialog signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerKey.__init__(self)
        self.connect("key-released", self._on_key_released)
        self._raise("delta > add controller", self)
