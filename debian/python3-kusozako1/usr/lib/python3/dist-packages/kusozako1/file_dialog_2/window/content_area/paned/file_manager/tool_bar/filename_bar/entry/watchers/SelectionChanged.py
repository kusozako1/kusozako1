# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaSelectionChanged(DeltaEntity):

    def _on_selection_changed(self, model, position, n_items):
        selection = model.get_selection()
        if selection.get_size() == 0:
            return
        index = selection.get_nth(0)
        file_info = model[index]
        filename = file_info.get_name()
        entry = self._enquiry("delta > entry")
        entry.set_text(filename)

    def __init__(self, parent):
        self._parent = parent
        model = self._enquiry("delta > selection model")
        model.connect("selection-changed", self._on_selection_changed)
