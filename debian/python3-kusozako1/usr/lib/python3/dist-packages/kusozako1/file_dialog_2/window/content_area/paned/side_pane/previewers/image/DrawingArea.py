# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity

INTERP = GdkPixbuf.InterpType.BILINEAR


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _get_scaled_pixbuf(self, allocated_width, allocated_height):
        pixbuf_width = self._pixbuf.get_width()
        pixbuf_height = self._pixbuf.get_height()
        width_rate = allocated_width / pixbuf_width
        height_rate = allocated_height / pixbuf_height
        rate = min(1, min(width_rate, height_rate))
        new_width = pixbuf_width*rate
        new_height = pixbuf_height*rate
        return self._pixbuf.scale_simple(new_width, new_height, INTERP)

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._pixbuf is None:
            return
        pixbuf = self._get_scaled_pixbuf(width, height)
        x = (width-pixbuf.get_width())/2
        y = (height-pixbuf.get_height())/2
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, x, y)
        cairo_context.paint()

    def set_pixbuf(self, pixbuf):
        self._pixbuf = pixbuf
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._pixbuf = None
        Gtk.DrawingArea.__init__(
            self,
            hexpand=True,
            vexpand=True,
            margin_top=8,
            margin_start=8,
            margin_end=8,
            )
        self.set_draw_func(self._draw_func)
        self._raise("delta > add to container", self)
