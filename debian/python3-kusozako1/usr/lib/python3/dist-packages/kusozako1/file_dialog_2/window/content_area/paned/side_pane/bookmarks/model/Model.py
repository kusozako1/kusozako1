# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .XdgUserDirs import DeltaXdgUserDirs
from .GtkBookmarks import DeltaGtkBookmarks


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _delta_call_gfile_found(self, alfa_gfile):
        for bravo_gfile in self:
            if bravo_gfile.equal(alfa_gfile):
                return
        self.append(alfa_gfile)

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gio.File)
        recent = Gio.File.new_for_uri("recent://")
        self.append(recent)
        DeltaXdgUserDirs(self)
        DeltaGtkBookmarks(self)
