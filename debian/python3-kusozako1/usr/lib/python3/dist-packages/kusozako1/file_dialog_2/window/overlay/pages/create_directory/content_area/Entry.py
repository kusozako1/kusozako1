# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import OverlayPages
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaEntry(Gtk.Entry, DeltaEntity):

    def _on_changed(self, entry):
        self._raise("delta > change name", entry.get_text())

    def _on_activate(self, entry):
        self._raise("delta > try make directory")

    def _reset(self, gfile):
        self.set_text("")
        self.grab_focus()

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != FileDialogSignals.SHOW_OVERLAY:
            return
        page_name, gfile = signal_param
        if page_name == OverlayPages.CREATE_DIRECTORY:
            self._reset(gfile)

    def __init__(self, parent):
        self._parent = parent
        box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=True,
            spacing=16,
            )
        Gtk.Entry.__init__(self, margin_bottom=16)
        self.set_placeholder_text(_("Input Directory Name"))
        box.append(Gtk.Box())
        box.append(self)
        box.append(Gtk.Box())
        self._raise("delta > add to container", box)
        self._raise("delta > register file dialog object", self)
        self.connect("changed", self._on_changed)
        self.connect("activate", self._on_activate)
