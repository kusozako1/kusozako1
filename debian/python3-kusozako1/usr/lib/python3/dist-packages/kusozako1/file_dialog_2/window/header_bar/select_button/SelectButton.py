# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from kusozako1.file_dialog_2.const import ConfigKeys
from kusozako1.file_dialog_2.const import DialogTypes
from .sensitivities.Sensitivities import DeltaSensitivities


class DeltaSelectButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = FileDialogSignals.SELECTION_FINISHED, None
        self._raise("delta > file dialog signal", user_data)

    def _delta_call_change_sensitive(self, sensitive):
        self.set_sensitive(sensitive)

    def _get_label(self):
        label = self._enquiry("delta > config", ConfigKeys.ACCEPT_LABEL)
        if label is not None:
            return label
        dialog_type = self._enquiry("delta > config", ConfigKeys.DIALOG_TYPE)
        if dialog_type == DialogTypes.SAVE_FILE:
            return _("Save")
        return _("Select")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=self._get_label(),
            has_frame=True,
            sensitive=True,
            margin_top=8,
            margin_bottom=8,
            margin_start=8,
            margin_end=8,
            )
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        DeltaSensitivities(self)
        self._raise("delta > pack end", self)
