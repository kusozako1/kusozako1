# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import OverlayPages


class DeltaBlank(Gtk.Box, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, can_target=False)
        user_data = self, OverlayPages.BLANK
        self._raise("delta > add to stack", user_data)
