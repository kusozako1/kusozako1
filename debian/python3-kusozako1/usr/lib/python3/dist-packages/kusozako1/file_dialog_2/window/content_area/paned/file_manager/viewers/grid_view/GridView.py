# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.file_dialog_2.const import ViewerTypes
from kusozako1.file_dialog_2.alfa.viewer.Viewer import AlfaViewer
from .factory.Factory import DeltaFactory


class DeltaGridView(AlfaViewer):

    __top_margin__ = 56
    __viewer_type__ = ViewerTypes.GRID_VIEW

    def _on_realize(self, grid_view):
        root = grid_view.get_root()
        root.set_focus(grid_view)

    def _get_viewer(self):
        viewer = Gtk.GridView(
            margin_top=self._get_top_margin(),
            model=self._enquiry("delta > viewer model"),
            single_click_activate=True,
            factory=DeltaFactory(self),
            max_columns=999,    # as infinity
            )
        viewer.connect("realize", self._on_realize)
        viewer.add_css_class("kusozako-content-area")
        return viewer
