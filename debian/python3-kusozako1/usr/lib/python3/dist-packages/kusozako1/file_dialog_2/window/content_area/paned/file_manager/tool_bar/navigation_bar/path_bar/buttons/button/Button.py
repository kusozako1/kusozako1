# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .watchers.Clicked import DeltaClicked
from .label.Label import DeltaLabel


class DeltaButton(Gtk.Button, DeltaEntity):

    @classmethod
    def new_for_gio_file(cls, parent, gfile):
        button = cls(parent)
        button.construct(gfile)
        return button

    def _delta_info_button(self):
        return self

    def _delta_info_button_gfile(self):
        return self._gfile

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def construct(self, gfile):
        self._gfile = gfile
        DeltaLabel(self)
        DeltaClicked(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            vexpand=False,
            can_focus=False
            )
        self.add_css_class("kusozako-primary-widget")
