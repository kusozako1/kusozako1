# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from kusozako1.file_dialog_2.const import ViewerTypes
from .Icon import DeltaIcon


class DeltaListViewButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        signal_param = "viewer-type", ViewerTypes.LIST_VIEW
        user_data = FileDialogSignals.CHANGE_SETTINGS, signal_param
        self._raise("delta > file dialog signal", user_data)

    def _delta_call_add_to_container(self, widget):
        self.set_child(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            has_frame=False,
            tooltip_text=_("List View"),
            can_focus=False,
            vexpand=False,
            )
        DeltaIcon(self)
        self.add_css_class("kusozako-primary-widget")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
