# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from .SelectionModelWatcher import FoxtrotSelectionModelWatcher
from .overlay.Overlay import FoxtrotOverlay


class FoxtrotListItemWidget(Gtk.AspectFrame):

    def bind(self, gfile, file_info):
        self._watcher.bind(file_info)
        self._overlay.bind(gfile, file_info)

    def _on_selection_changed(self, selection_model_watcher, selected):
        self._overlay.set_selected(selected)

    def __init__(self, selection_model):
        Gtk.AspectFrame.__init__(
            self,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            )
        self._watcher = FoxtrotSelectionModelWatcher(selection_model)
        self._watcher.connect("selection-changed", self._on_selection_changed)
        self._overlay = FoxtrotOverlay()
        self.set_child(self._overlay)
