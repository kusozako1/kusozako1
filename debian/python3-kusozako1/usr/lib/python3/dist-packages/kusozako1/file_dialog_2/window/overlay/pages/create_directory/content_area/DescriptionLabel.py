# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import OverlayPages
from kusozako1.file_dialog_2.const import FileDialogSignals

TEMPLATE = "parent directory : <i>{}</i>"


class DeltaDescriptionLabel(Gtk.Label, DeltaEntity):

    def _reset(self, gfile):
        path = gfile.get_path()
        self.set_markup(TEMPLATE.format(path))

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != FileDialogSignals.SHOW_OVERLAY:
            return
        page_name, gfile = signal_param
        if page_name == OverlayPages.CREATE_DIRECTORY:
            self._reset(gfile)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, label="don't look at me")
        self.set_size_request(-1, 32)
        self._raise("delta > add to container", self)
        self._raise("delta > register file dialog object", self)
