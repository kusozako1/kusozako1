# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals
from .grid_view.GridView import DeltaGridView
from .list_view.ListView import DeltaListView


class DeltaViewers(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != FileDialogSignals.SETTINGS_CHANGED:
            return
        key, value = param
        if key != "viewer-type":
            return
        self.set_visible_child_name(value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.SLIDE_LEFT_RIGHT,
            )
        DeltaGridView(self)
        DeltaListView(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register file dialog object", self)
