# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.const import FileDialogSignals


class DeltaCloseWindow(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != FileDialogSignals.CLOSE_DIALOG:
            return
        window = self._enquiry("delta > dialog window")
        window.close()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register file dialog object", self)
