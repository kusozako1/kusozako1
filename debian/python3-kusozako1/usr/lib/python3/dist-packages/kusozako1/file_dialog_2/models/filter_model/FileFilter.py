# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaFileFilter(Gtk.FileFilter, DeltaEntity):

    def _add_mime_types(self):
        mime_types = self._enquiry("delta > config", "mime_types")
        if mime_types is None:
            return
        for mime_type in mime_types:
            self.add_mime_type(mime_type)

    def _add_suffixes(self):
        suffixes = self._enquiry("delta > config", "suffixes")
        if suffixes is None:
            return
        for suffix in suffixes:
            self.add_suffix(suffix)

    def __init__(self, parent):
        self._parent = parent
        Gtk.FileFilter.__init__(
            self,
            mime_types=["inode/directory"],
            )
        if self._enquiry("delta > config", "pixbuf_formats_only"):
            self.add_pixbuf_formats()
        self._add_mime_types()
        self._add_suffixes()
        self._raise("delta > add filter", self)
