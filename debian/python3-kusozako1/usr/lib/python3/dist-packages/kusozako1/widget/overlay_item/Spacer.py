# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaSpacer(Gtk.Box, DeltaEntity):

    def _on_pressed(self, *args):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, hexpand=True)
        gesture = Gtk.GestureClick()
        gesture.connect("pressed", self._on_pressed)
        self.add_controller(gesture)
        self._raise("delta > add to container", self)
