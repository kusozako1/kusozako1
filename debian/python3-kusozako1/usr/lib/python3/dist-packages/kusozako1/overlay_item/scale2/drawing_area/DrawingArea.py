# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .controllers.Controllers import EchoControllers

COLOR_RGB = 1, 1, 1
LINE_WIDTH = 2
RADIUS = 8
MARGIN = 8


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)

    def _draw_func(self, drawing_area, cairo_context, width, height):
        if self._value is None:
            return
        cairo_context.set_source_rgb(*COLOR_RGB)
        cairo_context.set_line_width(LINE_WIDTH)
        cairo_context.move_to(MARGIN, height/2)
        cairo_context.line_to(width-MARGIN, height/2)
        cairo_context.stroke()
        value = (self._value-self._min)/(self._max-self._min)
        x = value*100+MARGIN
        cairo_context.arc(x, height/2, RADIUS, 0, 2*GLib.PI)
        cairo_context.fill()

    def _delta_call_add_controller(self, controller):
        self.add_controller(controller)

    def receive_transmission(self, value):
        self._value = value
        self.queue_draw()

    def construct(self, model):
        self._step = model.get("step", 0.01)
        self._max = model.get("max", 1)
        self._min = model.get("min", 0)
        self._scale = (self._max - self._min)/self._step

    def __init__(self, parent):
        self._parent = parent
        self._value = None
        Gtk.DrawingArea.__init__(self)
        self.add_css_class("kusozako-primary-widget")
        self.set_draw_func(self._draw_func)
        self.set_size_request(100+MARGIN*2, -1)
        EchoControllers(self)
        self._raise("delta > add to container", self)
