# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter


class DeltaData(DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        data = cls(parent)
        data.construct(model)
        return data

    def _timeout(self, index, value):
        if self._index == index:
            user_data = self._query+(value,)
            self._raise("delta > settings", user_data)
        return GLib.SOURCE_REMOVE

    def set_value(self, value):
        self._value = max(self._min, min(self._max, value))
        self._transmitter.transmit(self._value)
        index = self._index+1
        GLib.timeout_add(50, self._timeout, index, self._value)
        self._index += 1

    def register(self, object_):
        self._transmitter.register_listener(object_)
        object_.receive_transmission(self._value)

    def construct(self, model):
        self._max = model.get("max", 1)
        self._min = model.get("min", 0)
        self._query = model["query"]
        query = model["query"]+(model["default"],)
        self._value = self._enquiry("delta > settings", query)

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
        self._transmitter = FoxtrotTransmitter()
