# (c) copyright 2022-20242, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk


class FoxtrotLabel:

    @classmethod
    def new(cls, box, label):
        instance = cls()
        widget = instance.construct(label)
        box.append(widget)
        return widget

    def construct(self, label):
        return Gtk.Label(
            label=label,
            xalign=0,
            hexpand=True,
            margin_start=8,
            margin_end=8,
            )

    def __init__(self):
        pass
