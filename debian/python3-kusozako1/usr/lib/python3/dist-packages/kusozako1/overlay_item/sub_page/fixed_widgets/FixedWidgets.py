# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .BackButton import DeltaBackButton
from .Separator import DeltaSeparator


class EchoFixedWidgets:

    def __init__(self, parent):
        DeltaBackButton(parent)
        DeltaSeparator(parent)
