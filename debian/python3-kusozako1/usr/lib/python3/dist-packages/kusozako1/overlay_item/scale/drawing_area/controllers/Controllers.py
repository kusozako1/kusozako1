# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .GestureSingle import DeltaGestureSingle
from .GestureDrag import DeltaGestureDrag


class EchoControllers:

    def __init__(self, parent):
        DeltaGestureSingle(parent)
        DeltaGestureDrag(parent)
