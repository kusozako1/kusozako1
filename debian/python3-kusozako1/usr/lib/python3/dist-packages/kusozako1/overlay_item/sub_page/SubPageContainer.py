# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaSubpageContainer(Gtk.Box, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(
            vexpand=True,
            propagate_natural_width=True,
            )
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        scrolled_window.set_child(self)
        self._raise("delta > add to base container", scrolled_window)
