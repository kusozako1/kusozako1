# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1 import SymbolicIcon
from kusozako1.overlay_item.button.Button import AlfaButton


class AlfaSettingsButton(AlfaButton):

    LABEL = "define label here."
    GROUP = "define settings group here."
    KEY = "define settings key here."
    MATCH_VALUE = "define match value here."

    def _on_clicked(self, button):
        user_data = self.GROUP, self.KEY, self.MATCH_VALUE
        self._raise("delta > settings", user_data)

    def _refresh(self, type_):
        if type_ == self.MATCH_VALUE:
            name = "radio-checked-symbolic"
        else:
            name = "radio-symbolic"
        paintable = SymbolicIcon.get_paintable_for_name(name)
        self._start_image.set_from_paintable(paintable)

    def _on_initialize(self):
        query = self.GROUP, self.KEY, self.MATCH_VALUE
        type_ = self._enquiry("delta > settings", query)
        self._refresh(type_)
        self._raise("delta > register settings object", self)

    def receive_transmission(self, user_data):
        group, key, type_ = user_data
        if group == self.GROUP and key == self.KEY:
            self._refresh(type_)
