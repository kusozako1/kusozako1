# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from kusozako1.overlay_item.button.Button import AlfaButton


class DeltaBackButton(AlfaButton):

    START_ICON = "go-previous-symbolic"
    LABEL = _("Back")

    def _set_to_container(self):
        # overridden
        self._raise("delta > add to base container", self)

    def _on_clicked(self, button):
        back_to = self._enquiry("delta > back to")
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU, back_to
        self._raise("delta > main window signal", user_data)
