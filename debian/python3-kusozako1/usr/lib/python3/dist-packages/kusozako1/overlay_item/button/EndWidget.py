# (c) copyright 2022-20242, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1 import SymbolicIcon


class FoxtrotEndWidget:

    @classmethod
    def new(cls, box, shortcut, end_icon):
        instance = cls()
        widget = instance.construct(shortcut, end_icon)
        if widget is not None:
            box.append(widget)

    def construct(self, shortcut, end_icon):
        if shortcut:
            return Gtk.Label(label=shortcut)
        elif end_icon:
            return SymbolicIcon.get_image_for_name(end_icon)

    def __init__(self):
        pass
